import React from 'react';
import { Layout } from 'antd';
import { ToastContainer } from 'react-toastify';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { useSelector } from "react-redux";

import Home from "./pages/Home";
import ProductDetail from "./pages/ProductDetail";
import Header from "./components/Header";
import Message from "./components/Message";
import HeaderAuth from "./components/HeaderAuth";
import Profile from "./pages/Profile";
import ChangePassword from "./pages/ChangePassword";
import Cart from "./pages/Cart";
import Purchase from "./pages/Purchase";
import SearchProduct from "./pages/SearchProduct";
import MyOrder from "./pages/MyOrder";

import "./index.scss";
import 'antd/dist/antd.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';

function App() {

  const { Content, Footer } = Layout;

  const { token } = useSelector(state => state.auth);

  return (

    <Router>
      <Message>
        <Layout className="layout">
          {token ? <HeaderAuth /> : <Header />}
          <Content style={{ padding: '0 50px' }}>
            <div className="mt-5 container">
              <Switch>
                <Route component={Home} exact path="/" />
                <Route component={Profile} exact path="/profile" />
                <Route component={ProductDetail} exact path="/product-detail/:_id" />
                <Route component={ChangePassword} exact path="/change-password" />
                <Route component={Cart} exact path="/cart" />
                <Route component={Purchase} exact path="/purchase" />
                <Route component={SearchProduct} exact path="/search-product/:searchName" />
                <Route component={MyOrder} exact path="/my-order" />
              </Switch>
              <ToastContainer />
            </div>

          </Content>
          <Footer style={{ textAlign: 'center' }}>Created by Hà Nhựt</Footer>
        </Layout>
      </Message>
    </Router>


  );
}

export default App;
