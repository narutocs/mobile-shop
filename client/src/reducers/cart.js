import * as ActionType from "../constants/ActionType";

const initialState = {
  product: localStorage.getItem("CART") ? JSON.parse(localStorage.getItem("CART")).product : []
}

const cartReducers = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.ADD_CART_SUCCESS:
      const newList = [...state.product];

      const index = newList.findIndex(item => item.product._id === action.payload.product.product._id);

      if (index === -1) {
        newList.push(action.payload.product);
      } else {
        if (action.payload.product.product.colorId._id === newList[index].product.colorId._id) {
          newList[index].quality = newList[index].quality + action.payload.product.quality;
        } else {
          newList.push(action.payload.product);
        }
      }
      localStorage.setItem("CART", JSON.stringify({
        ...state,
        product: newList
      }))
      return {
        ...state,
        product: newList
      }

    case ActionType.CREATE_ORDER_SUCCESS:

      return {
        ...state,
        product: []
      }

    case ActionType.REMOVE_CART_SUCCESS:
      const newListProduct = [...state.product];

      const indexProduct = newListProduct.findIndex(item => item.product._id === action.payload.product.product._id);

      if (indexProduct !== -1) {
        if (action.payload.product.product.colorId._id === newListProduct[indexProduct].product.colorId._id) {
          newListProduct.splice(indexProduct, 1)
        }
      }
      localStorage.setItem("CART", JSON.stringify({
        ...state,
        product: newListProduct
      }))
      return {
        ...state,
        product: newListProduct
      }
    default: return state;
  }
}

export default cartReducers;