import * as ActionType from "../constants/ActionType";

const initialState = {
  list_city: [],
  list_district: [],
  list_village: []
}

const addressReducers = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.GET_CITY:
      return {
        ...state,
        list_city: action.payload.data
      };
    case ActionType.GET_DISTRICT:
      return {
        ...state,
        list_district: action.payload.data
      };
    case ActionType.GET_VILLAGE:
      return {
        ...state,
        list_village: action.payload.data
      };
    default: return state;
  }
}
export default addressReducers;