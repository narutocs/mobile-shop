import { combineReducers } from "redux";

import authReducers from "./auth";
import messageReducers from "./message";
import loadingReducers from "./loading";
import activeReducers from "./active";
import userReducers from "./user";
import productReducers from "./product";
import cartReducers from "./cart";
import addressReducers from "./address";
import orderReducers from "./order";

const appReducers = combineReducers({
  auth: authReducers,
  message: messageReducers,
  isLoading: loadingReducers,
  active: activeReducers,
  user: userReducers,
  product: productReducers,
  cart: cartReducers,
  address: addressReducers,
  order: orderReducers
})

export default appReducers;