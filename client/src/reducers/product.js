import * as ActionType from "../constants/ActionType";
import MessageType from "../constants/MessageType";

const initialState = {
  listNewProduct: [],
  current: [{}],
  selected: {},
  listProduct: [],
  pagination: {}
}

const productReducers = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.GET_PRODUCT_NEW_SUCCESS:
      return {
        ...state,
        listNewProduct: action.payload.products
      }
    case ActionType.GET_PRODUCT_SUCCESS:
      return {
        ...state,
        current: action.payload.product,
        selected: action.payload.product[0]
      }
    case ActionType.SELECTED_PRODUCT_SUCCESS:
      return {
        ...state,
        selected: action.payload.selected
      }
    case ActionType.SEARCH_PRODUCT:
      return {
        ...state,
        listProduct: action.payload.products,
        pagination: action.payload.pagination
      }
    default: return state;
  }
}

export default productReducers;