import * as ActionType from "../constants/ActionType";

const initialState = false

const loadingReducers = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.USER_LOADED:
      return false;
    case ActionType.USER_LOADING:
      return true;
    default: return state;
  }
}
export default loadingReducers;