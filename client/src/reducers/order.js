import * as ActionType from "../constants/ActionType";
import MessageType from "../constants/MessageType";

const initialState = {
  listOrder: [],
  orderId: '',
  pagination: {}
}

const orderReducers = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.CREATE_ORDER_SUCCESS:
      return {
        ...state,
        orderId: action.payload.orderId,
      }
    case ActionType.GET_LIST_ORDER_SUCCESS:
      return {
        ...state,
        listOrder: action.payload.listOrder,
        pagination: action.payload.pagination
      }

    default: return state;
  }
}

export default orderReducers;