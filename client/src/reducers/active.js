import * as ActionType from "../constants/ActionType";

const initialState = null

const activeReducers = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.ACTIVE_FAIL:
      return false;
    case ActionType.ACTIVE_SUCCESS:
      return true;
    default: return state;
  }
}
export default activeReducers;