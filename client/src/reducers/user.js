import * as ActionType from "../constants/ActionType";

const initialState = {
  isChangePasswordSuccess: false
}

const userReducers = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.CHANGE_PASSWORD_SUCCESS:
      return {
        ...state,
        isChangePasswordSuccess: true
      };
    case ActionType.CHANGE_PASSWORD_FAIL:
      return {
        ...state,
        isChangePasswordSuccess: false
      };
    default: return state;
  }
}
export default userReducers;