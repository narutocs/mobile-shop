import axiosClient from "./axiosClient";

class ProductApi {
  listNewProduct = () => {
    const url = "/product/list-new-product";
    return axiosClient.get(url);
  }
  getProduct = (id) => {
    const url = `product-color/${id}`
    return axiosClient.get(url);
  }
  listProduct = (params) => {
    const url = '/product';
    return axiosClient.get(url, {
      params
    })
  }
}

const productApi = new ProductApi();

export default productApi;