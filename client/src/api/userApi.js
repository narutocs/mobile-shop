import axiosClient from "./axiosClient";

class UserApi {
  forgotPassword = params => {
    const url = "/user/forgot-password";
    return axiosClient.post(url, params);
  }
  changePassword = params => {
    const url = "/user/change-password";
    return axiosClient.put(url, params);
  }
  getProfile = (id) => {
    const url = `/user/${id}`
    return axiosClient.get(url);
  }
  updateProfile = (id, params) => {
    const formData = new FormData();
    const keys = Object.keys(params);
    const values = Object.values(params);

    for (let i = 0; i < keys.length; i++) {
      formData.append(keys[i], values[i])
    }

    const url = `/user/${id}`
    return axiosClient.put(url, formData);
  }
  changePassword = (params) => {
    const url = "/user/change-password-user";
    return axiosClient.put(url, params);
  }
}

const userApi = new UserApi();

export default userApi;