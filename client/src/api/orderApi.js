import axiosClient from "./axiosClient";

class OrderApi {
  createOrder = params => {
    const url = "/order";
    return axiosClient.post(url, params);
  }
  getOrderByUser = (idUser, params) => {
    const url = `/order/${idUser}`
    return axiosClient.get(url, { params });
  }
  update = (id, params) => {
    const url = `/order/${id}`;
    return axiosClient.put(url, params)
  }
}

const orderApi = new OrderApi();

export default orderApi;