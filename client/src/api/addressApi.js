import axiosClient from "./axiosClient";

class AddressApi {
  getCity = () => {
    const url = "/city";
    return axiosClient.get(url);
  }
  getDistrict = (idCity) => {
    const url = `/district/${idCity}`;
    return axiosClient.get(url);
  }
  getVillage = (idDistrict) => {
    const url = `/village/${idDistrict}`;
    return axiosClient.get(url);
  }
}

const addressApi = new AddressApi();

export default addressApi;