import userApi from "../api/userApi";
import { actionSetMgsSuccess, actionSetMgsError, actionClearMgs } from "./message";
import { actionLoading, actionLoaded } from "./loading";
import * as ActionType from "../constants/ActionType";

export const actionForgotPassword = (email) => {
  return async dispatch => {
    try {
      dispatch(actionLoading());
      const result = await userApi.forgotPassword(email);
      const { message = '' } = result;
      dispatch(actionSetMgsSuccess(message))

      dispatch(actionLoaded())
    } catch (e) {
      dispatch(actionSetMgsError(e))
      dispatch(actionLoaded());

    }
  }
}

export const actionChangePassword = params => {
  return async dispatch => {
    try {
      dispatch(actionLoading());
      const result = await userApi.changePassword(params);
      const { message = '' } = result;
      dispatch(actionSetMgsSuccess(message))

      dispatch(actionLoaded())
    } catch (e) {
      dispatch(actionSetMgsError(e))
      dispatch(actionChangePasswordFail())
 
      dispatch(actionLoaded());
    }
  }
}


export const actionChangePasswordUser = (params) => {
  return async (dispatch, getState) => {
    try {

      dispatch(actionLoading());
      const result = await userApi.changePassword(params);

      const { message = '' } = result;
      dispatch(actionSetMgsSuccess(message))
 
      dispatch(actionLoaded())
    } catch (e) {
      dispatch(actionSetMgsError(e))

      dispatch(actionLoaded());
    }
  }
}


export const actionGetUser = () => {
  return async (dispatch, getState) => {
    try {
      const state = getState();
      const _id = state.auth.user._id;

      dispatch(actionLoading());
      const result = await userApi.getProfile(_id);

      const { message = '', user = {} } = result;
      dispatch(actionGetUserSuccess(user));

      dispatch(actionLoaded())
    } catch (e) {
      dispatch(actionSetMgsError(e))

      dispatch(actionLoaded());
    }
  }
}

export const actionUpdateUser = (params) => {
  return async (dispatch, getState) => {
    try {
      const state = getState();
      const _id = state.auth.user._id;
      dispatch(actionLoading());

      const result = await userApi.updateProfile(_id, params);

      const { message = '' } = result;
      dispatch(actionUpdateUserSuccess(params));
      dispatch(actionSetMgsSuccess(message))
 
      dispatch(actionLoaded())
    } catch (e) {
      dispatch(actionSetMgsError(e))

      dispatch(actionLoaded());
    }
  }
}

export const actionUpdateUserSuccess = user => {
  return {
    type: ActionType.UPDATE_USER_SUCCESS,
    payload: {
      user
    }
  }
}

export const actionGetUserSuccess = user => {
  return {
    type: ActionType.GET_USER_SUCCESS,
    payload: {
      user
    }
  }
}

export const actionGetUserFail = () => {
  return {
    type: ActionType.GET_USER_FAIL
  }
}

export const actionChangePasswordSuccess = (password) => {
  return {
    type: ActionType.CHANGE_PASSWORD_SUCCESS,
    payload: {
      password
    }
  }
}

export const actionChangePasswordFail = () => {
  return {
    type: ActionType.CHANGE_PASSWORD_FAIL
  }
}
