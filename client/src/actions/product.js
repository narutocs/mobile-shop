import * as ActionType from "../constants/ActionType";

import productApi from "../api/productApi";
import { actionClearMgs, actionSetMgsError } from "./message";
import { actionLoaded, actionLoading } from "./loading";

export const actionGetListProduct = (params) => {

  return async (dispatch) => {
    try {
      dispatch(actionLoading());
      const result = await productApi.listProduct(params);

      const { data: { listData = {} }, pagination = {} } = result;


      console.log("list data ", listData)
      dispatch(actionSearchProduct(listData, pagination));
      dispatch(actionClearMgs());
      dispatch(actionLoaded())
    } catch (e) {

      dispatch(actionSetMgsError(e))
      dispatch(actionClearMgs());
      dispatch(actionLoaded());
    }
  }
}

export const actionSearchProduct = (products, pagination) => {
  return {
    type: ActionType.SEARCH_PRODUCT,
    payload: {
      products,
      pagination
    }
  }
}

export const actionGetNewProduct = () => {
  return async (dispatch, getState) => {
    try {
      dispatch(actionLoading());
      const result = await productApi.listNewProduct();

      const { data: { listNewProduct = {} } } = result;

      dispatch(actionGetNewProductSuccess(listNewProduct));
      dispatch(actionClearMgs());
      dispatch(actionLoaded())
    } catch (e) {

      dispatch(actionSetMgsError(e))
      dispatch(actionClearMgs());
      dispatch(actionLoaded());
    }
  }
}

export const actionGetProduct = (id) => {
  return async (dispatch, getState) => {
    try {
      dispatch(actionLoading());
      const result = await productApi.getProduct(id);

      const { data: { product = {} } } = result;

      dispatch(actionGetProductSuccess(product));

      dispatch(actionClearMgs());
      dispatch(actionLoaded())
    } catch (e) {

      dispatch(actionSetMgsError(e))
      dispatch(actionClearMgs());
      dispatch(actionLoaded());
    }
  }
}

export const actionSelectedProduct = selected => {
  return {
    type: ActionType.SELECTED_PRODUCT_SUCCESS,
    payload: {
      selected
    }
  }
}

export const actionGetProductSuccess = product => {
  return {
    type: ActionType.GET_PRODUCT_SUCCESS,
    payload: {
      product
    }
  }
}

export const actionGetNewProductSuccess = products => {
  return {
    type: ActionType.GET_PRODUCT_NEW_SUCCESS,
    payload: {
      products
    }
  }
}