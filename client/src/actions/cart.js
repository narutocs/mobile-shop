import * as ActionType from "../constants/ActionType";
import productApi from "../api/productApi";
import { actionSetMgsError } from "./message";
import { Card, Rate, Button, Input, message } from "antd";
export const actionAddToCart = (product) => {
  return async (dispatch, getState) => {

    const { data } = await productApi.getProduct(product.product.productId._id);

    const state = getState();
    if (+data.product[0].productId.quality < +state.cart.product.length + product.quality) {
      message.error("Quality not enough")
      return
    }

    message.success("Add product success");

    dispatch({
      type: ActionType.ADD_CART_SUCCESS,
      payload: {
        product
      }
    })
  }

}

export const actionRemoveProductInCart = (product) => {
  return {
    type: ActionType.REMOVE_CART_SUCCESS,
    payload: {
      product
    }
  }
}