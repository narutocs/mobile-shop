import * as ActionType from "../constants/ActionType";
import addressApi from "../api/addressApi";

export const actionGetCity = () => {

  return async dispatch => {
    const { data } = await addressApi.getCity();
    dispatch(actionGetCityStore(data.listCity))
  }
}

export const actionGetDistrict = (idCity) => {
  console.log("zo action")
  return async dispatch => {
    const { data } = await addressApi.getDistrict(idCity);
    dispatch(actionGetDistrictStore(data.listDistrict))
  }
}

export const actionGetDistrictStore = (listDistrict) => {
  return {
    type: ActionType.GET_DISTRICT,
    payload: {
      data: listDistrict
    }
  }
}

export const actionGetVillage = (idDistrict) => {
  return async dispatch => {
    const { data } = await addressApi.getVillage(idDistrict);
    dispatch(actionGetVillageStore(data.listVillage))
  }
}

export const actionGetVillageStore = (listVillage) => {
  return {
    type: ActionType.GET_VILLAGE,
    payload: {
      data: listVillage
    }
  }
}

export const actionGetCityStore = (listCity) => {
  return {
    type: ActionType.GET_CITY,
    payload: {
      data: listCity
    }
  }
}

