import React from "react";
import PropTypes from "prop-types";
import { Card, Rate } from "antd";
import { Link } from "react-router-dom";

import { BASE_URL } from "../../constants";

Product.propTypes = {
  product: PropTypes.object,
};

Product.defaultProps = {
  product: {},
};

function Product(props) {
  const { product } = props;
  const { Meta } = Card;

  const { image = "", productId } = product;

  const { _id, name, price } = productId;

  const priceFormat = parseFloat((price + "").replace(/,/g, ""))
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");

  return (
    <Link
      style={{ textDecoration: "none" }}
      to={`/product-detail/${_id}`}
      title={name}
    >
      <Card
        hoverable
        style={{ width: 218 }}
        cover={
          <img
            style={{ width: 218, height: 218 }}
            alt="example"
            src={`${BASE_URL}${image}`}
          />
        }
      >
        <Meta title={name} />
        <Meta title={`${priceFormat} vnđ`} className="pt-3" />
        <div className="row" style={{ display: "flex", alignItems: "center" }}>
          <Rate disabled defaultValue={5} />
          <span className="ml-2">3 rates</span>
        </div>
      </Card>
    </Link>
  );
}

export default Product;
