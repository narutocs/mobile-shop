import React, { useState } from "react";
import PropTypes from "prop-types";
import { ShoppingCartOutlined, UserOutlined } from "@ant-design/icons";
import { Badge, Input, Layout } from "antd";
import { Link, useHistory } from "react-router-dom";

import Login from "../Login";
import CodeActive from "../CodeActive";
import Register from "../Register";
import ForgotPassword from "../ForgotPassword";
import ChangePassword from "../ForgotPassword/ChangePassword";
import Cart from "../Cart";

import "./style.scss";
import { useSelector, useDispatch } from "react-redux";
import { actionGetListProduct } from "../../actions/product";

Header.propTypes = {};

function Header(props) {
  const { Search } = Input;

  const { Header } = Layout;

  const { product } = useSelector((state) => state.cart);

  const dispatch = useDispatch();

  const history = useHistory();

  const arrQuality = product ? product.map((x) => x.quality) : [];
  const quality =
    arrQuality.length > 0 ? arrQuality.reduce((a, b) => a + b) : 0;

  const [isShowLogin, setIsShowLogin] = useState(false);
  const [isShowRegister, setIsShowRegister] = useState(false);
  const [isShowCodeActive, setIsShowCodeActive] = useState(false);
  const [currentEmail, setCurrentEmail] = useState(null);
  const [isLogin, setIsLogin] = useState(false);
  const [isShowForgotPassword, setIsShowForgotPassword] = useState(false);
  const [isShowChangePassword, setIsShowChangePassword] = useState(false);
  const [searchValue, setSearchValue] = useState("");

  const handleClick = () => {
    setIsShowLogin(true);
  };

  const handleCloseLogin = () => {
    setIsShowLogin(false);
  };

  const handleCloseRegister = () => {
    setIsShowRegister(false);
  };

  const handleShowLogin = () => {
    setIsShowLogin(true);
    setIsShowRegister(false);
    setIsShowCodeActive(false);
    setIsShowChangePassword(false);
  };

  const handleShowRegister = () => {
    setIsShowRegister(true);
    setIsShowCodeActive(false);
    setIsShowLogin(false);
  };

  const handleCloseCodeActive = () => {
    setIsShowCodeActive(false);
  };

  const handleShowCodeActive = (email, isLoginModel = false) => {
    setCurrentEmail(email);
    setIsShowCodeActive(true);
    setIsShowLogin(false);
    setIsShowRegister(false);
    if (isLoginModel) {
      setIsLogin(isLogin);
    }
  };

  const handShowForgotPassword = () => {
    setIsShowForgotPassword(true);
    setIsShowLogin(false);
  };

  const handleCloseForgotPassword = () => {
    setIsShowForgotPassword(false);
  };

  const handleCloseChangePassword = () => {
    setIsShowChangePassword(false);
  };

  const handleShowChangePassword = (email) => {
    setCurrentEmail(email);
    setIsShowChangePassword(true);
    setIsShowForgotPassword(false);
  };

  const handleChangeSearch = (e) => {
    setSearchValue(e.target.value);
  };

  const handleSearch = (e) => {
    e.preventDefault();
    dispatch(
      actionGetListProduct({
        search: searchValue,
      })
    );

    history.push(`/search-product/${searchValue ? searchValue :'all'  }`);
  };

  return (
    <Header className="header">
      <div className="header_right">
        <Link to="/">
          <img
            src="https://iweb.tatthanh.com.vn/pic/3/blog/images/logo-dien-thoai(35).jpg"
            className="logo"
          />
        </Link>
        <form onSubmit={handleSearch}>
          <Search
            value={searchValue}
            onChange={handleChangeSearch}
            name="search"
            placeholder="Search"
          />
        </form>
      </div>
      <div className="header_left">
        <div className="header_left-content">
          <div className="header_left-content-search">
            <Badge count={quality}>
              <ShoppingCartOutlined className="icon" />
            </Badge>
            <Link to="/cart">
              <span>Giỏ Hàng</span>
            </Link>
          </div>
          <div className="header_left-content-user" onClick={handleClick}>
            <UserOutlined className="icon" />
            <span>Đăng nhập/Đăng ký</span>
          </div>
          {isShowLogin && (
            <Login
              isShowModal={isShowLogin}
              onCloseModel={handleCloseLogin}
              onShowRegister={handleShowRegister}
              onShowCodeActive={handleShowCodeActive}
              onShowForgotPassword={handShowForgotPassword}
            />
          )}
          {isShowRegister && (
            <Register
              onShowLogin={handleShowLogin}
              isShowModal={isShowRegister}
              onCloseModel={handleCloseRegister}
              onShowCodeActive={handleShowCodeActive}
            />
          )}
          {isShowCodeActive && (
            <CodeActive
              isShowModal={isShowCodeActive}
              onCloseModel={handleCloseCodeActive}
              onShowLogin={handleShowLogin}
              email={currentEmail}
              isLogin={isLogin}
            />
          )}
          {isShowForgotPassword && (
            <ForgotPassword
              isShowModal={isShowForgotPassword}
              onCloseModel={handleCloseForgotPassword}
              onShowChangePassword={handleShowChangePassword}
            />
          )}

          {isShowChangePassword && (
            <ChangePassword
              isShowModal={isShowChangePassword}
              onCloseModel={handleCloseChangePassword}
              email={currentEmail}
              onShowLogin={handleShowLogin}
            />
          )}
        </div>
      </div>
    </Header>
  );
}

export default Header;
