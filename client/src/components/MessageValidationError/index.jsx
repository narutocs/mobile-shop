import React from "react";
import PropTypes from "prop-types";

import "./style.scss";

MessageValidationError.propTypes = {
  messageErr: PropTypes.string,
  touchedOop: PropTypes.bool,
};

MessageValidationError.defaultProps = {
  messageErr: "",
  touchedOop: false,
};

function MessageValidationError(props) {
  const { messageErr, touchedOop } = props;

  return messageErr && touchedOop ? (
    <span className="error-validate-form">{messageErr}</span>
  ) : (
    <></>
  );
}
export default MessageValidationError;
