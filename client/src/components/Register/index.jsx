import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { Modal, Input, Button } from "antd";
import { useFormik } from "formik";
import * as yup from "yup";
import { useDispatch, useSelector } from "react-redux";

import MessageValidationError from "../MessageValidationError";
import { actionRegister } from "../../actions/auth";
import Loading from "../Loading";

import "./style.scss";

Register.propTypes = {
  isShowModal: PropTypes.bool,
  onCloseModel: PropTypes.func,
  onShowLogin: PropTypes.func,
  onShowCodeActive: PropTypes.func,
};

Register.defaultProps = {
  isShowModal: false,
  onCloseModel: null,
  onShowLogin: null,
  onShowCodeActive: null,
};

function Register(props) {
  const { isShowModal, onCloseModel, onShowLogin, onShowCodeActive } = props;

  const isLoading = useSelector((state) => state.isLoading);
  const { isRegisterSuccess } = useSelector((state) => state.auth);

  const dispatch = useDispatch();

  const initialValues = {
    email: "",
    password: "",
    confirmPassword: "",
    fullName: "",
    phoneNumber: "",
  };

  const validationSchema = yup.object().shape({
    email: yup
      .string()
      .required("Please input your email")
      .matches(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        "Email incorrect format"
      ),
    password: yup
      .string()
      .required("Please input your password")
      .min(6, "Password must has length bigger 6 character"),
    confirmPassword: yup
      .string()
      .test("match", "Confirm password must like password", function (
        confirmPassword
      ) {
        if (confirmPassword && this.parent.password) {
          return confirmPassword === this.parent.password;
        }
      })
      .required("Please input your confirm password"),
    fullName: yup.string().required("Please input your full name"),
    phoneNumber: yup
      .string()
      .required("Please input your phone number")
      .matches(
        /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[0-9]*$/,
        "Phone number incorrect format"
      ),
  });

  const formik = useFormik({
    validationSchema,
    initialValues,
    onSubmit: async (values) => {
      await dispatch(actionRegister(values));
    },
  });

  const handleCancel = () => {
    if (onCloseModel && !isLoading) {
      onCloseModel();
    }
  };

  const handleShowLogin = () => {
    if (onShowLogin) {
      onShowLogin();
    }
  };

  const {
    touched,
    values,
    errors,
    handleBlur,
    handleSubmit,
    handleChange,
    setValues,
    setTouched,
    setErrors,
  } = formik;

  const resetForm = () => {
    setValues({
      email: "",
      password: "",
      confirmPassword: "",
      fullName: "",
      phoneNumber: "",
    });
    setTouched({});
    setErrors({});
  };

  useEffect(() => {
    resetForm();
  }, [isShowModal]);

  useEffect(() => {
    if (isRegisterSuccess) {
      if (onShowCodeActive) {
        onShowCodeActive(values.email);
      }
    }
  }, [isRegisterSuccess]);

  return (
    <Modal
      onCancel={handleCancel}
      footer={null}
      title="Register"
      visible={isShowModal}
      centered={true}
    >
      <Loading>
        <div className="container">
          <form onSubmit={handleSubmit}>
            <div className="form-group">
              <label htmlFor="name">Full Name</label>
              <Input
                id="name"
                type="text"
                name="fullName"
                value={values.fullName}
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <MessageValidationError
                touchedOop={touched.fullName}
                messageErr={errors.fullName}
              />
            </div>
            <div className="form-group">
              <label htmlFor="phone">Phone Number</label>
              <Input
                id="phone"
                type="text"
                name="phoneNumber"
                value={values.phoneNumber}
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <MessageValidationError
                touchedOop={touched.phoneNumber}
                messageErr={errors.phoneNumber}
              />
            </div>

            <div className="form-group">
              <label htmlFor="email">Email</label>
              <Input
                id="email"
                type="text"
                name="email"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
              />
              <MessageValidationError
                touchedOop={touched.email}
                messageErr={errors.email}
              />
            </div>
            <div className="form-group">
              <label htmlFor="password">Password</label>
              <Input.Password
                id="password"
                type="password"
                name="password"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
              />
            </div>
            <MessageValidationError
              touchedOop={touched.password}
              messageErr={errors.password}
            />
            <div className="form-group">
              <label htmlFor="confirmed">Confirmed Password</label>
              <Input.Password
                id="confirmed"
                type="password"
                name="confirmPassword"
                value={values.confirmPassword}
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </div>
            <MessageValidationError
              touchedOop={touched.confirmPassword}
              messageErr={errors.confirmPassword}
            />
            <div className="row mt-4">
              <div className="col-md-12 d-flex">
                <Button
                  htmlType="submit"
                  className="login-form-button flex-fill primary"
                >
                  Register
                </Button>
              </div>
            </div>
            <div className="row mt-3">
              <div className="col-md-5">Already have an account?</div>
              <span
                className="login-form-forgot register col-md-7"
                onClick={handleShowLogin}
              >
                Login
              </span>
            </div>
          </form>
        </div>
      </Loading>
    </Modal>
  );
}

export default Register;
