import React from "react";
import PropTypes from "prop-types";
import { ShoppingCartOutlined } from "@ant-design/icons";
import { Badge, Dropdown, Empty } from "antd";

Cart.propTypes = {};

function Cart(props) {
  const menu = <Empty />;

  return (
    <Dropdown overlay={menu}>
      <div>
        <Badge count={5}>
          <ShoppingCartOutlined className="icon" />
        </Badge>
        <span className="ml-2">Giỏ Hàng</span>
      </div>
    </Dropdown>
  );
}

export default Cart;
