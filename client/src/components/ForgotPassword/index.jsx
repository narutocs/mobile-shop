import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { Modal, Input, Button } from "antd";
import { useFormik } from "formik";
import * as yup from "yup";
import { useDispatch, useSelector } from "react-redux";

import MessageValidationError from "../MessageValidationError";
import MessageType from "../../constants/MessageType";
import Loading from "../Loading";
import { actionForgotPassword } from "../../actions/user";

import "./style.scss";

ForgotPassword.propTypes = {
  isShowModal: PropTypes.bool,
  onCloseModel: PropTypes.func,
  onShowChangePassword: PropTypes.func,
};

ForgotPassword.defaultProps = {
  isShowModal: false,
  onCloseModel: null,
  onShowChangePassword: null,
};
function ForgotPassword(props) {
  const { isShowModal, onCloseModel, onShowChangePassword } = props;

  const isLoading = useSelector((state) => state.isLoading);
  const { type } = useSelector((state) => state.message);

  const dispatch = useDispatch();

  const initialValues = {
    email: "",
  };

  const validationSchema = yup.object().shape({
    email: yup
      .string()
      .required("Please input your email")
      .matches(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        "Email incorrect format"
      ),
  });

  const formik = useFormik({
    validationSchema,
    initialValues,
    onSubmit: async (values) => {
      await dispatch(actionForgotPassword(values));
    },
  });

  const {
    touched,
    values,
    errors,
    handleBlur,
    handleSubmit,
    handleChange,
    setValues,
    setTouched,
    setErrors,
  } = formik;

  const handleCancel = () => {
    if (onCloseModel && !isLoading) {
      onCloseModel();
    }
  };

  const resetForm = () => {
    setValues({
      email: "",
    });
    setTouched({});
    setErrors({});
  };

  useEffect(() => {
    resetForm();
  }, [isShowModal]);

  useEffect(() => {
    if (type === MessageType.SUCCESS) {
      if (onShowChangePassword) {
        onShowChangePassword(formik.values.email);
      }
    }
  }, [type]);
  return (
    <Modal
      onCancel={handleCancel}
      footer={null}
      title="Forgot Password"
      visible={isShowModal}
      centered={true}
    >
      <Loading>
        <div className="container">
          <form onSubmit={handleSubmit}>
            <div className="form-group">
              <label htmlFor="email">Email</label>
              <Input
                id="email"
                type="text"
                name="email"
                value={values.email}
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <MessageValidationError
                touchedOop={touched.email}
                messageErr={errors.email}
              />
            </div>

            <div className="row mt-4">
              <div className="col-md-12 d-flex">
                <Button
                  htmlType="submit"
                  className="login-form-button flex-fill primary"
                >
                  Send
                </Button>
              </div>
            </div>
          </form>
        </div>
      </Loading>
    </Modal>
  );
}

export default ForgotPassword;
