import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { Modal, Input, Button } from "antd";
import { useFormik } from "formik";
import * as yup from "yup";
import { useDispatch, useSelector } from "react-redux";

import MessageValidationError from "../../MessageValidationError";
import MessageType from "../../../constants/MessageType";
import Loading from "../../Loading";
import { actionChangePassword } from "../../../actions/user";

ChangePassword.propTypes = {
  isShowModal: PropTypes.bool,
  onCloseModel: PropTypes.func,
  email: PropTypes.string,
  onShowLogin: PropTypes.func,
};

ChangePassword.defaultProps = {
  isShowModal: null,
  onCloseModel: null,
  email: "",
  onShowLogin: null,
};

function ChangePassword(props) {
  const { isShowModal, onCloseModel, email, onShowLogin } = props;

  const isLoading = useSelector((state) => state.isLoading);
  const isChangePasswordSuccess = useSelector(
    (state) => state.user.isChangePasswordSuccess
  );

  const dispatch = useDispatch();

  const initialValues = {
    code: "",
    password: "",
    confirmPassword: "",
  };

  const validationSchema = yup.object().shape({
    code: yup.number().required("Code is required"),
    password: yup
      .string()
      .required("Please input your password")
      .min(6, "Password must has length bigger 6 character"),
    confirmPassword: yup
      .string()
      .test("match", "Confirm password must like password", function (
        confirmPassword
      ) {
        if (confirmPassword && this.parent.password) {
          return confirmPassword === this.parent.password;
        }
      })
      .required("Please input your confirm password"),
  });

  const formik = useFormik({
    validationSchema,
    initialValues,
    onSubmit: async (values) => {
      await dispatch(
        actionChangePassword({
          ...values,
          email,
        })
      );
    },
  });

  const {
    touched,
    values,
    errors,
    handleBlur,
    handleSubmit,
    handleChange,
    setValues,
    setTouched,
    setErrors,
  } = formik;

  const handleCancel = () => {
    if (onCloseModel && !isLoading) {
      onCloseModel();
    }
  };

  const resetForm = () => {
    setValues({
      code: "",
      password: "",
      confirmPassword: "",
    });
    setTouched({});
    setErrors({});
  };

  useEffect(() => {
    resetForm();
  }, [isShowModal]);

  useEffect(() => {
    if (isChangePasswordSuccess) {
      if (onShowLogin) {
        onShowLogin();
      }
    }
  }, [isChangePasswordSuccess]);

  return (
    <Modal
      onCancel={handleCancel}
      footer={null}
      title="Change Password"
      visible={isShowModal}
      centered={true}
    >
      <Loading>
        <div className="container">
          <form onSubmit={handleSubmit}>
            <div className="form-group">
              <label htmlFor="code">Code</label>
              <Input
                id="code"
                type="text"
                name="code"
                value={values.code}
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <MessageValidationError
                touchedOop={touched.code}
                messageErr={errors.code}
              />
            </div>
            <div className="form-group">
              <label htmlFor="password">Password</label>
              <Input.Password
                id="password"
                type="password"
                name="password"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
              />
            </div>
            <MessageValidationError
              touchedOop={touched.password}
              messageErr={errors.password}
            />
            <div className="form-group">
              <label htmlFor="confirmed">Confirmed Password</label>
              <Input.Password
                id="confirmed"
                type="password"
                name="confirmPassword"
                value={values.confirmPassword}
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </div>
            <MessageValidationError
              touchedOop={touched.confirmPassword}
              messageErr={errors.confirmPassword}
            />
            <div className="row mt-4">
              <div className="col-md-12 d-flex">
                <Button
                  htmlType="submit"
                  className="login-form-button flex-fill primary"
                >
                  OK
                </Button>
              </div>
            </div>
          </form>
        </div>
      </Loading>
    </Modal>
  );
}

export default ChangePassword;
