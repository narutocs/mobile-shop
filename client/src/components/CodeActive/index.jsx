import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { Modal, Input, Button } from "antd";
import { useFormik } from "formik";
import MessageValidationError from "../MessageValidationError";
import * as yup from "yup";
import { useDispatch, useSelector } from "react-redux";

import Loading from "../Loading";
import { actionActiveAccount } from "../../actions/auth";
import { actionLoginFail } from "../../actions/auth";

import "./style.scss";

CodeActive.propTypes = {
  isShowModal: PropTypes.bool,
  onCloseModel: PropTypes.func,
  email: PropTypes.string.isRequired,
  onShowLogin: PropTypes.func,
  isLogin: PropTypes.bool,
};

CodeActive.defaultProps = {
  isShowModal: false,
  onCloseModel: null,
  onShowLogin: null,
  isLogin: false,
};

function CodeActive(props) {
  const { isShowModal, onCloseModel, email, onShowLogin, isLogin } = props;

  const isLoading = useSelector((state) => state.isLoading);

  const isActiveSuccess = useSelector((state) => state.active);

  const { token, user } = useSelector((state) => state.auth);

  const dispatch = useDispatch();

  const handleCancel = () => {
    if (onCloseModel && !isLoading) {
      if (user && !user.isActive) {
        dispatch(actionLoginFail());
      }
      onCloseModel();
    }
  };

  const initialValues = {
    code: "",
  };

  const validationSchema = yup.object().shape({
    code: yup.number().required("Please input your code"),
  });

  const formik = useFormik({
    validationSchema,
    initialValues,
    onSubmit: async (values) => {
      await dispatch(actionActiveAccount(email, values.code));
    },
  });

  const {
    values,
    handleBlur,
    handleChange,
    handleSubmit,
    errors,
    touched,
  } = formik;

  const resetForm = () => {
    formik.setValues({
      code: "",
    });
    formik.setTouched({});
    formik.setErrors({});
  };

  useEffect(() => {
    resetForm();
  }, [isShowModal]);

  useEffect(() => {
    if (isActiveSuccess && !isLogin) {
      if (!isLogin && onShowLogin) {
        onShowLogin();
      } else {
        onCloseModel();
      }
    }
  }, [isActiveSuccess]);

  return (
    <Modal
      onCancel={handleCancel}
      footer={null}
      title="Active Account"
      visible={isShowModal}
    >
      <Loading>
        <form onSubmit={handleSubmit}>
          <div className="row mb-4">
            <div className="col-md-12">
              <Input
                className={` ${
                  errors.code && touched.code ? "validate-form-error" : ""
                }`}
                name="code"
                placeholder="Code Active"
                value={values.code}
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <MessageValidationError
                touchedOop={touched.code}
                messageErr={errors.code}
              />
            </div>
          </div>
          <div className="row">
            <div className="col-md-12 d-flex">
              <Button
                htmlType="submit"
                className="login-form-button flex-fill primary"
              >
                Active
              </Button>
            </div>
          </div>
        </form>
      </Loading>
    </Modal>
  );
}

export default CodeActive;
