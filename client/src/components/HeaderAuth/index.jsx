import React, { useState } from "react";
import PropTypes from "prop-types";
import { ShoppingCartOutlined, UserOutlined } from "@ant-design/icons";
import { Badge, Avatar, Input, Layout, Dropdown, Menu } from "antd";
import { useSelector, useDispatch } from "react-redux";
import { Link, useHistory } from "react-router-dom";

import { actionLogout } from "../../actions/auth";
import { actionGetListProduct } from "../../actions/product";

HeaderAuth.propTypes = {};

function HeaderAuth(props) {
  const { Search } = Input;

  const [searchValue, setSearchValue] = useState("");

  const { Header } = Layout;

  const history = useHistory();

  const dispatch = useDispatch();

  const { token, user } = useSelector((state) => state.auth);

  const { product = [] } = useSelector((state) => state.cart);

  const arrQuality = product ? product.map((x) => x.quality) : [];
  const quality =
    arrQuality.length > 0 ? arrQuality.reduce((a, b) => a + b) : 0;

  const handleLogout = () => {
    dispatch(actionLogout());
    history.push("/");
  };

  const handleChangeSearch = (e) => {
    setSearchValue(e.target.value);
  };

  const handleSearch = (e) => {
    e.preventDefault();
    dispatch(
      actionGetListProduct({
        search: searchValue,
      })
    );

    history.push(`/search-product/${searchValue ? searchValue : "all"}`);
  };

  const menu = (
    <Menu>
      <Menu.Item>
        <Link to="/profile">Profile</Link>
      </Menu.Item>
      <Menu.Item>
        <Link to="/change-password">Change Password</Link>
      </Menu.Item>
      <Menu.Item>
        <Link to="/my-order">My orders</Link>
      </Menu.Item>
      <Menu.Item onClick={handleLogout}>Logout</Menu.Item>
    </Menu>
  );
  return (
    <Header className="header">
      <div className="header_right">
        <Link to="/">
          <img
            src="https://iweb.tatthanh.com.vn/pic/3/blog/images/logo-dien-thoai(35).jpg"
            className="logo"
          />
        </Link>
        <form onSubmit={handleSearch}>
          <Search
            value={searchValue}
            onChange={handleChangeSearch}
            name="search"
            placeholder="Search"
          />
        </form>
      </div>
      <div className="header_left">
        <div className="header_left-content">
          <div className="header_left-content-search">
            <Badge count={quality}>
              <ShoppingCartOutlined className="icon" />
            </Badge>
            <Link to="/cart">
              <span>Giỏ Hàng</span>
            </Link>
          </div>
          <div className="header_left-content-user">
            <Dropdown overlay={menu}>
              <div>
                <UserOutlined className="icon" />
                <span>{user.fullName}</span>
              </div>
            </Dropdown>
          </div>
        </div>
      </div>
    </Header>
  );
}

export default HeaderAuth;
