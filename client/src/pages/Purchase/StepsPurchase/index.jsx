import React from "react";
import PropTypes from "prop-types";
import { Steps, Card, Rate, Button, Input, message } from "antd";
import {
  UserOutlined,
  LoadingOutlined,
  SmileOutlined,
  SolutionOutlined,
} from "@ant-design/icons";

import "./style.scss";

StepsPurchase.propTypes = {
  current: PropTypes.number,
};

StepsPurchase.defaultProps = {
  current: 1,
};

function StepsPurchase(props) {
  const { Step } = Steps;

  const { current } = props;

  return (
    <Steps size="small" current={current}>
      <Step title="Login" />
      <Step title="Information" />
      <Step title="Payment" />
    </Steps>
  );
}

export default StepsPurchase;
