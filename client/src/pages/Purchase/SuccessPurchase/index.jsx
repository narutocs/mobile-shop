import React from "react";
import { Result, Button } from "antd";
import { useHistory } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

function SuccessPurchase(props) {
  const history = useHistory();

  const { orderId } = useSelector((state) => state.order);

  const handleGoToHome = () => {
    history.push("/");
  };

  return (
    <Result
      status="success"
      title="Successfully Purchased product"
      subTitle={`Order number: ${orderId} Admin configuration takes 1-5 minutes, please wait.`}
      extra={[
        <Button type="primary" key="console" onClick={handleGoToHome}>
          Go to home
        </Button>,
      ]}
    />
  );
}

export default SuccessPurchase;
