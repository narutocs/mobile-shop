import React, { useRef, useState } from "react";
import { Card, Rate, Button, Input, message, Select, Radio } from "antd";
import PropTypes from "prop-types";
import { CreditCardOutlined, DollarCircleOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";

import { actionClearMgs, actionSetMgsError } from "../../../actions/message";
import { actionCreateOrder } from "../../../actions/order";

import paypalIcon from "../../../assets/images/paypal.png";
import codIcon from "../../../assets/images/cod.png";

import "./style.scss";
import Paypal from "../../../components/Paypal";

PaymentPurchase.propTypes = {
  infoUser: PropTypes.object,
  onChangeStep: PropTypes.func,
};

PaymentPurchase.defaultProps = {
  infoUser: {},
  onChangeStep: null,
};

function PaymentPurchase(props) {
  const { infoUser, onChangeStep } = props;

  const [paymentMethod, setPaymentMethod] = useState("");

  const dispatch = useDispatch();

  const { list_city, list_district, list_village } = useSelector(
    (state) => state.address
  );

  const summary = useRef(0);

  let index_city = 0;

  let index_district = 0;

  let index_village = 0;

  if (infoUser) {
    index_city = list_city.findIndex((x) => x.id === infoUser.city);

    index_district = list_district.findIndex((x) => x.id === infoUser.district);

    index_village = list_village.findIndex((x) => x.id === infoUser.village);
  }

  const { product } = useSelector((state) => state.cart);
  let arrQuality;
  let quality;
  if (product.length > 0) {
    arrQuality = product.map((x) => x.quality);
    quality = arrQuality.reduce((a, b) => a + b);
  }

  const handleChangeInfo = () => {
    if (onChangeStep) {
      onChangeStep(1, infoUser);
    }
  };

  console.log("info: ", infoUser);

  const priceFormat = (price) => {
    return parseFloat((price + "").replace(/,/g, ""))
      .toString()
      .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };

  const handlePayment = async () => {
    if (!paymentMethod) {
      dispatch(actionSetMgsError("Please choose payment method"));

      setTimeout(() => {
        dispatch(actionClearMgs());
      }, 500);
      return;
    }

    await dispatch(
      actionCreateOrder({
        idUser: infoUser._id,
        status: "Pending",
        address: infoUser.address,
        city: list_city[index_city],
        district: list_district[index_district],
        village: list_village[index_village],
        cart: product,
      })
    );

    if (onChangeStep) {
      onChangeStep(3);
    }
  };

  const handleChangePayment = (e) => {
    setPaymentMethod(e.target.id);
  };

  const handleSuccess = async (payment) => {
    await dispatch(
      actionCreateOrder({
        idUser: infoUser._id,
        status: "Pending",
        address: infoUser.address,
        city: list_city[index_city],
        district: list_district[index_district],
        village: list_village[index_village],
        cart: product,
      })
    );
  };

  return (
    <div className="row">
      <div className="col-md-12">
        <div className="row mt-3">
          <div className="col-8">
            <div style={{ fontSize: "2.4rem", fontWeight: "bold" }}>
              payment methods
            </div>
            <div className="mt-3">
              <Radio.Group
                className="d-flex"
                onChange={handleChangePayment}
                value={paymentMethod}
              >
                <label htmlFor="paypal">
                  <Card
                    hoverable
                    style={{
                      width: 200,
                      height: 100,
                      border: "2px solid",
                      borderColor: "#1890FF",
                    }}
                  >
                    <Radio id="paypal" name="paypal" value="paypal"></Radio>
                    <CreditCardOutlined
                      style={{ fontSize: "2.6rem" }}
                      className="pr-3"
                    />
                    <img style={{ width: 86, height: 40 }} src={paypalIcon} />
                  </Card>
                </label>

                <label htmlFor="cod">
                  <Card
                    className="ml-5"
                    hoverable
                    style={{
                      width: 200,
                      height: 100,
                      border: "2px solid",
                      borderColor: "#1890FF",
                    }}
                  >
                    <Radio id="cod" name="cod" value="cod"></Radio>
                    <DollarCircleOutlined
                      style={{ fontSize: "2.6rem" }}
                      className="pr-3"
                    />
                    <img
                      style={{ width: 86, height: 60, objectFit: "fill" }}
                      src={codIcon}
                    />
                  </Card>
                </label>
              </Radio.Group>

              {paymentMethod === "paypal" ? (
                <div className="btn-payment btn-payment-paypal">
                  <Paypal
                    onSuccess={handleSuccess}
                    total={Math.ceil(+summary.current * 0.000043)}
                  />
                </div>
              ) : (
                <Button
                  className="btn-payment"
                  type="primary"
                  onClick={handlePayment}
                >
                  Payment
                </Button>
              )}
            </div>
          </div>

          <div className="col-md-4">
            <Card
              style={{
                width: "100%",
                borderWidth: 2,
              }}
            >
              <div className="row">
                <div className="col-md-12">
                  <div className="d-flex justify-content-between align-items-center">
                    <div className="info-title">Info User</div>
                    <Button onClick={handleChangeInfo}>Change</Button>
                  </div>
                  <hr />
                  <div>
                    <div className="name">
                      Name: {infoUser && infoUser.fullName}
                    </div>
                    <div className="name">
                      Address: {list_city[index_city].name}
                      {", "}
                      {list_district[index_district].name}
                      {", "}
                      {list_village[index_village].name}
                      {", "}
                      {infoUser && infoUser.address}
                    </div>
                    <div className="name">
                      Phone number: {infoUser && infoUser.phoneNumber}
                    </div>
                  </div>
                </div>
              </div>
            </Card>

            <Card
              className="mt-5"
              style={{
                width: "100%",
                borderWidth: 2,
              }}
            >
              <div className="row">
                <div className="col-md-12">
                  <div className="info-title">Cart: {quality} products</div>
                  <hr />
                  <div>
                    {product &&
                      product.map((item, index) => {
                        return (
                          <>
                            <div className="d-flex justify-content-between">
                              <span>
                                {item.quality}x {item.product.productId.name}
                              </span>
                              <span>
                                {priceFormat(item.product.productId.price)} vnđ
                              </span>
                            </div>
                            <label hidden>
                              {index === 0
                                ? (summary.current =
                                    0 +
                                    +item.product.productId.price *
                                      +item.quality)
                                : (summary.current =
                                    summary.current +
                                    +item.product.productId.price *
                                      +item.quality)}
                            </label>
                            <hr />
                            {index === product.length - 1 && (
                              <div className="d-flex justify-content-between">
                                <span>Summary:</span>
                                <span>{priceFormat(summary.current)} vnđ</span>
                              </div>
                            )}
                          </>
                        );
                      })}
                  </div>
                </div>
              </div>
            </Card>
          </div>
        </div>
      </div>
    </div>
  );
}

export default PaymentPurchase;
