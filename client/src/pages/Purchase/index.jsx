import React, { useState } from "react";
import PropTypes from "prop-types";
import { Card, Rate, Button, Input, message } from "antd";

import InfoPurchase from "./InfoPurchase";
import StepsPurchase from "./StepsPurchase";
import PaymentPurchase from "./PaymentPurchase";
import SuccessPurchase from "./SuccessPurchase";

Purchase.propTypes = {};

function Purchase(props) {
  const [currentStep, setCurrentStep] = useState(1);
  const [infoUser, setInfoUser] = useState(null);

  const handleChangeStep = (step, values = null) => {
    setCurrentStep(step);

    setInfoUser(values);
  };

  const renderContent = () => {
    switch (currentStep) {
      case 1:
        return (
          <InfoPurchase onChangeStep={handleChangeStep} infoUser={infoUser} />
        );
      case 2:
        return (
          <PaymentPurchase
            onChangeStep={handleChangeStep}
            infoUser={infoUser}
          />
        );
      case 3:
        return <SuccessPurchase />;
    }
  };

  return (
    <Card style={{ width: "100%", minHeight: 500, minWidth: 810 }}>
      <div className="row">
        <div className="col-md-12">
          <StepsPurchase current={currentStep} />
        </div>
      </div>
      {renderContent()}
    </Card>
  );
}

export default Purchase;
