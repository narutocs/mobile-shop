import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { useFormik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import * as yup from "yup";
import { Card, Rate, Button, Input, message, Select } from "antd";

import MessageValidationError from "../../../components/MessageValidationError";

import "./style.scss";
import {
  actionGetCity,
  actionGetDistrict,
  actionGetVillage,
} from "../../../actions/address";

InfoPurchase.propTypes = {
  onChangeStep: PropTypes.func,
  infoUser: PropTypes.object,
};

InfoPurchase.defaultProps = {
  onChangeStep: null,
  infoUser: {},
};

function InfoPurchase(props) {
  const { onChangeStep, infoUser } = props;

  const { Option } = Select;

  const initialValues = {
    address: "",
    city: "",
    district: "",
    village: "",
  };

  const validationSchema = yup.object().shape({
    address: yup.string().required("Please input your address"),
    city: yup.string().required("Please input your city"),
    district: yup.string().required("Please input your district"),
    village: yup.string().required("Please input your village"),
  });

  const formik = useFormik({
    validationSchema,
    initialValues,
    onSubmit: (values) => {
      if (onChangeStep) {
        onChangeStep(2, {
          ...values,
          ...user,
        });
      }
    },
  });

  const {
    touched,
    values,
    errors,
    handleBlur,
    handleSubmit,
    handleChange,
    setValues,
    setTouched,
    setErrors,
  } = formik;

  const resetForm = () => {
    setValues({
      address: "",
      city: "",
      district: "",
      village: "",
    });
    setTouched({});
    setErrors({});
  };

  const dispatch = useDispatch();

  const list_city = useSelector((state) => state.address.list_city);

  const list_district = useSelector((state) => state.address.list_district);

  const list_village = useSelector((state) => state.address.list_village);

  const { user } = useSelector((state) => state.auth);

  useEffect(() => {
    dispatch(actionGetCity());
  }, []);

  const handleChangeCity = (values) => {
    handleChange({
      target: {
        name: "city",
        value: values,
      },
    });
    if (values !== -1) {
      dispatch(actionGetDistrict(values));
    }
  };

  const handleChangeDistrict = (values) => {
    handleChange({
      target: {
        name: "district",
        value: values,
      },
    });
    if (values !== -1) {
      dispatch(actionGetVillage(values));
    }
  };

  const handleChangeVillage = (values) => {
    handleChange({
      target: {
        name: "village",
        value: values,
      },
    });
  };

  useEffect(() => {
    if (infoUser) {
      formik.setFieldValue("address", infoUser.address);
      formik.setFieldValue("city", infoUser.city);
      formik.setFieldValue("district", infoUser.district);
      formik.setFieldValue("village", infoUser.village);
    }
  }, [infoUser]);

  return (
    <div
      className="container d-flex justify-content-center mt-5 flex-column align-items-center"
      style={{ minWidth: 700 }}
    >
      {/* <div className="row mb-5">
        <div
          className="col-md-12"
          style={{ fontSize: "2.4rem", fontWeight: "bold" }}
        >
          USER INFO
        </div>
      </div> */}
      <form onSubmit={handleSubmit}>
        <div className="row" style={{ minWidth: 810 }}>
          <div className="col-md-12">
            <div className="form-group row">
              <label className="col-md-3" htmlFor="name">
                Full Name
              </label>
              <Input
                disabled
                className="col-md-9"
                id="name"
                type="text"
                name="fullName"
                value={user.fullName}
              />
            </div>

            <div className="form-group row">
              <label className="col-md-3" htmlFor="phone">
                Phone Number
              </label>
              <Input
                disabled
                className="col-md-9"
                id="phone"
                type="text"
                name="phoneNumber"
                value={user.phoneNumber}
              />
            </div>

            <div className="form-group row">
              <label className="col-md-3" htmlFor="email">
                Email
              </label>
              <Input
                disabled
                className="col-md-9"
                id="email"
                type="text"
                name="email"
                value={user.email}
              />
            </div>

            <div className="row form-group">
              <label className="col-md-3" htmlFor="phone">
                City
              </label>
              <Select
                showSearch
                style={{ width: 200 }}
                optionFilterProp="children"
                name="city"
                value={values.city}
                onChange={handleChangeCity}
                onBlur={handleBlur}
                filterOption={(input, option) =>
                  option.children.toLowerCase().indexOf(input.toLowerCase()) >=
                  0
                }
              >
                <Option value="">Select a city</Option>
                {list_city.length &&
                  list_city.map((city) => {
                    return <Option value={city.id}>{city.name}</Option>;
                  })}
              </Select>
              <MessageValidationError
                touchedOop={touched.city}
                messageErr={errors.city}
              />
            </div>

            <div className="row form-group">
              <label className="col-md-3" htmlFor="phone">
                District
              </label>
              <Select
                showSearch
                style={{ width: 200 }}
                optionFilterProp="children"
                name="district"
                value={values.district}
                onChange={handleChangeDistrict}
                onBlur={handleBlur}
                filterOption={(input, option) =>
                  option.children.toLowerCase().indexOf(input.toLowerCase()) >=
                  0
                }
              >
                <Option value="">Select a district</Option>
                {list_district &&
                  list_district.map((district) => {
                    return <Option value={district.id}>{district.name}</Option>;
                  })}
              </Select>
              <MessageValidationError
                touchedOop={touched.district}
                messageErr={errors.district}
              />
            </div>

            <div className="row form-group">
              <label className="col-md-3" htmlFor="phone">
                Village
              </label>
              <Select
                showSearch
                style={{ width: 200 }}
                optionFilterProp="children"
                name="village"
                value={values.village}
                onChange={handleChangeVillage}
                onBlur={handleBlur}
                filterOption={(input, option) =>
                  option.children.toLowerCase().indexOf(input.toLowerCase()) >=
                  0
                }
              >
                <Option value="">Select a village</Option>
                {list_village &&
                  list_village.map((village) => {
                    return <Option value={village.id}>{village.name}</Option>;
                  })}
              </Select>
              <MessageValidationError
                touchedOop={touched.village}
                messageErr={errors.village}
              />
            </div>

            <div className="form-group row">
              <label className="col-md-3" htmlFor="phone">
                Address
              </label>
              <Input
                className="col-md-9"
                id="phone"
                type="text"
                name="address"
                value={values.address}
                onChange={handleChange}
                onBlur={handleBlur}
              />
              <MessageValidationError
                touchedOop={touched.address}
                messageErr={errors.address}
              />
            </div>

            <div className="row mt-5">
              <div className="col-md-12  d-flex justify-content-center">
                <Button style={{ width: 200 }} type="primary" htmlType="submit">
                  Continue
                </Button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
}

export default InfoPurchase;
