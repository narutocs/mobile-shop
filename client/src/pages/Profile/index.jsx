import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Card, Input, Button, Radio, DatePicker, Space } from "antd";
import { useFormik } from "formik";
import * as yup from "yup";
import { PlusOutlined } from "@ant-design/icons";
import moment from "moment";

import { actionGetUser, actionUpdateUser } from "../../actions/user";
import { actionSetMgsError } from "../../actions/message";
import MessageValidationError from "../../components/MessageValidationError";

import DefaultAvatar from "../../assets/images/default-avatar.jpg";
import { BASE_URL } from "../../constants";
import "./style.scss";

Profile.propTypes = {};

const dateFormat = "DD/MM/YYYY";

function Profile(props) {
  const { user } = useSelector((state) => state.auth);
  console.log(user);
  const [isShowProgress, setIsShowProgress] = useState(false);
  const [valueProgress, setValueProgress] = useState(1);
  const [imageShowClient, setImageShowClient] = useState(DefaultAvatar);

  const dispatch = useDispatch();

  useEffect(() => {
    const {
      email = "",
      dateOfBirth = moment(new Date(), "DD/MM/YYYY"),
      avatar = "",
      fullName = "",
      gender = 2,
      phoneNumber = "",
    } = user;

    formik.setValues({
      email,
      dateOfBirth,
      avatar,
      fullName,
      gender,
      phoneNumber,
    });
    setImageShowClient(`${BASE_URL}${avatar}`);
  }, [user]);

  const handleOnChange = async (e) => {
    const { target: { files = [] } = {} } = e;
    const fileUpload = files[0];
    if (fileUpload.type.indexOf("image") === -1) {
      dispatch(actionSetMgsError("The file is not a image"));
    }
    let reader = new FileReader();
    reader.readAsDataURL(fileUpload);
    reader.onload = (event) => {
      if (event.target) {
        setImageShowClient(event.target.result);
        formik.setFieldValue("avatar", fileUpload);
      }
    };
  };

  const initialValues = {
    email: "",
    fullName: "",
    phoneNumber: "",
    avatar: "",
    gender: 2,
    dateOfBirth: "",
  };

  const validationSchema = yup.object().shape({
    email: yup
      .string()
      .required("Please input your email")
      .matches(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        "Email incorrect format"
      ),
    fullName: yup.string().required("Please input your full name"),
    phoneNumber: yup
      .string()
      .required("Please input your phone number")
      .matches(
        /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[0-9]*$/,
        "Phone number incorrect format"
      ),
    avatar: yup.string(),
    gender: yup.number().required("Please select your gender"),
    dateOfBirth: yup
      .string()
      .required("Please select your date of birth")
      .matches(
        /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/,
        "Date of birth incorrect format"
      ),
  });

  const formik = useFormik({
    validationSchema,
    initialValues,
    onSubmit: async (values) => {
      dispatch(actionUpdateUser(values));
    },
  });

  const {
    touched,
    values,
    errors,
    handleBlur,
    handleSubmit,
    handleChange,
    setValues,
    setTouched,
    setErrors,
  } = formik;

  const resetForm = () => {
    setValues({
      email: "",
      fullName: "",
      phoneNumber: "",
      avatar: "",
      gender: 2,
      dateOfBirth: "",
    });
    setTouched({});
    setErrors({});
  };

  const handleChangeDate = (date, dateString) => {
    formik.setFieldValue("dateOfBirth", dateString);
  };

  useEffect(() => {
    if (user) {
      const getProfile = async () => {
        await dispatch(actionGetUser());
      };
      getProfile();
    }
  }, []);

  return (
    <Card
      style={{ width: "100%" }}
      style={{ display: "flex", justifyContent: "center" }}
    >
      <form onSubmit={handleSubmit}>
        <div className="row mb-3">
          <div className="col-md-12 d-flex">
            <img className="img-preview mr-5" src={imageShowClient} />
            <div className="d-flex flex-column justify-content-center">
              <span className="mb-2">Upload your avatar</span>
              <label htmlFor="upload">
                <span className="btn btn-success btn-size d-flex justify-content-center align-items-center">
                  <PlusOutlined className="pr-3" />
                  <span className="btn-size"> Upload</span>
                </span>
                <input
                  name="avatar"
                  type="file"
                  id="upload"
                  className="form-control d-none"
                  onChange={handleOnChange}
                  accept="image/*"
                />
              </label>
              <MessageValidationError
                touchedOop={touched.avatar}
                messageErr={errors.avatar}
              />
            </div>
          </div>
        </div>

        <div className="form-group row">
          <label className="col-md-3" htmlFor="name">
            Full Name
          </label>
          <Input
            className="col-md-9"
            id="name"
            type="text"
            name="fullName"
            value={values.fullName}
            onChange={handleChange}
            onBlur={handleBlur}
          />
          <MessageValidationError
            touchedOop={touched.fullName}
            messageErr={errors.fullName}
          />
        </div>
        <div className="form-group row">
          <label className="col-md-3" htmlFor="phone">
            Phone Number
          </label>
          <Input
            className="col-md-9"
            id="phone"
            type="text"
            name="phoneNumber"
            value={values.phoneNumber}
            onChange={handleChange}
            onBlur={handleBlur}
          />
          <MessageValidationError
            touchedOop={touched.phoneNumber}
            messageErr={errors.phoneNumber}
          />
        </div>

        <div className="row mt-4 mb-4 form-group d-flex">
          <label htmlFor="phone" className="mr-5 col-md-3">
            Gender
          </label>
          <Radio.Group
            className="col-md-9"
            name="gender"
            value={values.gender}
            onChange={handleChange}
            onBlur={handleBlur}
          >
            <Radio value={0}>Male</Radio>
            <Radio value={1}>Female</Radio>
            <Radio value={2}>Others</Radio>
          </Radio.Group>
          <MessageValidationError
            touchedOop={touched.gender}
            messageErr={errors.gender}
          />
        </div>

        <div className="row form-group">
          <label className="col-md-3" htmlFor="email">
            Email
          </label>
          <Input
            className="col-md-9"
            id="email"
            type="text"
            name="email"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.email}
          />
          <MessageValidationError
            touchedOop={touched.email}
            messageErr={errors.email}
          />
        </div>

        <div className="row form-group">
          <span htmlFor="dateOfBirth" className="col-md-3">
            Date Of birth
          </span>
          <Space className="col-md-9" direction="vertical">
            <DatePicker
              format={dateFormat}
              style={{ width: 380 }}
              onChange={handleChangeDate}
              value={moment(values.dateOfBirth, "DD/MM/YYYY")}
            />
          </Space>
          <MessageValidationError
            touchedOop={touched.dateOfBirth}
            messageErr={errors.dateOfBirth}
          />
        </div>

        <div className="row mt-4 d-flex">
          <Button
            htmlType="submit"
            className="login-form-button flex-fill primary"
          >
            Save
          </Button>
        </div>
      </form>
    </Card>
  );
}

export default Profile;
