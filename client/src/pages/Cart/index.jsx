import React, { useRef, useState } from "react";
import PropTypes from "prop-types";
import { Card, Rate, Button, Input, message } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { Table } from "reactstrap";
import { useHistory } from "react-router-dom";

import { BASE_URL } from "../../constants";

import "./style.scss";
import { DeleteOutlined, MinusOutlined, PlusOutlined } from "@ant-design/icons";
import { actionAddToCart, actionRemoveProductInCart } from "../../actions/cart";
import Login from "../../components/Login";

Cart.propTypes = {};

function Cart(props) {
  const { product } = useSelector((state) => state.cart);

  const { token } = useSelector((state) => state.auth);

  const [isShowLogin, setIsShowLogin] = useState(false);

  const dispatch = useDispatch();

  const history = useHistory();

  const summary = useRef(0);

  const priceFormat = (price) => {
    return parseFloat((price + "").replace(/,/g, ""))
      .toString()
      .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };

  const handleChangeQuality = (value, product) => {
    if (+product.quality + value < 1) {
      return;
    }

    dispatch(
      actionAddToCart({
        ...product,
        quality: value,
      })
    );
  };

  const handleRemoveProduct = (item) => {
    dispatch(actionRemoveProductInCart(item));
    message.success("Remove success");
  };

  const handlePurchase = () => {
    if (!token) {
      setIsShowLogin(true);
    } else {
      history.push("/purchase");
    }
  };

  const handleCloseLogin = () => {
    if (isShowLogin) {
      setIsShowLogin(false);
      history.push("/purchase");
    }
  };

  return (
    <>
      <div className="row" style={{ height: 500 }}>
        <div className="col-md-9">
          <Card style={{ width: "100%" }}>
            <div className="row">
              <div className="col-md-12">
                <Table borderless>
                  <thead>
                    <tr>
                      <th>Product</th>
                      <th>Quality</th>
                      <th>Total</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    {product &&
                      product.map((item, index) => (
                        <tr>
                          <td className="d-flex align-items-center">
                            <img
                              className="product-image mr-3"
                              src={`${BASE_URL}${item.product.image}`}
                            />
                            <div className="product-info">
                              <div>{item.product.productId.name}</div>
                              <div>
                                {priceFormat(item.product.productId.price)} vnđ
                              </div>
                            </div>
                          </td>
                          <td className="align-middle">
                            <Button
                              onClick={() => handleChangeQuality(-1, item)}
                            >
                              <MinusOutlined />
                            </Button>
                            <Input
                              className="text-center"
                              style={{ width: 46 }}
                              value={item.quality}
                            />
                            <Button
                              onClick={() => handleChangeQuality(1, item)}
                            >
                              <PlusOutlined />
                            </Button>
                          </td>
                          <td className="align-middle">
                            <label hidden>
                              {index === 0
                                ? (summary.current =
                                    0 +
                                    +item.product.productId.price *
                                      +item.quality)
                                : (summary.current =
                                    summary.current +
                                    +item.product.productId.price *
                                      +item.quality)}
                            </label>
                            {priceFormat(
                              +item.product.productId.price * +item.quality
                            )}
                            vnđ
                          </td>
                          <td className="align-middle">
                            <Button
                              className="d-flex align-items-center"
                              type="primary"
                              onClick={() => handleRemoveProduct(item)}
                            >
                              <DeleteOutlined />
                              Remove
                            </Button>
                          </td>
                        </tr>
                      ))}
                  </tbody>
                </Table>
              </div>
            </div>
          </Card>
        </div>

        <div className="col-md-3">
          <Card style={{ width: "100%" }}>
            <div className="row">
              <div className="col-md-12">Order Summary</div>
            </div>
            <hr />
            <div className="row">
              <div className="col-md-12">
                Total:{" "}
                {product && product.length > 0
                  ? priceFormat(summary.current)
                  : 0}{" "}
                vnđ
              </div>
            </div>
            <hr />

            <div className="row">
              <div className="col-md-12">
                <Button
                  onClick={handlePurchase}
                  type="primary"
                  style={{ width: "100%" }}
                >
                  Purchase
                </Button>
              </div>
            </div>
          </Card>
        </div>
      </div>

      <Login
        isShowModal={isShowLogin}
        isLogin={true}
        onCloseModel={handleCloseLogin}
      />
    </>
  );
}

export default Cart;
