import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Card, Input, Button, Radio, DatePicker, Space } from "antd";
import { useFormik } from "formik";
import * as yup from "yup";
import { PlusOutlined } from "@ant-design/icons";
import moment from "moment";

import {
  actionGetUser,
  actionUpdateUser,
  actionChangePasswordUser,
} from "../../actions/user";
import { actionSetMgsError } from "../../actions/message";
import MessageValidationError from "../../components/MessageValidationError";

import DefaultAvatar from "../../assets/images/default-avatar.jpg";

import "./style.scss";

ChangePassword.propTypes = {};

function ChangePassword(props) {
  const { user } = useSelector((state) => state.auth);

  const dispatch = useDispatch();

  useEffect(() => {
    resetForm();
  }, []);

  const initialValues = {
    currentPassword: "",
    password: "",
    confirmPassword: "",
  };

  const validationSchema = yup.object().shape({
    currentPassword: yup
      .string()
      .required("Please input your current password"),
    password: yup.string().required("Please input your password"),
    confirmPassword: yup
      .string()
      .test("match", "Confirm password must like password", function (
        confirmPassword
      ) {
        if (confirmPassword && this.parent.password) {
          return confirmPassword === this.parent.password;
        }
      })
      .required("Please input your confirm password"),
  });

  const formik = useFormik({
    validationSchema,
    initialValues,
    onSubmit: async (values) => {
      await dispatch(actionChangePasswordUser(values));
    },
  });

  const {
    touched,
    values,
    errors,
    handleBlur,
    handleSubmit,
    handleChange,
    setValues,
    setTouched,
    setErrors,
  } = formik;

  const resetForm = () => {
    setValues({
      currentPassword: "",
      password: "",
      confirmPassword: "",
    });
    setTouched({});
    setErrors({});
  };

  return (
    <Card
      style={{
        display: "flex",
        justifyContent: "center",
        width: "100%",
        height: "500px",
        alignItems: "center",
      }}
    >
      <div className="row">
        <div
          className="col-md-12"
          style={{ marginBottom: 70, textAlign: "center", fontSize: "2.6rem", color:'#289373' }}
        >
          CHANGE PASSWORD
        </div>
      </div>
      <form onSubmit={handleSubmit} style={{ width: 500 }}>
        <div className="form-group row">
          <label className="col-md-4" htmlFor="currentPassword">
            Current Password
          </label>
          <Input.Password
            className="col-md-8"
            id="currentPassword"
            type="text"
            name="currentPassword"
            value={values.currentPassword}
            onChange={handleChange}
            onBlur={handleBlur}
          />
          <MessageValidationError
            touchedOop={touched.currentPassword}
            messageErr={errors.currentPassword}
          />
        </div>
        <div className="form-group row">
          <label className="col-md-4" htmlFor="password">
            Password
          </label>
          <Input.Password
            className="col-md-8"
            id="password"
            type="text"
            name="password"
            value={values.password}
            onChange={handleChange}
            onBlur={handleBlur}
          />
          <MessageValidationError
            touchedOop={touched.password}
            messageErr={errors.password}
          />
        </div>

        <div className="row form-group">
          <label className="col-md-4" htmlFor="confirmPassword">
            Confirm Password
          </label>
          <Input.Password
            className="col-md-8"
            id="confirmPassword"
            type="text"
            name="confirmPassword"
            onChange={handleChange}
            onBlur={handleBlur}
            value={values.confirmPassword}
          />
          <MessageValidationError
            touchedOop={touched.confirmPassword}
            messageErr={errors.confirmPassword}
          />
        </div>

        <div className="row mt-4 d-flex">
          <Button
            htmlType="submit"
            className="login-form-button flex-fill primary"
          >
            Save
          </Button>
        </div>
      </form>
    </Card>
  );
}

export default ChangePassword;
