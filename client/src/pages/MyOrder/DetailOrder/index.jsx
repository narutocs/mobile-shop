import React, { useState } from "react";
import PropTypes from "prop-types";
import { Modal, Button, Card, Input } from "antd";
import { DeleteOutlined, MinusOutlined, PlusOutlined } from "@ant-design/icons";
import { Table, Label, Button as Btn } from "reactstrap";
import { BASE_URL } from "../../../constants";

import { useDispatch, useSelector } from "react-redux";
import { actionSetMgsSuccess } from "../../../actions/message";
import orderApi from "../../../api/orderApi";

DetailOrder.propTypes = {
  isShowModal: PropTypes.bool,
  onCloseModel: PropTypes.func,
  currentOrder: PropTypes.object,
};

DetailOrder.defaultProps = {
  isShowModal: false,
  onCloseModel: null,
  currentOrder: null,
};

function DetailOrder(props) {
  const { isShowModal, onCloseModel, currentOrder } = props;
  const dispatch = useDispatch();
  const handleCancel = () => {
    if (onCloseModel) {
      onCloseModel();
    }
  };

  const _id = useSelector((state) => state.auth.user._id);

  const priceFormat = (price) => {
    return parseFloat((price + "").replace(/,/g, ""))
      .toString()
      .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };

  const handleReject = async () => {
    await orderApi.update(currentOrder[0].idOrder._id, {
      status: "Reject",
      idSeller: _id,
    });
    dispatch(actionSetMgsSuccess("Reject order success"));
    if (onCloseModel) {
      onCloseModel(true);
    }
  };
  return (
    <Modal
      onCancel={handleCancel}
      footer={null}
      title="Order Detail"
      visible={isShowModal}
    >
      <Card style={{ width: "100%" }}>
        <div className="row">
          <div className="col-md-12">
            <Table borderless>
              <thead>
                <tr>
                  <th>Product</th>
                  <th>Quality</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
                {currentOrder &&
                  currentOrder.map((item, index) => (
                    <tr>
                      <td className="d-flex align-items-center">
                        <img
                          className="product-image mr-3"
                          src={`${BASE_URL}${item.idProductColor.image}`}
                        />
                        <div className="product-info">
                          <div>{item.idProductColor.productId.name}</div>
                          <div>
                            {priceFormat(item.idProductColor.productId.price)}{" "}
                            vnđ
                          </div>
                        </div>
                      </td>
                      <td className="align-middle">
                        <Label className="text-center">{item.amount}</Label>
                      </td>
                      <td className="align-middle">
                        <Label className="text-center">
                          {priceFormat(
                            +item.idProductColor.productId.price * item.amount
                          )}{" "}
                          vnđ
                        </Label>
                      </td>
                    </tr>
                  ))}
              </tbody>
            </Table>
          </div>
        </div>
        <div className="row">
          <div
            className="col-md-12"
            style={{ display: "flex", justifyContent: "center" }}
          >
            <Btn
              className="btn"
              style={{ width: 100, height: 30, fontSize: "1.6rem" }}
              onClick={handleReject}
              color="danger"
              hidden={
                currentOrder &&
                (currentOrder[0].idOrder.status === "Reject" ||
                  currentOrder[0].idOrder.status === "Delivered")
                  ? true
                  : false
              }
            >
              Reject
            </Btn>
          </div>
        </div>
      </Card>
    </Modal>
  );
}

export default DetailOrder;
