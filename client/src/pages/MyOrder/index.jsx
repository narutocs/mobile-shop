import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Table, Label } from "reactstrap";
import { Card, Rate, Button, Input, message, Pagination } from "antd";
import { useSelector, useDispatch } from "react-redux";
import moment from "moment";
import DetailOrder from "./DetailOrder";

import { actionGetListOrder } from "../../actions/order";

MyOrder.propTypes = {};

function MyOrder(props) {
  const { listOrder, pagination } = useSelector((state) => state.order);

  const { user } = useSelector((state) => state.auth);

  const [isShowModal, setIsShowModal] = useState(false);

  const [currentOrder, setCurrentOrder] = useState(null);

  const handleCloseModel = (value = false) => {
    if (value) {
      if (user) {
        dispatch(actionGetListOrder(user._id));
      }
    }
    setIsShowModal(false);
  };

  const dispatch = useDispatch();

  useEffect(() => {
    if (user) {
      dispatch(actionGetListOrder(user._id));
    }
  }, [user]);

  const handleChangePage = (value) => {
    if (user) {
      dispatch(
        actionGetListOrder(user._id, {
          page: value,
        })
      );
    }
  };

  const handleShowDetail = (order) => {
    setIsShowModal(true);
    setCurrentOrder(order);
  };

  return (
    <div className="row" style={{ height: 500 }}>
      <div className="col-md-12">
        <Card style={{ width: "100%" }}>
          <Table borderless hover>
            <thead>
              <tr>
                <th>#</th>
                <th>Order Id</th>
                <th>Order Time</th>
                <th>Address</th>
                <th>Total</th>
                <th>Status</th>
                <th>Seller</th>
                <th>Detail</th>
              </tr>
            </thead>
            <tbody>
              {listOrder &&
                listOrder.map((order, index) => {
                  const date = new Date(order[0].idOrder.timeOrder);
                  let total = 0;

                  for (let i = 0; i < order.length; i++) {
                    total +=
                      +order[i].amount *
                      +order[i].idProductColor.productId.price;
                  }

                  total = parseFloat((total + "").replace(/,/g, ""))
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                  console.log({ listOrder });
                  const colorClass =
                    order[0].idOrder.status === "Pending"
                      ? "bg-secondary"
                      : order[0].idOrder.status === "Reject"
                      ? "bg-danger"
                      : order[0].idOrder.status === "Delivered"
                      ? "bg-success"
                      : "bg-info";
                  return (
                    <tr>
                      <th scope="row">{index + 1}</th>
                      <th>{order[0].idOrder._id}</th>
                      <th>{moment(date).format("DD/MM/YYY hh:mm")}</th>
                      <th>{order[0].idOrder.address}</th>
                      <th>{total} vnđ</th>
                      <th>
                        <Label
                          className={`${colorClass} p-2`}
                          style={{
                            color: "#fff",
                            fontWeight: 300,
                            borderRadius: 10,
                            minWidth: 80,
                            textAlign: "center",
                          }}
                        >
                          {order[0].idOrder.status}
                        </Label>
                      </th>
                      <th>
                        {order[0].idOrder.idSeller
                          ? order[0].idOrder.idSeller.fullName
                          : ""}
                      </th>
                      <th>
                        <Button
                          type="primary"
                          onClick={() => handleShowDetail(order)}
                        >
                          View Detail
                        </Button>
                      </th>
                    </tr>
                  );
                })}
            </tbody>
          </Table>
          <div className="row container-fluid">
            <div className="col-md-12 d-flex justify-content-center">
              <Pagination
                current={+pagination.page}
                total={
                  pagination
                    ? Math.ceil(+pagination.total / +pagination.amount) * 10
                    : 0
                }
                onChange={handleChangePage}
              />
            </div>
          </div>
        </Card>
        <DetailOrder
          isShowModal={isShowModal}
          onCloseModel={handleCloseModel}
          currentOrder={currentOrder}
        />
      </div>
    </div>
  );
}

export default MyOrder;
