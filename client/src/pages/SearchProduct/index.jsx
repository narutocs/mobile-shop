import React from "react";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import { Pagination, Button } from "antd";

import Product from "../../components/Product";
import { actionGetListProduct } from "../../actions/product";
import { ArrowUpOutlined, ArrowDownOutlined } from "@ant-design/icons";

import { useParams } from "react-router-dom";

import "./style.scss";
import Loading from "../../components/Loading";

SearchProduct.propTypes = {};

function SearchProduct(props) {
  const dispatch = useDispatch();

  const listProduct = useSelector((state) => state.product.listProduct);

  const { searchName } = useParams();

  const pagination = useSelector((state) => state.product.pagination);

  const handleChangePage = (value) => {
    dispatch(
      actionGetListProduct({
        page: value,
        search: searchName === "all" ? "" : searchName,
      })
    );
  };

  const handleSort = (value) => {
    dispatch(
      actionGetListProduct({
        sort: value,
        search: searchName === "all" ? "" : searchName,
      })
    );
  };

  return (
    <>
      <div className="row mr-0 pr-0">
        <div className="col-md-12 title-new d-flex justify-content-between mr-0 pr-0">
          <span> Have {pagination.total} product</span>
          <div>
            <Button
              className={+pagination.sort === 1 ? "selected" : ""}
              onClick={() => handleSort(1)}
            >
              <ArrowUpOutlined />
              <span>Price Low To High</span>
            </Button>
            <Button
              className={+pagination.sort === -1 ? "selected" : ""}
              onClick={() => handleSort(-1)}
            >
              <ArrowDownOutlined />
              <span>Price High To Low</span>
            </Button>
          </div>
        </div>
      </div>
      <Loading>
        <div className="row site-layout-content ">
          <div className="col-md-12 list-product ">
            <div className="d-flex justify-content-between">
              {listProduct &&
                listProduct.map((product, index) => {
                  if (index < 5) {
                    return (
                      <Product key={product[0]._id} product={product[0]} />
                    );
                  }
                })}
            </div>
          </div>
        </div>

        <div className="row site-layout-content ">
          <div className="col-md-12 list-product ">
            <div className="d-flex justify-content-between">
              {listProduct &&
                listProduct.map((product, index) => {
                  if (index >= 5 && index < 10) {
                    return (
                      <Product key={product[0]._id} product={product[0]} />
                    );
                  }
                })}
            </div>
          </div>
        </div>

        <div className="row site-layout-content ">
          <div className="col-md-12 list-product ">
            <div className="d-flex justify-content-between">
              {listProduct &&
                listProduct.map((product, index) => {
                  if (index >= 10 && index < 15) {
                    return (
                      <Product key={product[0]._id} product={product[0]} />
                    );
                  }
                })}
            </div>
          </div>
        </div>

        <div className="row site-layout-content ">
          <div className="col-md-12 list-product ">
            <div className="d-flex justify-content-between">
              {listProduct &&
                listProduct.map((product, index) => {
                  if (index >= 15 && index < 20) {
                    return (
                      <Product key={product[0]._id} product={product[0]} />
                    );
                  }
                })}
            </div>
          </div>
        </div>

        <div className="row container-fluid">
          <div className="col-md-12 d-flex justify-content-center">
            <Pagination
              current={+pagination.page}
              total={
                pagination
                  ? Math.ceil(+pagination.total / +pagination.amount) * 10
                  : 0
              }
              onChange={handleChangePage}
            />
          </div>
        </div>
      </Loading>
    </>
  );
}

export default SearchProduct;
