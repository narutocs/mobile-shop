import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Carousel, Card, Rate } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { actionGetNewProduct } from "../../actions/product";
import sliderApi from "../../api/sliderApi";
import Product from "../../components/Product";

import "./style.scss";
import { BASE_URL } from "../../constants";

Home.propTypes = {};

function Home(props) {
  const { Meta } = Card;

  const dispatch = useDispatch();
  const [dataSource, setDataSource] = useState([]);
  const { listNewProduct } = useSelector((state) => state.product);

  useEffect(() => {
    dispatch(actionGetNewProduct());
    const getListSlider = async () => {
      const {
        data: { listSlider },
      } = await sliderApi.getList();
      console.log("list ", listSlider);
      setDataSource(listSlider);
    };

    getListSlider();
  }, []);
  return (
    <>
      <Carousel autoplay effect="fade" className="carousel">
        {dataSource.length > 0 &&
          dataSource.map((data) => {
            return (
              <div>
                <img className="slide-show" src={`${BASE_URL}${data.image}`} />
              </div>
            );
          })}
      </Carousel>
      <div className="list-brand mt-3 mb-5 d-flex justify-content-between">
        <img
          className="img-brand"
          src="https://cdn.tgdd.vn/Brand/1/iPhone-(Apple)42-b_16.jpg"
          alt=""
        />
        <img
          className="img-brand"
          src="https://cdn.tgdd.vn/Brand/1/Samsung42-b_25.jpg"
          alt=""
        />
        <img
          className="img-brand"
          src="https://cdn.tgdd.vn/Brand/1/OPPO42-b_9.png"
          alt=""
        />
        <img
          className="img-brand"
          src="https://cdn.tgdd.vn/Brand/1/Xiaomi42-b_45.jpg"
          alt=""
        />
        <img
          className="img-brand"
          src="https://cdn.tgdd.vn/Brand/1/Vivo42-b_50.jpg"
          alt=""
        />
        <img
          className="img-brand"
          src="https://cdn.tgdd.vn/Brand/1/Realme42-b_37.png"
          alt=""
        />
        <img
          className="img-brand"
          src="https://cdn.tgdd.vn/Brand/1/Huawei42-b_30.jpg"
          alt=""
        />
      </div>
      <div className="row">
        <div className="col-md-12 title-new">New Products</div>
      </div>
      <div className="row site-layout-content ">
        <div className="col-md-12 list-product ">
          <div className="d-flex justify-content-between">
            {listNewProduct &&
              listNewProduct.map((product, index) => {
                if (index < 5) {
                  return <Product key={product._id} product={product} />;
                }
              })}
          </div>
        </div>
      </div>

      <div className="row site-layout-content ">
        <div className="col-md-12 list-product ">
          <div className="d-flex justify-content-between">
            {listNewProduct &&
              listNewProduct.map((product, index) => {
                if (index >= 5) {
                  return <Product key={product._id} product={product} />;
                }
              })}
          </div>
        </div>
      </div>

      {/* <div className="row mt-5">
        <div className="col-md-12 title-new">Best seller</div>
      </div>

      <div className="row site-layout-content ">
        <div className="col-md-12 list-product ">
          <div className="d-flex justify-content-between">
            <Card
              hoverable
              style={{ width: 240 }}
              cover={
                <img
                  alt="example"
                  src="https://cdn.tgdd.vn/Products/Images/42/219314/samsung-galaxy-a21s-055620-045659-400x400.jpg"
                />
              }
            >
              <Meta title="Samsung galaxy A21s" />
              <Meta title="4.890.000₫" className="pt-3" />
              <div>
                <Rate disabled defaultValue={2} />
                <span className="ml-3">3 rates</span>
              </div>
            </Card>
            <Card
              hoverable
              style={{ width: 240 }}
              cover={
                <img
                  alt="example"
                  src="https://cdn.tgdd.vn/Products/Images/42/219314/samsung-galaxy-a21s-055620-045659-400x400.jpg"
                />
              }
            >
              <Meta title="Samsung galaxy A21s" />
              <Meta title="4.890.000₫" className="pt-3" />
              <div>
                <Rate disabled defaultValue={2} />
                <span className="ml-3">3 rates</span>
              </div>
            </Card>
            <Card
              hoverable
              style={{ width: 240 }}
              cover={
                <img
                  alt="example"
                  src="https://cdn.tgdd.vn/Products/Images/42/219314/samsung-galaxy-a21s-055620-045659-400x400.jpg"
                />
              }
            >
              <Meta title="Samsung galaxy A21s" />
              <Meta title="4.890.000₫" className="pt-3" />
              <div>
                <Rate disabled defaultValue={2} />
                <span className="ml-3">3 rates</span>
              </div>
            </Card>
            <Card
              hoverable
              style={{ width: 240 }}
              cover={
                <img
                  alt="example"
                  src="https://cdn.tgdd.vn/Products/Images/42/219314/samsung-galaxy-a21s-055620-045659-400x400.jpg"
                />
              }
            >
              <Meta title="Samsung galaxy A21s" />
              <Meta title="4.890.000₫" className="pt-3" />
              <div>
                <Rate disabled defaultValue={2} />
                <span className="ml-3">3 rates</span>
              </div>
            </Card>
            <Card
              hoverable
              style={{ width: 240 }}
              cover={
                <img
                  alt="example"
                  src="https://cdn.tgdd.vn/Products/Images/42/219314/samsung-galaxy-a21s-055620-045659-400x400.jpg"
                />
              }
            >
              <Meta title="Samsung galaxy A21s" />
              <Meta title="4.890.000₫" className="pt-3" />
              <div>
                <Rate disabled defaultValue={2} />
                <span className="ml-3">3 rates</span>
              </div>
            </Card>
          </div>

          <div className="d-flex justify-content-between mt-3">
            <Card
              hoverable
              style={{ width: 240 }}
              cover={
                <img
                  alt="example"
                  src="https://cdn.tgdd.vn/Products/Images/42/219314/samsung-galaxy-a21s-055620-045659-400x400.jpg"
                />
              }
            >
              <Meta title="Samsung galaxy A21s" />
              <Meta title="4.890.000₫" className="pt-3" />
              <div>
                <Rate disabled defaultValue={2} />
                <span className="ml-3">3 rates</span>
              </div>
            </Card>
            <Card
              hoverable
              style={{ width: 240 }}
              cover={
                <img
                  alt="example"
                  src="https://cdn.tgdd.vn/Products/Images/42/219314/samsung-galaxy-a21s-055620-045659-400x400.jpg"
                />
              }
            >
              <Meta title="Samsung galaxy A21s" />
              <Meta title="4.890.000₫" className="pt-3" />
              <div>
                <Rate disabled defaultValue={2} />
                <span className="ml-3">3 rates</span>
              </div>
            </Card>
            <Card
              hoverable
              style={{ width: 240 }}
              cover={
                <img
                  alt="example"
                  src="https://cdn.tgdd.vn/Products/Images/42/219314/samsung-galaxy-a21s-055620-045659-400x400.jpg"
                />
              }
            >
              <Meta title="Samsung galaxy A21s" />
              <Meta title="4.890.000₫" className="pt-3" />
              <div>
                <Rate disabled defaultValue={2} />
                <span className="ml-3">3 rates</span>
              </div>
            </Card>
            <Card
              hoverable
              style={{ width: 240 }}
              cover={
                <img
                  alt="example"
                  src="https://cdn.tgdd.vn/Products/Images/42/219314/samsung-galaxy-a21s-055620-045659-400x400.jpg"
                />
              }
            >
              <Meta title="Samsung galaxy A21s" />
              <Meta title="4.890.000₫" className="pt-3" />
              <div>
                <Rate disabled defaultValue={2} />
                <span className="ml-3">3 rates</span>
              </div>
            </Card>
            <Card
              hoverable
              style={{ width: 240 }}
              cover={
                <img
                  alt="example"
                  src="https://cdn.tgdd.vn/Products/Images/42/219314/samsung-galaxy-a21s-055620-045659-400x400.jpg"
                />
              }
            >
              <Meta title="Samsung galaxy A21s" />
              <Meta title="4.890.000₫" className="pt-3" />
              <div>
                <Rate disabled defaultValue={2} />
                <span className="ml-3">3 rates</span>
              </div>
            </Card>
          </div>
        </div>
      </div> */}
    </>
  );
}

export default Home;
