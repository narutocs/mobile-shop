import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Card, Rate, Button, Input, message } from "antd";
import { useHistory, useParams } from "react-router-dom";
import { Table } from "reactstrap";
import {
  MinusOutlined,
  PlusOutlined,
  ShoppingCartOutlined,
  ShoppingOutlined,
  CheckOutlined,
} from "@ant-design/icons";
import { useSelector, useDispatch } from "react-redux";

import { actionGetProduct, actionSelectedProduct } from "../../actions/product";
import { BASE_URL } from "../../constants";

import "./style.scss";
import { actionAddToCart } from "../../actions/cart";

ProductDetail.propTypes = {};

function ProductDetail(props) {
  const dispatch = useDispatch();

  const { current, selected } = useSelector((state) => state.product);

  const [productQuality, setProductQuality] = useState(1);

  const history = useHistory();

  const { _id } = useParams();

  const { image = "", productId = {} } = selected;

  const {
    price,
    name,
    cpu,
    batteries,
    operating_system,
    internal_storage,
    primary_camera,
    secondary_camera,
    ram,
  } = productId;

  useEffect(() => {
    dispatch(actionGetProduct(_id));
  }, []);

  const priceFormat = parseFloat((price + "").replace(/,/g, ""))
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");

  const handleChangeSelectColor = (item) => {
    dispatch(actionSelectedProduct(item));
  };

  const handleChangeQuality = (data) => {
    if (productQuality + data < 1) {
      return;
    }
    setProductQuality(productQuality + data);
  };

  const handleAddToCart = () => {
    dispatch(actionAddToCart({ product: selected, quality: productQuality }));
 
  };

  const handleBuyNow = () => {
    dispatch(actionAddToCart({ product: selected, quality: productQuality }));
    history.push("/cart");
  };

  const renderListColor = () => {
    let element = null;
    if (Object.keys(current[0]).length <= 0) {
      return;
    }

    element = current.map((item) => {
      const { colorId } = item;
      const { name, code_color } = colorId;

      return (
        <div className="d-flex align-items-center">
          <span className="mr-3">{name}: </span>
          <label
            className={`btn-color mr-5`}
            style={{ backgroundColor: code_color }}
            onClick={() => handleChangeSelectColor(item)}
          >
            {selected.colorId.name === name && name === "Black" && (
              <CheckOutlined className="ml-3 mt-3" style={{ color: "white" }} />
            )}
            {selected.colorId.name === name && name !== "Black" && (
              <CheckOutlined className="ml-3 mt-3" />
            )}
          </label>
        </div>
      );
    });

    return element;
  };
  return (
    <Card style={{ width: "100%" }}>
      <div className="row">
        <div className="col-md 6" style={{ maxWidth: 500 }}>
          <img style={{ maxWidth: 500 }} src={`${BASE_URL}${image}`} />
        </div>
        <div className="col-md 6">
          <div className="row ">
            <h2>{name}</h2>
          </div>
          <div className="row d-flex align-items-center">
            <Rate disabled defaultValue={2} />
            <span className="pt-3 pl-3 pr-3">100 reviews</span>
            <span
              className="pt-3 d-flex align-items-center"
              style={{ color: "green" }}
            >
              <ShoppingOutlined />
              <span className="pl-2">200 orders</span>
            </span>
          </div>

          <div className="row row-price mb-3">{priceFormat} vnđ</div>

          <div className="row mb-3">
            <Table striped>
              <tbody>
                <tr>
                  <td>Ram</td>
                  <td>{ram}</td>
                </tr>
                <tr>
                  <td>Cpu</td>
                  <td>{cpu}</td>
                </tr>

                <tr>
                  <td>Batteries</td>
                  <td>{batteries}</td>
                </tr>

                <tr>
                  <td>Operating System</td>
                  <td>{operating_system}</td>
                </tr>

                <tr>
                  <td>Internal Storage</td>
                  <td>{internal_storage}</td>
                </tr>

                <tr>
                  <td>Primary Camera</td>
                  <td>{primary_camera}</td>
                </tr>

                <tr>
                  <td>Secondary Camera</td>
                  <td>{secondary_camera}</td>
                </tr>
              </tbody>
            </Table>
          </div>

          <div className="row mb-3">{renderListColor()}</div>

          <div className="row d-flex align-items-center mb-3">
            <span className="mr-3">Quality: </span>
            <Button onClick={() => handleChangeQuality(-1)}>
              <MinusOutlined />
            </Button>
            <Input
              className="text-center"
              style={{ width: 46 }}
              value={productQuality}
            />
            <Button onClick={() => handleChangeQuality(1)}>
              <PlusOutlined />
            </Button>
          </div>

          <div className="row mt-3">
            <Button type="primary" onClick={handleBuyNow}>
              Buy now
            </Button>
            <Button onClick={handleAddToCart}>
              Add to cart
              <ShoppingCartOutlined />
            </Button>
          </div>
        </div>
      </div>
    </Card>
  );
}

export default ProductDetail;
