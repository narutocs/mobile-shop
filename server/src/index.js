require('dotenv').config()

import express from "express";
import bodyParser from "body-parser"
import morgan from "morgan";
import http from "http";
import cors from "cors";
import fs from "fs";
import mongoose from "mongoose";

import ConnectDatabase from "../libs/ConnectDB";
import router from "../routers";
import createDefaultRole from "../controllers/role.controller";
import cityModel from "../models/city.model";
import districtModel from "../models/district.model";
import villageModel from "../models/village.model";


const app = express();

const server = http.createServer(app);

app.use("/public", express.static("public"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }))
app.use(morgan("dev"));
app.use(cors());
app.use("/api", router);

app.use((error, req, res, next) => {
  const status = error.status || 500;
  const message = error.message;
  res.status(status).send({
    message: message,
  });
});

app.use((req, res, next) => {
  res.status(404).send({
    message: "PAGE NOT FOUND",
    statusCode: 404
  })
})

const createDataAddress = async () => {
  const listCity = await cityModel.find();

  if (listCity.length > 0) {
    return;
  }

  const rawdata = fs.readFileSync('dataAddress.json');
  const listData = JSON.parse(rawdata);

  listData.map(async city => {
    const newCity = new cityModel({
      _id: new mongoose.Types.ObjectId(),
      id: city.id,
      name: city.name
    });

    await newCity.save();
    city.huyen.map(async district => {
      const newDistrict = new districtModel({
        _id: new mongoose.Types.ObjectId(),
        id: district.id,
        name: district.name,
        idCity: district.tinh_id
      })

      await newDistrict.save()

      district.xa.map(async village => {
        const newVillage = new villageModel({
          _id: new mongoose.Types.ObjectId(),
          id: village.id,
          name: village.name,
          idDistrict: village.huyen_id
        })

        await newVillage.save()
      })
    })

  })

}

ConnectDatabase();

createDefaultRole();

createDataAddress();

const PORT = process.env.PORT || 5000;


server.listen(PORT, () => {
  console.log(`SERVER START AT PORT ${PORT}`)
})