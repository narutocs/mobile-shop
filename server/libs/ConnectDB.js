import mongoose from "mongoose";

const optional = {
  useNewUrlParser: true,
  autoIndex: true,
  useCreateIndex: true,
  useFindAndModify: false,
  useUnifiedTopology: true

}
const connect = () => {
  return mongoose.connect('mongodb://localhost/Shopping', optional, () => {
    console.log("connect database success")
  });
}

export default connect;