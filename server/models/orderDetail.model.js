import { Schema, model } from "mongoose";

const orderDetailSchema = new Schema({
  _id: Schema.Types.ObjectId,
  idOrder: {
    type: Schema.Types.ObjectId,
    ref: "ORDER"
  },
  idProductColor: {
    type: Schema.Types.ObjectId,
    ref: "PRODUCT_COLOR"
  },
  amount: Number
})

const orderDetailModel = model("ORDER_DETAIL", orderDetailSchema);

export default orderDetailModel;
