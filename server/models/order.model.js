import { Schema, model } from "mongoose";

const orderSchema = new Schema({
  _id: Schema.Types.ObjectId,
  idUser: {
    type: Schema.Types.ObjectId,
    ref: "USER"
  },
  idSeller: {
    type: Schema.Types.ObjectId,
    ref: "USER"
  },
  status: String,
  timeOrder: Number,
  address: String,
  idCity: {
    type: Schema.Types.ObjectId,
    ref: "CITY"
  },
  idDistrict: {
    type: Schema.Types.ObjectId,
    ref: "DISTRICT"
  },
  idVillage: {
    type: Schema.Types.ObjectId,
    ref: "VILLAGE"
  }
})

const orderModel = model("ORDER", orderSchema);

export default orderModel;
