import { Schema, model } from "mongoose";

const brandSchema = new Schema({
  _id: Schema.Types.ObjectId,
  name: String,
  logo: String
})

const brandModel = model("BRAND", brandSchema);

export default brandModel;