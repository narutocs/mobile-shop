import { Schema, model } from "mongoose";

const citySchema = new Schema({
  _id: Schema.Types.ObjectId,
  id: Number,
  name: String,
})

const cityModel = model("CITY", citySchema);

export default cityModel;