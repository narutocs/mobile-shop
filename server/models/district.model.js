import { Schema, model } from "mongoose";

const districtChema = new Schema({
  _id: Schema.Types.ObjectId,
  id: Number,
  name: String,
  idCity: Number
})

const districtModel = model("DISTRICT", districtChema);

export default districtModel;