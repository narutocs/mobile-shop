import { Schema, model } from "mongoose";

const userSchema = new Schema({
  _id: Schema.Types.ObjectId,
  email: {
    type: String,
    unique: false
  },
  password: String,
  fullName: String,
  gender: Number,
  phoneNumber: {
    type: String,
    unique: false
  },
  avatar: String,
  dateOfBirth: String,
  codeActive: Number,
  code: Number,
  isActive: {
    type: Boolean,
    default: false
  },
  idRole: {
    type: Schema.Types.ObjectId,
    ref: 'ROLE'
  }
})

const userModel = model("USER", userSchema);

export default userModel