import { Schema, model } from "mongoose";

const villageSchema = new Schema({
  _id: Schema.Types.ObjectId,
  id: Number,
  name: String,
  idDistrict: Number
})

const villageModel = model("VILLAGE", villageSchema);

export default villageModel;