import { Schema, model } from "mongoose";

const colorSchema = new Schema({
  _id: Schema.Types.ObjectId,
  code_color: String,
  name: String
})

const colorModel = model("COLOR", colorSchema);

export default colorModel;