import { Schema, model } from "mongoose";

const productColorSchema = new Schema({
  _id: Schema.Types.ObjectId,
  image: String,
  colorId: {
    type: Schema.Types.ObjectId,
    ref: "COLOR"
  },
  productId: {
    type: Schema.Types.ObjectId,
    ref: "PRODUCT"
  }
})

const productColorModel = model("PRODUCT_COLOR", productColorSchema);

export default productColorModel;