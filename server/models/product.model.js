import { Schema, model } from "mongoose";

const productSchema = new Schema({
  _id: Schema.Types.ObjectId,
  name: String,
  price: Number,
  quality: Number,
  isDeleted: {
    type: Boolean,
    default: false
  },

  ram: String,
  display: String,
  cpu: String,
  batteries: String,
  rom: String,
  operating_system: String,
  internal_storage: String,
  primary_camera: String,
  secondary_camera: String,

  create_at: Number,
  idBrand: {
    type: Schema.Types.ObjectId,
    ref: "BRAND"
  },
})

const productModel = model("PRODUCT", productSchema);

export default productModel;
