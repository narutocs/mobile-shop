import { Schema, model } from "mongoose";

const sliderSchema = new Schema({
  _id: Schema.Types.ObjectId,
  image: String
})

const sliderNodel = model("SLIDER", sliderSchema);

export default sliderNodel;