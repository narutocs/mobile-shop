import { model, Schema } from "mongoose";

const roleSchema = new Schema({
  _id: Schema.Types.ObjectId,
  name: String
})

const roleModel = model("ROLE", roleSchema);

export default roleModel;