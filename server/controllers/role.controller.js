import mongoose from "mongoose";

import roleModel from "../models/role.model";

const createDefaultRole = async (req, res, next) => {
  try {

    const count = await roleModel.find().count();

    if (count > 0) {
      return;
    }

    const roleAdmin = new roleModel({
      _id: new mongoose.Types.ObjectId(),
      name: "ADMIN"
    });

    const roleUser = new roleModel({
      _id: new mongoose.Types.ObjectId(),
      name: "USER"
    });

    const roleSuperAdmin = new roleModel({
      _id: new mongoose.Types.ObjectId(),
      name: "SUPER_ADMIN"
    })
    roleSuperAdmin.save();
    roleUser.save();
    roleAdmin.save();
  } catch (e) {
    next(e);
  }
}

export const getList = async (req, res, next) => {
  try {

    const listRole = await roleModel.find();
    const index = listRole.findIndex(x => x.name === "SUPER_ADMIN")
    listRole.splice(index, 1)
    res.status(200).send({
      listRole
    })
  } catch (e) {
    next(e);
  }
}

export default createDefaultRole;