import mongoose from "mongoose";

import sliderModel from "../models/silder.model";

export { createSlider, updateSlider, deleteSlider, getSlider, getListSlider }

const createSlider = async (req, res, next) => {
  try {

    const { file } = req;
    if (!file) {
      let error = new Error();
      error.status = 401;
      error.message = "Please input image";
      throw error;
    }
    const { originalname } = file;
    const image = `/public/img/${originalname}`;
    const newSlider = new sliderModel({
      _id: new mongoose.Types.ObjectId(),
      image
    })
    await newSlider.save();

    res.status(200).send({
      message: "Create slider success"
    })
  } catch (e) {
    next(e)
  }
}

const updateSlider = async (req, res, next) => {
  try {
    const { _id } = req.params;
    const sliderFound = await sliderModel.findById({ _id });
    if (!sliderFound) {
      let error = new Error();
      error.status = 401;
      error.message = "Slider not found";
      throw error;
    }
    const { file } = req;
    let image = sliderFound.image;
    if (file) {
      const { originalname } = file;
      image = `/public/img/${originalname}`;
    }



    await sliderModel.updateOne({ _id }, { image })

    res.status(200).send({
      message: 'Update slider success'
    })
  } catch (e) {
    next(e)
  }
}

const deleteSlider = async (req, res, next) => {
  try {
    const { _id } = req.params;
    const sliderFound = await sliderModel.findById({ _id });
    if (!sliderFound) {
      let error = new Error();
      error.status = 401;
      error.message = "Slider not found";
      throw error;
    }
    await sliderModel.deleteOne({ _id });
    res.status(200).send({
      message: "Delete slider success"
    })
  } catch (e) {
    next(e)
  }
}

const getSlider = async (req, res, next) => {
  try {
    const { _id } = req.query;
    const sliderFound = await sliderModel.findById({ _id });
    if (!sliderFound) {
      let error = new Error();
      error.status = 401;
      error.message = "Slider not found";
      throw error;
    }
    res.status(200).send({
      message: "get slider success",
      data: {
        brand: sliderFound
      }
    })
  } catch (e) {
    next(e)
  }
}

const getListSlider = async (req, res, next) => {
  try {
    console.log("zo")
    const listSlider = await sliderModel.find();
    if (listSlider.length === 0) {
      let error = new Error();
      error.status = 401;
      error.message = "List slider is empty";
      throw error;
    }

    res.status(200).send({
      message: "Get list slider success",
      data: {
        listSlider
      }
    })
  } catch (e) {
    next(e)
  }
}