import mongoose from "mongoose";

import colorModel from "../models/color.model";

export { getListColor, getOneColor, deleteColor, createColor, updateColor }

const getOneColor = async (req, res, next) => {
  try {
    const { _id } = req.query;

    const colorFound = await colorModel.findById({ _id });

    if (!colorFound) {
      let error = new Error();
      error.status = 401;
      error.message = "Color not found";
      throw error;
    }

    res.status(200).send({
      message: "Get color success",
      data: {
        color: colorFound
      }
    })
  } catch (e) {
    next(e);
  }
}

const getListColor = async (req, res, next) => {
  try {
    const { page = 0, amount = 10, sort = -1, search = "" } = req.query;
    const total = await colorModel.find().count();
    const listColor = await colorModel.find({ name: { $regex: search, $options: "$i" } }).limit(amount)
      .skip(amount * page).sort({ name: sort });

    if (listColor.length === 0) {
      let error = new Error();
      error.status = 401;
      error.message = "List color is empty";
      throw error;
    }
    const result = {
      message: "Get list color success",
      data: {
        listColor
      },
      pagination: {
        total,
        page,
        amount,
        sort,
        search
      }
    }

    res.status(200).send(result);

  } catch (e) {
    next(e);
  }
}

const createColor = async (req, res, next) => {
  try {

    const { name, code_color } = req.body;

    const newColor = new colorModel({
      _id: new mongoose.Types.ObjectId(),
      name,
      code_color
    })

    await newColor.save();

    res.status(200).send({
      message: "Create color success",
      idColor: newColor._id
    })
  } catch (e) {
    next(e);
  }
}

const updateColor = async (req, res, next) => {
  try {
    const { _id } = req.query;
    const colorFound = await colorModel.findById({ _id });
    if (!colorFound) {
      let error = new Error();
      error.status = 404;
      error.message = "color not found";
      throw error;
    }
    const { name = colorFound.name, code_color = colorFound.code_color } = req.body;

    await colorModel.updateOne({ _id }, { name, code_color });

    res.status(200).send({
      message: "Update color success"
    })
  } catch (e) {
    next(e);
  }
}

const deleteColor = async (req, res, next) => {
  try {
    const { _id } = req.query;
    const colorFound = await colorModel.findById({ _id });

    if (!colorFound) {
      let error = new Error();
      error.status = 404;
      error.message = "color not found";
      throw error;
    }

    await colorModel.deleteOne({ _id });

    res.status(200).send({
      message: "Delete color success"
    })
  } catch (e) {
    next(e);
  }
}