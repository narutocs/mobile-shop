import mongoose from "mongoose";

import cityModel from "../models/city.model";

export { createCity, getListCity }

const createCity = async (name, id) => {
  const newCity = new cityModel({
    _id: new mongoose.Types.ObjectId(),
    name,
    id
  })

  await newCity.save();
}

const getListCity = async (req, res, next) => {
  try {

    const listCity = await cityModel.find()

    res.status(200).send({
      message: "Get list City success",
      data: {
        listCity
      }
    })
  } catch (e) {
    next(e)
  }
}