import mongoose from "mongoose";

import villageModel from "../models/village.model";

export { createVillage, getListVillage }

const createVillage = async (name, idDistrict, id) => {
  const newVillage = new villageModel({
    _id: new mongoose.Types.ObjectId(),
    name,
    id,
    idDistrict
  })

  await newVillage.save();
}

const getListVillage = async (req, res, next) => {
  try {

    const { id } = req.params;

    const listVillage = await villageModel.find({ idDistrict: id })

    res.status(200).send({
      message: "Get list village success",
      data: {
        listVillage
      }
    })
  } catch (e) {
    next(e)
  }
}