import mongoose from "mongoose";

import orderModel from "../models/order.model";
import productModel from "../models/product.model";
import orderDetailModel from "../models/orderDetail.model";


export { createOrder, deleteOrder, getOneOrder, getListOrder, getListOrderByUser, updateOrder }

const getOneOrder = async (req, res, next) => {
  try {
    const { _id } = req.query;
    const orderFound = await orderDetailModel.findOne({ idOrder: _id }).populate(["idProduct", "idColor", "idOrder"]);
    if (!orderFound) {
      let error = new Error();
      error.status = 404;
      error.message = "Order not found";
      throw error;
    }

    res.status(200).send({
      message: "Get product success",
      data: {
        order: orderFound
      }
    })

  } catch (e) {
    next(e);
  }

}
const getListOrder = async (req, res, next) => {
  try {

    const { page = 1, amount = 10, sort = -1, search = "" } = req.query;

    const listOrder = await orderModel.find();

    const listResult = [];

    const total = listOrder.length;


    for (let i = 0; i < listOrder.length; i++) {
      const data = await orderDetailModel.find()
        .limit(+amount)
        .skip(+amount * (+page - 1)).sort({ price: sort })
        .populate({
          path: 'idOrder',
          populate: [
            {
              path: 'idCity',
            },
            {
              path: 'idDistrict',
            },
            {
              path: 'idVillage',
            },
            {
              path: 'idUser',
            },
            {
              path: 'idSeller',
            }],
        }).populate({
          path: "idProductColor",
          populate: [
            {
              path: 'colorId'
            },
            {
              path: "productId"
            }
          ]
        })

      listResult.push(data)
    }

    if (listResult.length === 0) {
      let error = new Error();
      error.status = 404;
      error.message = "List order is empty";
      throw error;
    }
    listResult.reverse();
    const result = {
      message: "Get list order success",
      data: {
        listResult
      },
      pagination: {
        total,
        page,
        amount,
        sort,
        search
      }
    }

    res.status(200).send(result);
  } catch (e) {
    next(e);
  }
}


const getListOrderByUser = async (req, res, next) => {
  try {

    const { page = 1, amount = 10, sort = -1, search = "" } = req.query;
    const { idUser } = req.params;
    const listOrder = await orderModel.find({ idUser });

    const listResult = [];

    const total = listOrder.length;


    for (let i = 0; i < listOrder.length; i++) {
      const data = await orderDetailModel.find({ idOrder: listOrder[i]._id })
        .limit(+amount)
        .skip(+amount * (+page - 1)).sort({ price: sort })
        .populate({
          path: 'idOrder',
          populate: [
            {
              path: 'idCity',
            },
            {
              path: 'idDistrict',
            },
            {
              path: 'idVillage',
            },
            {
              path: 'idUser',
            },
            {
              path: 'idSeller',
            }],
        }).populate({
          path: "idProductColor",
          populate: [
            {
              path: 'colorId'
            },
            {
              path: "productId"
            }
          ]
        }).populate("idSeller")

      listResult.push(data)
    }
    listResult.reverse();

    if (listResult.length === 0) {
      let error = new Error();
      error.status = 404;
      error.message = "List order is empty";
      throw error;
    }
    const result = {
      message: "Get list order success",
      data: {
        listResult
      },
      pagination: {
        total,
        page,
        amount,
        sort,
        search
      }
    }

    res.status(200).send(result);
  } catch (e) {
    next(e);
  }
}

const createOrder = async (req, res, next) => {
  try {
    const { idUser, status, address, city, district, village, cart } = req.body;

    const newOrder = new orderModel({
      _id: new mongoose.Types.ObjectId(),
      idUser,
      status,
      address,
      idCity: city,
      idDistrict: district,
      idVillage: village,
      timeOrder: new Date().getTime()
    })
    await newOrder.save();

    cart.map(async item => {
      const newOrderDetail = new orderDetailModel({
        _id: new mongoose.Types.ObjectId(),
        idOrder: newOrder._id,
        idProductColor: item.product._id,
        amount: item.quality
      })
      const productFound = await productModel.findOne({
        _id: item.product.productId._id
      })

      const quality = +productFound.quality - +item.quality;
      await productModel.updateOne({ _id: item.product.productId._id }, { quality })

      await newOrderDetail.save();
    })


    res.status(200).send({
      message: "Create order success",
      data: {
        idOrder: newOrder._id
      }
    })
  } catch (e) {
    next(e);
  }
}

const updateOrder = async (req, res, next) => {
  try {
    const { _id } = req.params;
    const orderFound = await orderModel.findById({ _id });
    if (!orderFound) {
      let error = new Error();
      error.status = 404;
      error.message = "Order not found";
      throw error;
    }
    const { status = orderFound.status, quality = 0, idSeller = '', productId } = req.body;

    if (status === "Reject") {
      const productFound = await productModel.findById({ _id: productId });
      await productModel.updateOne({ _id: productId }, { quality: +productFound.quality + +quality })
    }

    await orderModel.updateOne({ _id }, { status, idSeller });

    res.status(200).send({
      message: "Update product success"
    })
  } catch (e) {
    next(e);
  }
}

const deleteOrder = async (req, res, next) => {
  try {
    const { _id } = req.query;
    const orderFound = await orderModel.findById({ _id });
    if (!orderFound) {
      let error = new Error();
      error.status = 404;
      error.message = "Order not found";
      throw error;
    }

    await orderModel.deleteOne({ _id })

    await orderDetailModel.deleteMany({ idOrder: _id })

  } catch (e) {
    next(e);
  }
}
