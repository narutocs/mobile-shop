import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import mongoose from "mongoose";

import userModel from "../models/user.model";
import roleModel from "../models/role.model";
import transporter from "../libs/sendEmail";

export { register, login, activeAccount };

const register = async (req, res, next) => {
  try {
    const { email, password, fullName, phoneNumber } = req.body;

    const roleFound = await roleModel.findOne({ name: 'USER' });

    let codeActive = Math.floor(100000 + Math.random() * 900000);
    transporter.sendMail({
      from: "Shopping",
      to: email,
      subject: "Code active account",
      html: "<h1>" + "Your code active account is: " + codeActive + "</h1>"
    });

    const passwordHash = await bcrypt.hash(password, +process.env.saltRound);

    const newUser = new userModel({
      _id: new mongoose.Types.ObjectId(),
      fullName,
      email,
      password: passwordHash,
      codeActive,
      idRole: roleFound._id,
      phoneNumber
    })
    await newUser.save();

    res.status(200).send({
      message: "Register success please check your email to see code active"
    })

  } catch (e) {
    next(e);
  }
}

const activeAccount = async (req, res, next) => {
  try {
    const { code, email } = req.body;
    const userFound = await userModel.findOne({ email });
    if (!userFound) {
      let error = new Error();
      error.status = 401;
      error.message = "Lỗi xác thực";
      throw error;
    }

    if (+code !== +userFound.codeActive) {
      let error = new Error();
      error.status = 403;
      error.message = "Code active incorrect";
      throw error;
    }
    await userModel.updateOne({ email }, { isActive: true });

    res.status(200).send({
      message: "Active account success"
    })
  } catch (e) {
    next(e);
  }
}

const login = async (req, res, next) => {
  try {
    const { email, password } = req.body;

    const userFound = await userModel.findOne({ email }).populate("idRole");

    if (!userFound) {
      let error = new Error();
      error.status = 401;
      error.message = "Lỗi xác thực";
      throw error;
    }
    const resultCompare = await bcrypt.compare(password, userFound.password);
    if (resultCompare) {
      const payload = {
        email: userFound.email,
        role: userFound.idRole.name,
        _id: userFound._id
      }

      const token = await jwt.sign(payload, process.env.JWT_KEY);
      res.status(200).send({
        message: "Login success",
        data: {
          token,
          user: userFound
        }
      })
    } else {
      let error = new Error();
      error.status = 401;
      error.message = "Lỗi xác thực";
      throw error;
    }

  } catch (e) {
    next(e);
  }
}