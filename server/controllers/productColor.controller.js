import mongoose from "mongoose";

import productColorModel from "../models/productColor.model";

export { createProductColor, updateProductColor, deleteProductColor, getListProductColorByProduct, getOneProductColor, getProductById }

const getOneProductColor = async (req, res, next) => {
  try {
    const { _id } = req.query;

    const productColorFound = await productColorModel.findById({ _id });

    if (!productColorFound) {
      let error = new Error();
      error.status = 401;
      error.message = "ProductColor not found";
      throw error;
    }

    res.status(200).send({
      message: "Get productColor success",
      data: {
        productColor: productColorFound
      }
    })
  } catch (e) {
    next(e);
  }
}

const getProductById = async (req, res, next) => {
  try {

    const { _id } = req.params;
    const productFound = await productColorModel.find({ productId: _id }).populate(["productId", "colorId"]);
    if (!productFound) {
      let error = new Error();
      error.status = 401;
      error.message = "Product not found";
      throw error;
    }

    res.status(200).send({
      message: "Get product success",
      data: {
        product: productFound
      }
    })

  } catch (e) {
    next(e);
  }
}

const getListProductColorByProduct = async (req, res, next) => {
  try {
    const listProductColor = await productColorModel.find()

    if (listProductColor.length === 0) {
      let error = new Error();
      error.status = 401;
      error.message = "List productColor is empty";
      throw error;
    }
    const result = {
      message: "Get list productColor success",
      data: {
        listProductColor
      }
    }

    res.status(200).send(result);

  } catch (e) {
    next(e);
  }
}

const createProductColor = async (req, res, next) => {
  try {

    const { file } = req;

    if (!file) {
      let error = new Error();
      error.status = 401;
      error.message = "Please input image";
      throw error;
    }
    const { originalname } = file;
    const { colorId, productId } = req.body;
    const image = `/public/img/${originalname}`;
    const newProductColor = new productColorModel({
      _id: new mongoose.Types.ObjectId(),
      image,
      colorId,
      productId
    })

    await newProductColor.save();

    res.status(200).send({
      message: "Create productColor success"
    })
  } catch (e) {
    next(e);
  }
}

const updateProductColor = async (req, res, next) => {
  try {
    const { files } = req;

    let imageNew = []
    if (files) {
      for (let i = 0; i < files.length; i++) {
        const { originalname } = files[i];
        imageNew.push(`/public/img/${originalname}`);
      }

    }

    const { idColor, productId } = req.body;
    let listDataColor = JSON.parse(idColor);
    const productColorFound = await productColorModel.find({ productId });

    if (productColorFound.length <= 0) {
      let error = new Error();
      error.status = 404;
      error.message = "ProductColor not found";
      throw error;
    }
    console.log("listDataColor", listDataColor)
    await productColorModel.deleteMany({ productId })
    let index = 0;
    for (let i = 0; i < listDataColor.length; i++) {
      if (listDataColor[i].image && JSON.stringify(listDataColor[i].image) !== JSON.stringify({})) {
        const newProductColor = new productColorModel({
          _id: new mongoose.Types.ObjectId(),
          image: listDataColor[i].image.replace("http://localhost:5000", ""),
          colorId: listDataColor[i].id,
          productId
        })

        await newProductColor.save();
      } else {
        const newProductColor = new productColorModel({
          _id: new mongoose.Types.ObjectId(),
          image: imageNew[index],
          colorId: listDataColor[i].id,
          productId
        })
        await newProductColor.save();
        index++;
      }

    }
    res.status(200).send({
      message: "Update productColor success",
    })
  } catch (e) {
    next(e);
  }
}

const deleteProductColor = async (req, res, next) => {
  try {
    const { _id } = req.query;
    const productColorFound = await productColorModel.findById({ _id });

    if (!productColorFound) {
      let error = new Error();
      error.status = 404;
      error.message = "ProductColor not found";
      throw error;
    }

    await productColorModel.deleteOne({ _id });

    res.status(200).send({
      message: "Delete productColor success"
    })
  } catch (e) {
    next(e);
  }
}