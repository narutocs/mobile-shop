import mongoose from "mongoose";

import productModel from "../models/product.model";
import productColorModel from "../models/productColor.model";
import { createProductColor } from "./productColor.controller";
import orderDetailModel from "../models/orderDetail.model";

export { createProduct, updateProduct, deleteProduct, getListProduct, getOneProduct, getListNewProduct }

const getOneProduct = async (req, res, next) => {
  try {

    const { _id } = req.params;

    const productFound = await productModel.findById({ _id });

    if (!productFound) {
      let error = new Error();
      error.status = 401;
      error.message = "Product not found";
      throw error;
    }

    res.status(200).send({
      message: "Get product success",
      data: {
        product: productFound
      }
    })
  } catch (e) {
    next(e);
  }
}

const getListProduct = async (req, res, next) => {
  try {
    const listData = [];

    const { page = 1, amount = 10, sort = -1, search = "" } = req.query;

    let total = await productModel.find({ name: { $regex: search, $options: "$i" } }).count();
    let listProduct = [];

    if (search.length > 0) {
      total = await productModel.find({ name: { $regex: search, $options: "$i" } }).count();
      listProduct = await productModel.find({ name: { $regex: search, $options: "$i" } }).limit(+amount)
        .skip(+amount * (+page - 1)).sort({ price: sort });

    } else {
      total = await productModel.find().count();
      listProduct = await productModel.find().limit(+amount)
        .skip(+amount * (+page - 1)).sort({ price: sort });

    }


    for (let i = 0; i < listProduct.length; i++) {
      const data = await productColorModel.find({ productId: listProduct[i]._id }).populate(['productId', 'colorId']);
      listData.push(data);
    }

    if (listProduct.length === 0) {
      let error = new Error();
      error.status = 404;
      error.message = "List product is empty";
      throw error;
    }

    const result = {
      message: "Get list product success",
      data: {
        listData
      },
      pagination: {
        total,
        page,
        amount,
        sort,
        search
      }
    }

    console.log("result", result)
    res.status(200).send(result);
  } catch (e) {
    next(e);
  }
}

const createProduct = async (req, res, next) => {
  try {

    const { name, price, quality, ram, screen, cpu, batteries, operating_system, internal_storage, primary_camera, secondary_camera, idBrand } = req.body;
    const currentTime = new Date();
    const newProduct = new productModel({
      _id: new mongoose.Types.ObjectId(),
      name,
      price,
      screen,
      cpu,
      batteries,
      operating_system,
      internal_storage,
      primary_camera,
      secondary_camera,
      quality,
      ram,
      idBrand,
      create_at: currentTime.getTime()
    })

    createProductColor()

    await newProduct.save();

    res.status(200).send({
      message: "Create product success",
      idProduct: newProduct._id
    })
  } catch (e) {
    next(e);
  }
}

const updateProduct = async (req, res, next) => {
  try {
    const { _id } = req.params;

    const productFound = await productModel.findById({ _id });
    if (!productFound) {
      let error = new Error();
      error.status = 404;
      error.message = "Product not found";
      throw error;
    }
    const { name = productFound.name, description = productFound.description,
      price = productFound.price, quality = productFound.quality, technical_detail = productFound.technical_detail
      , idBrand = productFound.idBrand } = req.body;

    await productModel.updateOne({ _id }, { name, description, price, quality, idBrand, technical_detail });

    res.status(200).send({
      message: "Update product success"
    })
  } catch (e) {
    next(e);
  }
}

const deleteProduct = async (req, res, next) => {
  try {
    const { _id } = req.params;
    const productFound = await productModel.findById({ _id });
    if (!productFound) {
      let error = new Error();
      error.status = 404;
      error.message = "Product not found";
      throw error;
    }

    const productColorFound = await productColorModel.find({ productId: _id });
    for (let i = 0; i < productColorFound.length; i++) {
      const orderDetailFound = await orderDetailModel.find({ idProductColor: productColorFound[i]._id })
      if (orderDetailFound.length > 0) {
        let error = new Error();
        error.status = 400;
        error.message = "Product can't delete";
        throw error;
      }
    }

    await productModel.deleteOne({ _id });

    await productColorModel.deleteMany({ productId: _id })

    res.status(200).send({
      message: "Delete product success"
    })
  } catch (e) {
    next(e);
  }
}

const getListNewProduct = async (req, res, next) => {
  try {

    const listProduct = await productModel.find().sort({ create_at: -1 }).limit(10);
    const listNewProduct = [];

    for (let i = 0; i < listProduct.length; i++) {
      const data = await productColorModel.find({ productId: listProduct[i]._id }).populate(['productId']);
      listNewProduct.push(data[0]);
    }

    res.status(200).send({
      data: {
        listNewProduct
      }
    })
  } catch (e) {
    next(e);
  }
}