import mongoose from "mongoose";

import brandModel from "../models/brand.model";

export { createBrand, updateBrand, deleteBrand, getBrand, getListBrand }

const createBrand = async (req, res, next) => {
  try {
    const { name } = req.body;

    const { file } = req;
    if (!file) {
      let error = new Error();
      error.status = 401;
      error.message = "Please input logo";
      throw error;
    }
    const { originalname } = file;
    const logo = `/public/img/${originalname}`;
    const newBrand = new brandModel({
      _id: new mongoose.Types.ObjectId(),
      name,
      logo,
    })
    await newBrand.save();

    res.status(200).send({
      message: "Create brand success"
    })
  } catch (e) {
    next(e)
  }
}

const updateBrand = async (req, res, next) => {
  try {
    const { _id } = req.params;
    const brandFound = await brandModel.findById({ _id });
    if (!brandFound) {
      let error = new Error();
      error.status = 401;
      error.message = "Brand not found";
      throw error;
    }
    const { file } = req;
    let logo = brandFound.logo;
    if (file) {
      const { originalname } = file;
      logo = `/public/img/${originalname}`;
    }


    const { name = brandFound.name } = req.body;
    await brandModel.updateOne({ _id }, { name, logo })

    res.status(200).send({
      message: 'Update brand success'
    })
  } catch (e) {
    next(e)
  }
}

const deleteBrand = async (req, res, next) => {
  try {
    const { _id } = req.params;
    const brandFound = await brandModel.findById({ _id });
    if (!brandFound) {
      let error = new Error();
      error.status = 401;
      error.message = "Brand not found";
      throw error;
    }
    await brandModel.deleteOne({ _id });
    res.status(200).send({
      message: "Delete brand success"
    })
  } catch (e) {
    next(e)
  }
}

const getBrand = async (req, res, next) => {
  try {
    const { _id } = req.query;
    const brandFound = await brandModel.findById({ _id });
    if (!brandFound) {
      let error = new Error();
      error.status = 401;
      error.message = "Brand not found";
      throw error;
    }
    res.status(200).send({
      message: "get brand success",
      data: {
        brand: brandFound
      }
    })
  } catch (e) {
    next(e)
  }
}

const getListBrand = async (req, res, next) => {
  try {
    const listBrand = await brandModel.find();
    if (listBrand.length === 0) {
      let error = new Error();
      error.status = 401;
      error.message = "List brand is empty";
      throw error;
    }

    res.status(200).send({
      message: "Get list brand success",
      data: {
        listBrand
      }
    })
  } catch (e) {
    next(e)
  }
}