import mongoose from "mongoose";
import bcrypt from "bcrypt";

import userModel from "../models/user.model";
import transporter from "../libs/sendEmail";
import orderDetailModel from "../models/orderDetail.model";

export const getListUser = async (req, res, next) => {
  try {

    const { page = 1, amount = 10, sort = -1, search = "" } = req.query;

    let total = await userModel.find({ fullName: { $regex: search, $options: "$i" } }).count();
    let listUser = [];

    if (search.length > 0) {
      total = await userModel.find({ fullName: { $regex: search, $options: "$i" } }).count();
      listUser = await userModel.find({ fullName: { $regex: search, $options: "$i" } }).limit(+amount)
        .skip(+amount * (+page - 1)).sort({ fullName: sort }).populate("idRole");
    } else {
      total = await userModel.find().count();

      listUser = await userModel.find().limit(+amount)
        .skip(+amount * (+page - 1)).sort({ fullName: sort }).populate("idRole");

    }

    if (listUser.length === 0) {
      let error = new Error();
      error.status = 404;
      error.message = "List user is empty";
      throw error;
    }

    const result = {
      message: "Get list user success",
      data: {
        listUser
      },
      pagination: {
        total,
        page,
        amount,
        sort,
        search
      }
    }
    res.status(200).send(result);
  } catch (e) {
    next(e);
  }
}

export const changePassword = async (req, res, next) => {
  try {
    const { password, currentPassword } = req.body;
    const { _id } = req.decode;
    const userFound = await userModel.findOne({ _id });

    if (!userFound) {
      let error = new Error();
      error.status = 404;
      error.message = "User not found";
      throw error;
    }
    const resultCompare = await bcrypt.compare(currentPassword, userFound.password);
    if (resultCompare) {
      const passwordHash = await bcrypt.hash(password, +process.env.saltRound);
      await userModel.updateOne({ _id }, { password: passwordHash })

      res.status(200).send({
        message: "Change password success"
      })
    } else {
      let error = new Error();
      error.status = 401;
      error.message = "Lỗi xác thực";
      throw error;
    }
  } catch (e) {
    next(e)
  }
}

export const updateProfile = async (req, res, next) => {
  try {
    const { _id } = req.params;
    const { file } = req;
    const userFound = await userModel.findById({ _id });
    if (!userFound) {
      let error = new Error();
      error.status = 401;
      error.message = "User not found";
      throw error;
    }
    let { avatar } = userFound;
    if (file) {
      const { originalname } = file;
      avatar = `/public/img/${originalname}`;
    }

    const { email = userFound.email, dateOfBirth = userFound.dateOfBirth,
      fullName = userFound.fullName, phoneNumber = userFound.phoneNumber, gender = userFound.gender } = req.body;

    await userModel.updateOne({ _id }, { email, phoneNumber, gender, dateOfBirth, avatar, fullName })
    res.status(200).send({
      message: "Update user success"
    })
  } catch (e) {
    next(e)
  }
}

export const getProfile = async (req, res, next) => {
  try {
    const { _id } = req.params;
    const userFound = await userModel.findById({ _id });
    if (!userFound) {
      let error = new Error();
      error.status = 401;
      error.message = "User not found";
      throw error;
    }
    res.status(200).send({
      message: "Get user success",
      user: userFound
    })
  } catch (e) {
    next(e);
  }
}

export const forgotPassword = async (req, res, next) => {
  try {
    const { email } = req.body;
    const userFound = await userModel.findOne({ email });
    if (!userFound) {
      let error = new Error();
      error.status = 401;
      error.message = "User not found";
      throw error;
    }
    let codeActive = Math.floor(100000 + Math.random() * 900000);
    transporter.sendMail({
      from: "Shopping",
      to: email,
      subject: "Code change password",
      html: "<h1>" + "Your code to change password is: " + codeActive + "</h1>"
    });

    await userModel.updateOne({ email }, { code: codeActive })

    res.status(200).send({
      message: 'Please check your email to get code to change new password'
    })
  } catch (e) {
    next(e)
  }
}

export const createUser = async (req, res, next) => {
  try {
    console.log("zo")
    const { role, fullName, phoneNumber, email } = req.body;
    const passwordHash = await bcrypt.hash('123456', +process.env.saltRound);
    const newUser = new userModel({
      _id: new mongoose.Types.ObjectId(),
      idRole: role,
      fullName,
      email,
      phoneNumber,
      password: passwordHash
    })

    await newUser.save();

    res.status(200).send({
      message: "Create user success"
    })
  } catch (e) {
    next(e);
  }
}

export const updateUser = async (req, res, next) => {
  try {
    const { _id } = req.params;
    const userFound = await userModel.findOne({ _id });
    if (!userFound) {
      let error = new Error();
      error.status = 401;
      error.message = "User not found";
      throw error;
    }
    const { email = userFound.email, phoneNumber = userFound.phoneNumber, fullName = userFound.fullName, role = userFound.idRole } = req.body;
    await userModel.updateOne({ _id }, { email, phoneNumber, fullName, idRole: role })

    res.status(200).send({
      message: "Update user success"
    })
  } catch (e) {
    next(e);
  }
}

export const deleteUser = async (req, res, next) => {
  try {
    const { _id } = req.params;
    const userFound = await userModel.findOne({ _id });
    if (!userFound) {
      let error = new Error();
      error.status = 401;
      error.message = "User not found";
      throw error;
    }

    const orderDetailFound = await orderDetailModel.find({ idSeller: _id });
    if (orderDetailFound.length > 0) {
      let error = new Error();
      error.status = 401;
      error.message = "can't delete this user";
      throw error;
    }
    await userModel.deleteOne({ _id })

    res.status(200).send({
      message: "Delete user success"
    })
  } catch (e) {
    next(e);
  }
}


export const changePasswordForgot = async (req, res, next) => {
  try {
    const { password, email, code } = req.body;
    const userFound = await userModel.findOne({ email });

    if (!userFound) {
      let error = new Error();
      error.status = 404;
      error.message = "User not found";
      throw error;
    }
    if (userFound.code === code) {
      let error = new Error();
      error.status = 401;
      error.message = "Lỗi xác thực";
      throw error;
    }


    const passwordHash = await bcrypt.hash(password, +process.env.saltRound);
    await userModel.updateOne({ email }, { password: passwordHash })

    res.status(200).send({
      message: "Change password success"
    })

  } catch (e) {
    next(e)
  }
}