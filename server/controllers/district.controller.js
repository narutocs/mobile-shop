import mongoose from "mongoose";

import districtModel from "../models/district.model";

export { createDistrict, getListDistrict }

const createDistrict = async (name, idCity, id) => {
  const newDistrict = new districtModel({
    _id: new mongoose.Types.ObjectId(),
    name,
    id,
    idCity
  })

  await newDistrict.save();
}

const getListDistrict = async (req, res, next) => {
  try {

    const { id } = req.params;

    const listDistrict = await districtModel.find({ idCity: id })

    res.status(200).send({
      message: "Get list District success",
      data: {
        listDistrict
      }
    })
  } catch (e) {
    next(e)
  }
}