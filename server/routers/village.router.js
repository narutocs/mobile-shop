import { Router } from "express";

import { getListVillage } from "../controllers/village.controller";

const router = Router();

router.get("/:id", getListVillage);

export default router; 