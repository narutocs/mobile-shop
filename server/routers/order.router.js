import { Router } from "express";

import { createOrder, deleteOrder, getListOrder, getOneOrder, getListOrderByUser, updateOrder } from "../controllers/order.controller";

const router = Router();

router.get("/:idUser", getListOrderByUser);

router.get("/", getListOrder);

router.post("/", createOrder);

router.delete("/:_id", deleteOrder);

router.put("/:_id", updateOrder);

export default router; 