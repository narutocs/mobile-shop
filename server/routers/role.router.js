import { Router } from "express";

import { getList } from "../controllers/role.controller";

const router = Router();

router.get("/", getList);

export default router; 