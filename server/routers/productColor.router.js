import { Router } from "express";

import UploadFile from "../libs/UploadFile";
import { updateProductColor, deleteProductColor, createProductColor, getListProductColorByProduct, getProductById } from "../controllers/productColor.controller";

const router = Router();

router.get('/:_id', getProductById);

router.get("/", getListProductColorByProduct);

router.put("/", UploadFile.array('image', 20), updateProductColor);

router.post("/", UploadFile.single('image'), createProductColor);

router.delete("/:_id", deleteProductColor);

export default router;