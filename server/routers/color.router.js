import { Router } from "express";

import { createColor, getOneColor, updateColor, deleteColor, getListColor } from "../controllers/color.controller";

const router = Router();

router.get("/", getListColor);

router.get('/:_id', getOneColor);

router.put("/:_id", updateColor);

router.post("/", createColor);

router.delete("/:_id", deleteColor);

export default router; 