import { Router } from "express";

import { createProduct, updateProduct, deleteProduct, getListProduct, getOneProduct, getListNewProduct } from "../controllers/product.controller";

const router = Router();

router.get("/list-new-product", getListNewProduct);

router.get("/", getListProduct);

router.get('/:_id', getOneProduct);

router.put("/:_id", updateProduct);

router.post("/", createProduct);

router.delete("/:_id", deleteProduct);

export default router;