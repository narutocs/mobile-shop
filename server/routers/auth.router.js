import { Router } from "express";

import { login, register, activeAccount } from "../controllers/auth.controller";

const router = Router();

router.post('/register', register);

router.post('/login', login)

router.put("/active", activeAccount)

export default router;