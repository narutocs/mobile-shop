import { Router } from "express";

import UploadFile from "../libs/UploadFile";
import { createSlider, updateSlider, deleteSlider, getSlider, getListSlider } from "../controllers/slider.controller";

const router = Router();

router.get("/", getListSlider);

router.get('/:_id', getSlider);

router.put("/:_id", UploadFile.single("image"), updateSlider);

router.post("/", UploadFile.single("image"), createSlider);

router.delete("/:_id", deleteSlider);

export default router;