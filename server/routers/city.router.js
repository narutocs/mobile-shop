import { Router } from "express";

import { getListCity } from "../controllers/city.controller";

const router = Router();

router.get("/", getListCity);

export default router; 