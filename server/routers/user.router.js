import { Router } from "express";

import UploadFile from "../libs/UploadFile";
import { createUser, deleteUser, updateUser, changePasswordForgot, forgotPassword, getProfile, updateProfile, changePassword, getListUser } from "../controllers/user.controller";
import { checkToken } from "../middleware/auth";

const router = Router();

router.get("/admin/:_id", getProfile)

router.post("/admin/", createUser);

router.delete('/admin/:_id', deleteUser);

router.put("/admin/:_id", updateUser);

router.get('/', getListUser)

router.get("/:_id", checkToken, getProfile)

router.put("/:_id", checkToken, UploadFile.single('avatar'), updateProfile)

router.post('/forgot-password', forgotPassword);

router.put("/change-password-user", checkToken, changePassword);

router.put('/change-password', changePasswordForgot)



export default router;