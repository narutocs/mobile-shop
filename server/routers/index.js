import { Router } from "express";

import userRouter from "./user.router";
import authRouter from "./auth.router";
import productRouter from "./product.router";
import brandRouter from "./brand.router";
import colorRouter from "./color.router";
import productColorRouter from "./productColor.router";
import cityRouter from "./city.router";
import districtRouter from "./district.router";
import villageRouter from "./village.router";
import orderRouter from "./order.router";
import roleRouter from "./role.router";
import sliderRouter from "./slider.router";

const router = Router();

router.use('/user', userRouter);

router.use('/auth', authRouter);

router.use("/product", productRouter);

router.use("/brand", brandRouter);

router.use("/color", colorRouter);

router.use("/product-color", productColorRouter);

router.use("/village", villageRouter);

router.use("/city", cityRouter);

router.use("/district", districtRouter);

router.use('/order', orderRouter);

router.use("/role", roleRouter);

router.use('/slider', sliderRouter);

export default router;