import { Router } from "express";

import UploadFile from "../libs/UploadFile";
import { createBrand, updateBrand, deleteBrand, getBrand, getListBrand } from "../controllers/brand.controller";

const router = Router();

router.get("/", getListBrand);

router.get('/:_id', getBrand);

router.put("/:_id", UploadFile.single("logo"), updateBrand);

router.post("/", UploadFile.single("logo"), createBrand);

router.delete("/:_id", deleteBrand);

export default router;