import { Router } from "express";

import { getListDistrict } from "../controllers/district.controller";

const router = Router();

router.get("/:id", getListDistrict);

export default router; 