import * as ActionType from "../constants/ActionType";

export const actionLoading = () => {
  return {
    type: ActionType.USER_LOADING
  }
}

export const actionLoaded = () => {
  return {
    type: ActionType.USER_LOADED
  }
}