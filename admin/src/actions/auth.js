import * as ActionType from "../constants/ActionType";
import authApi from "../api/authApi";
import { actionSetMgsSuccess, actionSetMgsError, actionClearMgs } from "./message";
import { actionLoading, actionLoaded } from "./loading";
import jwt from "jwt-client";

export const actionRegister = (user) => {
  return async (dispatch, getState) => {
    try {
      dispatch(actionLoading());
      const result = await authApi.register(user)
      const { message = '' } = result;
      dispatch(actionSetMgsSuccess(message));
      dispatch(actionClearMgs());
      dispatch(actionRegisterSuccess())
      dispatch(actionLoaded())
    } catch (e) {
      dispatch(actionSetMgsError(e))
      dispatch(actionRegisterFail())
      dispatch(actionClearMgs());
      dispatch(actionLoaded())
    }
  }
}
export const actionLogout = () => {

  localStorage.removeItem("TOKEN");

  localStorage.removeItem("USER");

  localStorage.removeItem("CART")

  return {
    type: ActionType.LOG_OUT
  }
}

export const actionLogin = (user) => {
  return async (dispatch, getState) => {
    try {
      dispatch(actionLoading());
      const result = await authApi.login(user);
      const { message, data: { token = "", user: userApi = {} } = {} } = result;

      const { role } = jwt.read(token).claim;
      if (role === 'USER') {
        dispatch(actionLoginFail())
        dispatch(actionSetMgsError("You not a Admin"))
        return
      }
      dispatch(actionLoginSuccess(token, userApi))
      localStorage.setItem("TOKEN", token);
      localStorage.setItem("USER", JSON.stringify(userApi));
      dispatch(actionClearMgs());
      dispatch(actionLoaded())
    } catch (e) {
      dispatch(actionSetMgsError(e))
      dispatch(actionLoginFail())
      dispatch(actionClearMgs());
      dispatch(actionLoaded())
    }
  }
}

export const actionFailActive = () => {
  return {
    type: ActionType.LOGIN_FAIL_ACTIVE
  }
}

export const actionLoginSuccess = (token, user) => {
  return {
    type: ActionType.LOGIN_SUCCESS,
    payload: {
      token,
      user
    }
  }
}

export const actionLoginFail = () => {
  return {
    type: ActionType.LOGIN_FAIL
  }
}

export const actionRegisterSuccess = () => {
  return {
    type: ActionType.REGISTER_SUCCESS
  }
}

export const actionRegisterFail = () => {
  return {
    type: ActionType.REGISTER_FAIL
  }
}

export const actionActiveSuccess = () => {
  return {
    type: ActionType.ACTIVE_SUCCESS
  }
}

export const actionActiveFail = () => {
  return {
    type: ActionType.ACTIVE_FAIL
  }
}

export const actionActiveAccount = (email, code) => {
  return async dispatch => {

    try {
      dispatch(actionLoading());
      const result = await authApi.active({ email, code });
      const { message = '' } = result;
      dispatch(actionSetMgsSuccess(message))
      dispatch(actionClearMgs());
      dispatch(actionActiveSuccess())
      dispatch(actionLoaded())
    } catch (e) {
      dispatch(actionSetMgsError(e))
      dispatch(actionActiveFail())
      dispatch(actionClearMgs());
      dispatch(actionLoaded());
    }
  }
}
