import * as ActionType from "../constants/ActionType";
import orderApi from "../api/orderApi";
import { actionClearMgs, actionSetMgsError } from "./message";
import { actionLoaded, actionLoading } from "./loading";

export const actionCreateOrder = (order) => {
  return async dispatch => {
    try {
      dispatch(actionLoading());

      const { data } = await orderApi.createOrder(order);

      dispatch(actionCreateOrderSuccess(data.idOrder))
      localStorage.removeItem("CART");
      dispatch(actionClearMgs());
      dispatch(actionLoaded())
    } catch (e) {

      dispatch(actionSetMgsError(e))
      dispatch(actionClearMgs());
      dispatch(actionLoaded());
    }
  }
}

export const actionGetListOrder = (params = {}) => {
  return async dispatch => {
    try {
      dispatch(actionLoading());

      const { data, pagination } = await orderApi.getList(params);
      dispatch(actionGetListOrderSuccess(data.listResult[0], pagination))

      dispatch(actionClearMgs());
      dispatch(actionLoaded())
    } catch (e) {

      dispatch(actionSetMgsError(e))
      dispatch(actionClearMgs());
      dispatch(actionLoaded());
    }
  }
}

export const actionGetListOrderSuccess = (listOrder, pagination) => {
  return {
    type: ActionType.GET_LIST_ORDER_SUCCESS,
    payload: {
      listOrder,
      pagination
    }
  }
}

export const actionCreateOrderSuccess = (id) => {
  return {
    type: ActionType.CREATE_ORDER_SUCCESS,
    payload: {
      orderId: id
    }
  }
}