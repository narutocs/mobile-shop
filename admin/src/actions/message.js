import * as ActionType from "../constants/ActionType";

export const actionSetMgsSuccess = message => {
  return {
    type: ActionType.SET_SUCCESS,
    payload: {
      message
    }
  }
}

export const actionSetMgsError = message => {
  return {
    type: ActionType.SET_ERROR,
    payload: {
      message
    }
  }
}

export const actionClearMgs = () => {
  return {
    type: ActionType.CLEAR_MESSAGE,
  }
}