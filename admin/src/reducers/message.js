import * as ActionType from "../constants/ActionType";
import MessageType from "../constants/MessageType";

const initialState = {
  message: '',
  type: null
}

const messageReducers = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.SET_SUCCESS:
      const { payload: {
        message: messageSuccess = '',
      } = {} } = action;
      return {
        message: messageSuccess,
        type: MessageType.SUCCESS
      }
    case ActionType.SET_ERROR:
      const { payload: {
        message: messageError = '',
      } = {} } = action;
      return {
        message: messageError,
        type: MessageType.FAIL
      }
    case ActionType.CLEAR_MESSAGE:
      return {
        message: '',
        type: null
      }
    default: return state;
  }
}
export default messageReducers;