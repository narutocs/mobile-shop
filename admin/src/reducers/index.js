import { combineReducers } from "redux";

import userReducers from "./user";
import authReducers from "./auth";
import messageReducers from "./message";
import loadingReducers from "./loading";
import orderReducers from "./order";
const appReducers = combineReducers({
  user: userReducers,
  message: messageReducers,
  isLoading: loadingReducers,
  auth: authReducers,
  order: orderReducers
})

export default appReducers;