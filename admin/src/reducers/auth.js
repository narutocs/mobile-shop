import * as ActionType from "../constants/ActionType";

const initialState = {
  isRemember: false,
  token: localStorage.getItem("TOKEN") || null,
  user: JSON.parse(localStorage.getItem("USER")) || null,
  isRegisterSuccess: false,
  isAuthenticated: false
}

const authReducers = (state = initialState, action) => {
  switch (action.type) {
    case ActionType.LOG_OUT:
      localStorage.clear();
      return {
        isRemember: false,
        token: null,
        user: null,
        isRegisterSuccess: false,
        isAuthenticated: false
      }
    case ActionType.REMEMBER_ACCOUNT:
      const newState = { ...state };
      newState.isRemember = true;
      return newState;
    case ActionType.LOGIN_SUCCESS:
      const { payload: {
        token = '',
        user = null,
        isRemember = false
      } = {} } = action;
      return {
        ...state,
        token,
        user,
        isRemember,
        isAuthenticated: true
      }
    case ActionType.LOGIN_FAIL:
      return {
        ...state,
        token: null,
        user: null,
      }
    case ActionType.LOGIN_FAIL_ACTIVE:
      return {
        ...state,
        isAuthenticated: false
      }
    case ActionType.USER_LOADING:
      return {
        ...state,
      }
    case ActionType.REGISTER_SUCCESS:
      return {
        ...state,
        isRegisterSuccess: true,
      }
    case ActionType.REGISTER_FAIL:
      return {
        ...state,
        isRegisterSuccess: false,
      }
    case ActionType.USER_LOADING:
      return {
        ...state,
      }
    case ActionType.GET_USER_SUCCESS:
      const { user: userPayload } = action.payload;
      return {
        ...state,
        user: userPayload
      }
    case ActionType.UPDATE_USER_SUCCESS:
      return {
        ...state,
        user: {
          ...user,
          ...action.payload.user
        }
      }
    case ActionType.CHANGE_PASSWORD_SUCCESS:
      return {
        ...state,
      }
    case ActionType.LOGOUT:
      return {
        ...state,
        token: null,
        user: null,
        isAuthenticated: false
      }
    default: return state;
  }
}

export default authReducers;