const MessageType = {
  SUCCESS: "SUCCESS",
  FAIL: "FAIL"
}

export default MessageType;