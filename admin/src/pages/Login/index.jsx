import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { Modal, Input, Button, Checkbox, Card } from "antd";
import {
  UserOutlined,
  LockOutlined,
  FacebookFilled,
  GooglePlusSquareFilled,
} from "@ant-design/icons";
import { useFormik } from "formik";
import * as yup from "yup";
import { useDispatch, useSelector } from "react-redux";

import MessageValidationError from "../../components/MessageValidationError";
import { actionLogin } from "../../actions/auth";
import Loading from "../../components/Loading";
import "./style.scss";

Login.propTypes = {
  isShowModal: PropTypes.bool,
  onCloseModel: PropTypes.func,
  onShowRegister: PropTypes.func,
  onShowCodeActive: PropTypes.func,
  onShowForgotPassword: PropTypes.func,
  isLogin: PropTypes.bool,
};

Login.defaultProps = {
  isShowModal: false,
  onCloseModel: null,
  onShowRegister: null,
  onShowCodeActive: null,
  onShowForgotPassword: null,
};

function Login(props) {
  const {
    isShowModal,
    onCloseModel,
    onShowRegister,
    onShowCodeActive,
    onShowForgotPassword,
  } = props;

  // const { token, user } = useSelector((state) => state.auth);

  const dispatch = useDispatch();

  const handleCancel = () => {
    if (onCloseModel) {
      onCloseModel();
    }
  };

  const initialValues = {
    email: "",
    password: "",
  };

  const handleShowRegister = () => {
    if (onShowRegister) {
      onShowRegister();
    }
  };

  const validationSchema = yup.object().shape({
    email: yup
      .string()
      .required("Please input your email")
      .matches(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        "Your email incorrect format"
      ),
    password: yup
      .string()
      .required("Please input your password")
      .min(6, "Your password has from 6 to 12 characters")
      .max(12, "Your password has from 6 to 12 characters"),
  });

  const formik = useFormik({
    validationSchema,
    initialValues,
    onSubmit: async (values) => {
      await dispatch(actionLogin(values));
    },
  });

  const {
    values,
    handleBlur,
    handleChange,
    handleSubmit,
    errors,
    touched,
  } = formik;

  const resetForm = () => {
    formik.setValues({
      email: "",
      password: "",
    });
    formik.setTouched({});
    formik.setErrors({});
  };

  useEffect(() => {
    resetForm();
  }, [isShowModal]);

  // useEffect(() => {
  //   if (user && !user.isActive) {
  //     console.log("active");
  //     if (onShowCodeActive) {
  //       onShowCodeActive(user.email, true);
  //     }
  //   }
  // }, [user]);

  // useEffect(() => {
  //   if (token) {
  //     console.log("close");
  //     if (onCloseModel) {
  //       onCloseModel();
  //     }
  //   }
  // }, [token]);

  const handleForgotPassword = () => {
    if (onShowForgotPassword) {
      onShowForgotPassword();
    }
  };
  return (
    <Card
      style={{
        width: 500,
        height: 300,
        display: "flex",
        marginLeft: "auto",
        marginRight: "auto",
      }}
      className="card"
    >
      <div className="row">
        <div className="title-login">LOGIN</div>
      </div>
      <form onSubmit={handleSubmit} class="login">
        <div className="row mb-4">
          <div className="col-md-12">
            <Input
              className={` ${
                errors.email && touched.email ? "validate-form-error" : ""
              }`}
              name="email"
              prefix={<UserOutlined className="site-form-item-icon " />}
              placeholder="Email"
              value={values.email}
              onChange={handleChange}
              onBlur={handleBlur}
            />
            <MessageValidationError
              touchedOop={touched.email}
              messageErr={errors.email}
            />
          </div>
        </div>
        <div className="row mb-4">
          <div className="col-md-12">
            <Input.Password
              name="password"
              className={`${
                errors.password && touched.password ? "validate-form-error" : ""
              }`}
              value={values.password}
              onChange={handleChange}
              onBlur={handleBlur}
              prefix={<LockOutlined className="site-form-item-icon " />}
              type="password"
              placeholder="Password"
            />
            <MessageValidationError
              touchedOop={touched.password}
              messageErr={errors.password}
            />
          </div>
        </div>

        <div className="row">
          <div className="col-md-12 d-flex">
            <Button
              htmlType="submit"
              className="login-form-button flex-fill primary"
            >
              Log in
            </Button>
          </div>
        </div>
      </form>
    </Card>
  );
}

export default Login;
