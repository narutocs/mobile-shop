import React, { useState } from "react";
import PropTypes from "prop-types";
import { Modal, Button, Card, Input } from "antd";
import { DeleteOutlined, MinusOutlined, PlusOutlined } from "@ant-design/icons";
import { Table, Label } from "reactstrap";
import orderApi from "../../../api/orderApi";
import { BASE_URL } from "../../../constants";
import { useDispatch, useSelector } from "react-redux";
import { Button as Btn } from "reactstrap";
import "./style.scss";
import { actionSetMgsSuccess } from "../../../actions/message";

DetailOrder.propTypes = {
  isShowModal: PropTypes.bool,
  onCloseModel: PropTypes.func,
  currentOrder: PropTypes.object,
};

DetailOrder.defaultProps = {
  isShowModal: false,
  onCloseModel: null,
  currentOrder: null,
};

function DetailOrder(props) {
  const { isShowModal, onCloseModel, currentOrder } = props;
  const handleCancel = () => {
    if (onCloseModel) {
      onCloseModel();
    }
  };

  const _id = useSelector((state) => state.auth.user._id);

  const priceFormat = (price) => {
    return parseFloat((price + "").replace(/,/g, ""))
      .toString()
      .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };

  const handleReject = async () => {
    await orderApi.update(currentOrder.idOrder._id, {
      status: "Reject",
      idSeller: _id,
    });
    dispatch(actionSetMgsSuccess("Reject order success"));
    if (onCloseModel) {
      onCloseModel(true);
    }
  };
  const handleDelivering = async () => {
    await orderApi.update(currentOrder.idOrder._id, {
      status: "Delivering",
      idSeller: _id,
    });
    dispatch(actionSetMgsSuccess("Delivering order success"));
    if (onCloseModel) {
      onCloseModel(true);
    }
  };
  const handleDelivered = async () => {
    await orderApi.update(currentOrder.idOrder._id, {
      status: "Delivered",
      idSeller: _id,
    });
    dispatch(actionSetMgsSuccess("Delivered order success"));
    if (onCloseModel) {
      onCloseModel(true);
    }
  };

  const dispatch = useDispatch();
  if (currentOrder) {
    console.log(
      currentOrder && currentOrder.idOrder.status === "Delivered" ? true : false
    );
  }
  return (
    <Modal
      onCancel={handleCancel}
      footer={null}
      title="Order Detail"
      visible={isShowModal}
    >
      <Card style={{ width: "100%", height: "500px" }}>
        <div className="row">
          <div className="col-md-12">
            <Table borderless>
              <thead>
                <tr>
                  <th>Product</th>
                  <th>Quality</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
                {currentOrder && (
                  <tr>
                    <td className="d-flex align-items-center">
                      <img
                        className="product-image mr-3"
                        style={{ width: 50, height: 50 }}
                        src={`${BASE_URL}${currentOrder.idProductColor.image}`}
                      />
                      <div className="product-info">
                        <div>{currentOrder.idProductColor.productId.name}</div>
                        <div>
                          {priceFormat(
                            currentOrder.idProductColor.productId.price
                          )}{" "}
                          vnđ
                        </div>
                      </div>
                    </td>
                    <td className="align-middle">
                      <Label className="text-center">
                        {currentOrder.amount}
                      </Label>
                    </td>
                    <td className="align-middle">
                      <Label className="text-center">
                        {priceFormat(
                          +currentOrder.idProductColor.productId.price *
                            currentOrder.amount
                        )}{" "}
                        vnđ
                      </Label>
                    </td>
                  </tr>
                )}

                {currentOrder &&
                  currentOrder.orders &&
                  currentOrder.orders.map((order) => {
                    return (
                      <tr>
                        <td className="d-flex align-items-center">
                          <img
                            className="product-image mr-3"
                            style={{ width: 50, height: 50 }}
                            src={`${BASE_URL}${order.idProductColor.image}`}
                          />
                          <div className="product-info">
                            <div>{order.idProductColor.productId.name}</div>
                            <div>
                              {priceFormat(
                                order.idProductColor.productId.price
                              )}{" "}
                              vnđ
                            </div>
                          </div>
                        </td>
                        <td className="align-middle">
                          <Label className="text-center">{order.amount}</Label>
                        </td>
                        <td className="align-middle">
                          <Label className="text-center">
                            {priceFormat(
                              +order.idProductColor.productId.price *
                                order.amount
                            )}{" "}
                            vnđ
                          </Label>
                        </td>
                      </tr>
                    );
                  })}
              </tbody>
            </Table>
          </div>
        </div>

        <div
          className="row"
          style={{ position: "absolute", bottom: 0, width: "100%" }}
        >
          <div
            className="col-md-12"
            style={{ display: "flex", justifyContent: "space-between" }}
          >
            <Btn
              className="btn"
              style={{ width: 100, height: 30, fontSize: "1.6rem" }}
              onClick={handleReject}
              color="danger"
              hidden={
                currentOrder &&
                (currentOrder.idOrder.status === "Delivered" ||
                  currentOrder.idOrder.status === "Reject")
                  ? true
                  : false
              }
            >
              Reject
            </Btn>
            <Btn
              className="btn"
              style={{ width: 100, height: 30, fontSize: "1.6rem" }}
              onClick={handleDelivering}
              color="info"
              hidden={
                currentOrder &&
                (currentOrder.idOrder.status === "Delivering" ||
                  currentOrder.idOrder.status === "Delivered" ||
                  currentOrder.idOrder.status === "Reject")
                  ? true
                  : false
              }
            >
              Delivering
            </Btn>
            <Btn
              className="btn"
              style={{ width: 100, height: 30, fontSize: "1.6rem" }}
              onClick={handleDelivered}
              color="success"
              hidden={
                currentOrder &&
                (currentOrder.idOrder.status === "Pending" ||
                  currentOrder.idOrder.status === "Reject" ||
                  currentOrder.idOrder.status === "Delivered")
                  ? true
                  : false
              }
            >
              Delivered
            </Btn>
          </div>
        </div>
      </Card>
    </Modal>
  );
}

export default DetailOrder;
