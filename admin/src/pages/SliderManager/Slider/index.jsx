import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useFormik } from "formik";
import * as yup from "yup";
import { Input, Button, Modal, Upload, message, Select } from "antd";
import MessageValidationError from "../../../components/MessageValidationError";
import EmptyImage from "../../../assets/empty.jpg";
import "./style.scss";
import {
  PlusCircleOutlined,
  EditOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import { SketchPicker } from "react-color";
import { LoadingOutlined, PlusOutlined } from "@ant-design/icons";
import sliderApi from "../../../api/sliderApi";
import productApi from "../../../api/productApi";
import { useDispatch } from "react-redux";
import { useHistory, useLocation } from "react-router-dom";
import roleApi from "../../../api/roleApi";
import {
  actionSetMgsSuccess,
  actionSetMgsError,
} from "../../../actions/message";
import { BASE_URL } from "../../../constants";
import UploadFile from "../../../components/UploadFile";

Slider.propTypes = {};

function Slider(props) {
  const history = useHistory();
  const [defaultUrl, setDefaultUrl] = useState("");
  const location = useLocation();
  const [file, setFile] = useState(null);
  const dispatch = useDispatch();
  const handleUploadSuccess = (url, file) => {
    setFile(file);
  };

  const handleUploadFail = (message) => {};

  const handleSave = async () => {
    if (location.state) {
      try {
        if (file) {
          await sliderApi.update(location.state._id, {
            image: file,
          });
        }

        dispatch(actionSetMgsSuccess("Update slider success"));
        history.goBack();
      } catch (e) {
        dispatch(actionSetMgsError("Update slider fail"));
      }
    } else {
      try {
        if (!file) {
          dispatch(actionSetMgsError("Please choose image"));
          return;
        }
        await sliderApi.create({
          image: file,
        });
        dispatch(actionSetMgsSuccess("Create slider success"));
        history.goBack();
      } catch (e) {
        dispatch(actionSetMgsError("Create slider fail"));
      }
    }
  };
  useEffect(() => {
    if (location.state) {
      setDefaultUrl(`${BASE_URL}${location.state.image}`);
    }
  }, [location.state]);

  return (
    <>
      <div className="row">
        <div className="col-md-6">
          <label htmlFor="">Image:</label>
          <UploadFile
            defaultUrl={defaultUrl}
            onUploadSuccess={handleUploadSuccess}
            onUploadFail={handleUploadFail}
          />
        </div>
      </div>

      <div className="row">
        <div className="col-md-12 d-flex">
          <Button
            onClick={handleSave}
            className="login-form-button flex-fill primary"
          >
            SAVE
          </Button>

          <Button
            className="login-form-button flex-fill"
            onClick={() => history.goBack()}
          >
            CANCEL
          </Button>
        </div>
      </div>
    </>
  );
}

export default Slider;
