import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Table, Button, Pagination } from "antd";
import userApi from "../../api/userApi";
import { BASE_URL } from "../../constants";
import { Route, useHistory } from "react-router-dom";
import { DeleteOutlined, EditOutlined, PlusOutlined } from "@ant-design/icons";
import { useDispatch } from "react-redux";
import { actionSetMgsError, actionSetMgsSuccess } from "../../actions/message";
import sliderApi from "../../api/sliderApi";

BrandManager.propTypes = {};

function BrandManager(props) {
  const [dataSource, setaDataSource] = useState([]);
  const [pagina, setPagina] = useState({
    page: 0,
    total: 10,
    amount: 10,
  });
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    const getListSlider = async () => {
      const {
        data: { listSlider },
      } = await sliderApi.getList();

      const data = [];

      for (let i = 0; i < listSlider.length; i++) {
        data.push({
          image: `${BASE_URL}${listSlider[i].image}`,
          action: listSlider[i],
        });
      }
      setaDataSource(data);
    };

    getListSlider();
  }, []);

  const handleEdit = (data) => {
    history.push({
      pathname: "/edit-slider",
      state: data,
    });
  };

  const handleRemove = async (data) => {
    try {
      await sliderApi.delete(data._id);
      dispatch(actionSetMgsSuccess("Delete slider success"));
      const getListSlider = async () => {
        const {
          data: { listSlider },
        } = await sliderApi.getList();

        const data = [];

        for (let i = 0; i < listSlider.length; i++) {
          data.push({
            image: `${BASE_URL}${listSlider[i].image}`,
            action: listSlider[i],
          });
        }
        setaDataSource(data);
      };

      getListSlider();
    } catch (e) {
      dispatch(actionSetMgsError("Delete slider fail"));
    }
  };

  const columns = [
    {
      title: "Image",
      dataIndex: "image",
      key: "image",
      render: (image) => {
        return (
          <img
            style={{ width: "100%", height: "auto", objectFit: "cover" }}
            src={image}
          />
        );
      },
    },

    {
      title: "",
      dataIndex: "action",
      key: "action",
      render: (data) => {
        return (
          <div className="d-flex">
            <Button
              className="mr-3 d-flex justify-content-center align-items-center"
              onClick={() => handleEdit(data)}
            >
              <EditOutlined />
              <span>Edit</span>
            </Button>
            <Button
              className=" d-flex justify-content-center align-items-center"
              onClick={() => handleRemove(data)}
            >
              <DeleteOutlined />
              <span> Remove</span>
            </Button>
          </div>
        );
      },
    },
  ];
  const handleChangePage = (value) => {};
  return (
    <>
      <div style={{ justifyContent: "flex-end" }} className="row">
        <Button
          type="primary"
          style={{ display: "flex", alignItems: "center" }}
          onClick={() => history.push("/add-slider")}
        >
          <PlusOutlined />
          <span> Add Slider</span>
        </Button>
      </div>
      <Table
        dataSource={dataSource}
        columns={columns}
        pagination={false}
      ></Table>
      <Pagination
        current={+pagina.page}
        total={pagina ? Math.ceil(+pagina.total / +pagina.amount) * 10 : 0}
        onChange={handleChangePage}
      />
    </>
  );
}

export default BrandManager;
