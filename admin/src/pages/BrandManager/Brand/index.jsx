import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useFormik } from "formik";
import * as yup from "yup";
import { Input, Button, Modal, Upload, message, Select } from "antd";
import MessageValidationError from "../../../components/MessageValidationError";
import EmptyImage from "../../../assets/empty.jpg";
import "./style.scss";
import {
  PlusCircleOutlined,
  EditOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import { SketchPicker } from "react-color";
import { LoadingOutlined, PlusOutlined } from "@ant-design/icons";
import brandApi from "../../../api/brandApi";
import productApi from "../../../api/productApi";
import { useDispatch } from "react-redux";
import { useHistory, useLocation } from "react-router-dom";
import roleApi from "../../../api/roleApi";
import {
  actionSetMgsSuccess,
  actionSetMgsError,
} from "../../../actions/message";
import { BASE_URL } from "../../../constants";
import UploadFile from "../../../components/UploadFile";

Brand.propTypes = {};

function Brand(props) {
  const [name, setName] = useState("");
  const [isError, setIsError] = useState(false);
  const history = useHistory();
  const [defaultUrl, setDefaultUrl] = useState("");
  const location = useLocation();
  const [file, setFile] = useState(null);
  const dispatch = useDispatch();
  const handleUploadSuccess = (url, file) => {
    setFile(file);
  };
  const handleChange = (e) => {
    if (e.target.value) {
      setIsError(false);
    }
    setName(e.target.value);
  };
  const handleUploadFail = (message) => {};

  const handleSave = async () => {
    if (!name) {
      setIsError(true);
    }
    if (location.state) {
      try {
        if (file) {
          await brandApi.update(location.state._id, {
            name,
            logo: file,
          });
        } else {
          await brandApi.update(location.state._id, {
            name,
          });
        }

        dispatch(actionSetMgsSuccess("Update brand success"));
        history.goBack();
      } catch (e) {
        dispatch(actionSetMgsError("Update brand fail"));
      }
    } else {
      try {
        if (!file) {
          dispatch(actionSetMgsError("Please choose logo"));
          return;
        }
        await brandApi.create({
          name,
          logo: file,
        });
        dispatch(actionSetMgsSuccess("Create brand success"));
        history.goBack();
      } catch (e) {
        dispatch(actionSetMgsError("Create brand fail"));
      }
    }
  };
  useEffect(() => {
    if (location.state) {
      setName(location.state.name);

      setDefaultUrl(`${BASE_URL}${location.state.logo}`);
    }
  }, [location.state]);

  return (
    <>
      <div className="row">
        <div className="col-md-6">
          <label htmlFor="">Logo:</label>
          <UploadFile
            defaultUrl={defaultUrl}
            onUploadSuccess={handleUploadSuccess}
            onUploadFail={handleUploadFail}
          />
        </div>
        <div className="col-md-6">
          <div className="form-group">
            <div className="input-group">
              <label className="title-form">Name: </label>
              <Input
                className={`form-control ml-3 ${
                  isError ? "validate-form-error" : ""
                }`}
                name="name"
                value={name}
                onChange={handleChange}
              />
              <br />
              {isError && (
                <label style={{ color: "red", display: "block" }}>
                  Name is required
                </label>
              )}
            </div>
          </div>
        </div>
      </div>

      <div className="row">
        <div className="col-md-12 d-flex">
          <Button
            onClick={handleSave}
            className="login-form-button flex-fill primary"
          >
            SAVE
          </Button>

          <Button
            className="login-form-button flex-fill"
            onClick={() => history.goBack()}
          >
            CANCEL
          </Button>
        </div>
      </div>
    </>
  );
}

export default Brand;
