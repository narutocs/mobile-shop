import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Table, Button, Pagination } from "antd";
import userApi from "../../api/userApi";
import { BASE_URL } from "../../constants";
import { Route, useHistory } from "react-router-dom";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { useDispatch } from "react-redux";
import { actionSetMgsError, actionSetMgsSuccess } from "../../actions/message";

UserManager.propTypes = {};

function UserManager(props) {
  const [dataSource, setaDataSource] = useState([]);
  const [pagina, setPagina] = useState({
    page: 0,
    total: 10,
    amount: 10,
  });
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    const getListUser = async () => {
      const {
        data: { listUser },
        pagination,
      } = await userApi.getList();
      const data = [];

      for (let i = 0; i < listUser.length; i++) {
        data.push({
          image: `${BASE_URL}${listUser[i].avatar}`,
          name: listUser[i].fullName,
          email: listUser[i].email,
          phone_number: listUser[i].phoneNumber,
          role: listUser[i].idRole.name,
          action: listUser[i],
        });
      }
      setaDataSource(data);

      setPagina(pagination);
    };

    getListUser();
  }, []);

  const handleChangePage = (value) => {};

  const handleEdit = (data) => {
    history.push({
      pathname: "/edit-user",
      state: data,
    });
  };

  const handleRemove = async (data) => {
    try {
      await userApi.delete(data._id);
      const getListUser = async () => {
        const {
          data: { listUser },
          pagination,
        } = await userApi.getList();
        const data = [];

        for (let i = 0; i < listUser.length; i++) {
          data.push({
            image: `${BASE_URL}${listUser[i].avatar}`,
            name: listUser[i].fullName,
            email: listUser[i].email,
            phone_number: listUser[i].phoneNumber,
            role: listUser[i].idRole.name,
            action: listUser[i],
          });
        }
        setaDataSource(data);

        setPagina(pagination);
      };

      getListUser();
      dispatch(actionSetMgsSuccess("Delete user success"));
    } catch (e) {
      dispatch(actionSetMgsError("Delete user fail"));
    }
  };
  const columns = [
    {
      title: "Image",
      dataIndex: "image",
      key: "image",
      render: (image) => {
        return (
          <img
            style={{ width: 50, height: 50, objectFit: "cover" }}
            src={image}
          />
        );
      },
    },
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Phone number",
      dataIndex: "phone_number",
      key: "phone_number",
    },
    {
      title: "Role",
      dataIndex: "role",
      key: "role",
    },
    {
      title: "",
      dataIndex: "action",
      key: "action",
      render: (data) => {
        return (
          <div className="d-flex">
            <Button
              className="mr-3 d-flex justify-content-center align-items-center"
              onClick={() => handleEdit(data)}
            >
              <EditOutlined />
              <span>Edit</span>
            </Button>
            <Button
              onClick={() => handleRemove(data)}
              className=" d-flex justify-content-center align-items-center"
            >
              <DeleteOutlined />
              <span> Remove</span>
            </Button>
          </div>
        );
      },
    },
  ];
  return (
    <>
      <div style={{ justifyContent: "flex-end" }} className="row">
        <Button type="primary" onClick={() => history.push("/add-user")}>
          Add User
        </Button>
      </div>
      <Table
        dataSource={dataSource}
        columns={columns}
        pagination={false}
      ></Table>
      <Pagination
        current={+pagina.page}
        total={pagina ? Math.ceil(+pagina.total / +pagina.amount) * 10 : 0}
        onChange={handleChangePage}
      />
    </>
  );
}

export default UserManager;
