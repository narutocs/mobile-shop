import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useFormik } from "formik";
import * as yup from "yup";
import { Input, Button, Modal, Upload, message, Select } from "antd";
import MessageValidationError from "../../../components/MessageValidationError";
import EmptyImage from "../../../assets/empty.jpg";
import "./style.scss";
import {
  PlusCircleOutlined,
  EditOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import { SketchPicker } from "react-color";
import { LoadingOutlined, PlusOutlined } from "@ant-design/icons";
import brandApi from "../../../api/brandApi";
import productApi from "../../../api/productApi";
import { useDispatch } from "react-redux";
import { useHistory, useLocation } from "react-router-dom";
import roleApi from "../../../api/roleApi";
import {
  actionSetMgsSuccess,
  actionSetMgsError,
} from "../../../actions/message";
import { BASE_URL } from "../../../constants";
import userApi from "../../../api/userApi";

User.propTypes = {};

function User(props) {
  const { Option } = Select;
  const initialValues = {
    fullName: "",
    phoneNumber: "",
    email: "",
    role: "",
  };

  const validationSchema = yup.object().shape({
    fullName: yup.string().required(),
    phoneNumber: yup.string().required(),
    email: yup.string().required(),
    role: yup.string().required(),
  });
  const [listRole, setListRole] = useState([]);
  const location = useLocation();
  const history = useHistory();

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: async (values) => {
      if (location.state) {
        try {
          await userApi.update(location.state._id, values);
          dispatch(actionSetMgsSuccess("Update product success"));
          history.goBack();
        } catch (e) {
          dispatch(actionSetMgsError("Update product fail"));
        }
      } else {
        try {
          await userApi.create(values);
          dispatch(actionSetMgsSuccess("Create user success"));
          history.goBack();
        } catch (e) {
          dispatch(actionSetMgsError("Create user fail"));
        }
      }
    },
  });

  useEffect(() => {
    const getListRole = async () => {
      const data = await roleApi.getList();
      setListRole(data.listRole);
    };
    getListRole();
  }, []);
  const {
    values,
    handleBlur,
    handleChange,
    handleSubmit,
    errors,
    touched,
  } = formik;
  const dispatch = useDispatch();
  const resetForm = () => {
    formik.setValues({
      fullName: "",
      phoneNumber: "",
      email: "",
      role: "",
    });
    formik.setTouched({});
    formik.setErrors({});
  };

  const renderListRole = () => {
    let element = null;

    element = listRole.map((role) => {
      return <Option value={role._id}>{role.name}</Option>;
    });
    return element;
  };
  const handleChangRole = (value) => {
    formik.setFieldValue("role", value);
  };
  useEffect(() => {
    if (!location.state) {
      return;
    }

    formik.setValues({
      fullName: location.state.fullName,
      phoneNumber: location.state.phoneNumber,
      email: location.state.email,
      role: location.state.idRole._id,
    });
  }, [location.state]);
  return (
    <>
      <div className="row">
        <div className="title-login">
          {location.state ? "EDIT USER" : "CREATE USER"}
        </div>
      </div>
      <form onSubmit={handleSubmit}>
        <div className="row mb-4 ">
          <div className="col-md-6">
            <div className="form-group">
              <div className="input-group">
                <label className="title-form">Full name: </label>
                <Input
                  className={`form-control ml-3 ${
                    errors.fullName && touched.fullName
                      ? "validate-form-error"
                      : ""
                  }`}
                  name="fullName"
                  value={values.fullName}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                <MessageValidationError
                  touchedOop={touched.fullName}
                  messageErr={errors.fullName}
                />
              </div>
            </div>
          </div>

          <div className="col-md-6">
            <div className="form-group ">
              <div className="input-group">
                <label className="title-form">Phone Number: </label>
                <Input
                  name="phoneNumber"
                  className={`form-control ml-3 ${
                    errors.phoneNumber && touched.phoneNumber
                      ? "validate-form-error"
                      : ""
                  }`}
                  value={values.phoneNumber}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                <MessageValidationError
                  touchedOop={touched.phoneNumber}
                  messageErr={errors.phoneNumber}
                />
              </div>
            </div>
          </div>
        </div>

        <div className="row mb-4 ">
          <div className="col-md-6">
            <div className="form-group">
              <div className="input-group">
                <label className="title-form">Email: </label>
                <Input
                  className={`form-control ml-3 ${
                    errors.email && touched.email ? "validate-form-error" : ""
                  }`}
                  name="email"
                  value={values.email}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                <MessageValidationError
                  touchedOop={touched.email}
                  messageErr={errors.email}
                />
              </div>
            </div>
          </div>

          <div className="col-md-6">
            <div className="form-group">
              <div className="input-group">
                <label className="title-form">Role: </label>
                <Select
                  style={{ width: 350 }}
                  onChange={handleChangRole}
                  value={formik.values.role}
                >
                  {renderListRole()}
                </Select>
                <MessageValidationError
                  touchedOop={touched.role}
                  messageErr={errors.role}
                />
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-md-12 d-flex">
            <Button
              htmlType="submit"
              className="login-form-button flex-fill primary"
            >
              SAVE
            </Button>

            <Button
              className="login-form-button flex-fill"
              onClick={() => history.goBack()}
            >
              CANCEL
            </Button>
          </div>
        </div>
      </form>
    </>
  );
}

export default User;
