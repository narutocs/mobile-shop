import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useFormik } from "formik";
import * as yup from "yup";
import { Input, Button, Modal, Upload, message, Select } from "antd";
import MessageValidationError from "../../../components/MessageValidationError";
import EmptyImage from "../../../assets/empty.jpg";
import "./style.scss";
import {
  PlusCircleOutlined,
  EditOutlined,
  DeleteOutlined,
} from "@ant-design/icons";
import { SketchPicker } from "react-color";
import { LoadingOutlined, PlusOutlined } from "@ant-design/icons";
import brandApi from "../../../api/brandApi";
import productApi from "../../../api/productApi";
import { useDispatch } from "react-redux";
import { useHistory, useLocation } from "react-router-dom";
import colorApi from "../../../api/colorApi";
import {
  actionSetMgsSuccess,
  actionSetMgsError,
} from "../../../actions/message";
import { BASE_URL } from "../../../constants";
import UploadFile from "../../../components/UploadFile";

Product.propTypes = {};
function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener("load", () => callback(reader.result));
  reader.readAsDataURL(img);
}

function b64toBlob(b64Data, contentType, sliceSize) {
  contentType = contentType || "";
  sliceSize = sliceSize || 512;

  var byteCharacters = atob(b64Data);
  var byteArrays = [];

  for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    var slice = byteCharacters.slice(offset, offset + sliceSize);

    var byteNumbers = new Array(slice.length);
    for (var i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }

    var byteArray = new Uint8Array(byteNumbers);

    byteArrays.push(byteArray);
  }

  var blob = new Blob(byteArrays, { type: contentType });
  return blob;
}

function beforeUpload(file) {
  const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
  if (!isJpgOrPng) {
    message.error("You can only upload JPG/PNG file!");
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error("Image must smaller than 2MB!");
  }
  return isJpgOrPng && isLt2M;
}
function dataURLtoFile(dataurl, filename) {
  var arr = dataurl.split(","),
    mime = arr[0].match(/:(.*?);/)[1],
    bstr = atob(arr[1]),
    n = bstr.length,
    u8arr = new Uint8Array(n);

  while (n--) {
    u8arr[n] = bstr.charCodeAt(n);
  }

  return new File([u8arr], filename, { type: mime });
}

function toDataURL(url, callback) {
  var xhr = new XMLHttpRequest();
  xhr.onload = function () {
    var reader = new FileReader();
    reader.onloadend = function () {
      callback(reader.result);
    };
    reader.readAsDataURL(xhr.response);
  };
  xhr.open("GET", url);
  xhr.responseType = "blob";
  xhr.send();
}
function Product(props) {
  const [isShowModal, setIsShowModal] = useState(false);
  const [loading, setLoading] = useState(false);
  const [color, setColor] = useState("#fff");
  const [image, setImage] = useState("");
  const [file, setFile] = useState(null);
  const [listFile, setListFile] = useState([]);
  const [listImage, setListImage] = useState([]);
  const [listColor, setListColor] = useState([]);
  const [listBrand, setListBrand] = useState([]);

  const [listColorShow, setListColorShow] = useState([]);

  useEffect(() => {
    const getListBrand = async () => {
      const {
        data: { listBrand },
      } = await brandApi.getList();

      setListBrand(listBrand);
    };
    getListBrand();

    const getListColor = async () => {
      const {
        data: { listColor: colors },
      } = await colorApi.getList();
      setListColor(colors);
    };

    getListColor();
  }, []);
  const handleChangeColor = (value) => {
    formik.setFieldValue("color", value);
  };
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();

  const [currentColor, setCurrentColor] = useState(null);
  const { Option } = Select;
  const initialValues = {
    name: "",
    price: "",
    quality: "",
    cpu: "",
    batteries: "",
    operating_system: "",
    internal_storage: "",
    primary_camera: "",
    secondary_camera: "",
    ram: "",
    brand: "",
    color: "",
  };

  const validationSchema = yup.object().shape({
    name: yup.string(),
    price: yup.number(),
    quality: yup.number(),
    cpu: yup.string(),
    batteries: yup.string(),
    operating_system: yup.string(),
    internal_storage: yup.string(),
    primary_camera: yup.string(),
    secondary_camera: yup.string(),
    ram: yup.string(),
    brand: yup.string(),
    color: yup.string(),
  });

  const formik = useFormik({
    initialValues,
    onSubmit: async (values) => {
      if (location.state) {
        try {
          await productApi.update(location.state[0].productId._id, {
            ...values,
            idBrand: values.brand,
          });

          for (let i = 0; i < listColorShow.length; i++) {
            if (listColorShow[i].image.includes("data")) {
              var tempFile = dataURLtoFile(
                listColorShow[i].image,
                `${new Date().getTime()}.jpg`
              );

              listColorShow[i].image = tempFile;
            }
          }

          await productApi.updateProductColor({
            image: listFile,
            productId: location.state[0].productId._id,
            colorId: listColorShow,
          });
          dispatch(actionSetMgsSuccess("Update product success"));
          history.goBack();
        } catch (e) {
          dispatch(actionSetMgsError("Update product fail"));
        }
      } else {
        try {
          const { idProduct } = await productApi.create({
            ...values,
            idBrand: values.brand,
          });

          for (let i = 0; i < listFile.length; i++) {
            if (typeof listFile[i] === "object") {
              await productApi.createProductColor({
                image: listFile[i],
                productId: idProduct,
                colorId: listColorShow[i].id,
              });
            }
          }
          dispatch(actionSetMgsSuccess("Update product success"));
          history.goBack();
        } catch (e) {
          dispatch(actionSetMgsError("Create product fail"));
        }
      }
    },
  });

  const {
    values,
    handleBlur,
    handleChange,
    handleSubmit,
    errors,
    touched,
  } = formik;

  const resetForm = () => {
    formik.setValues({
      name: "",
      price: "",
      quality: "",
      cpu: "",
      batteries: "",
      operating_system: "",
      internal_storage: "",
      primary_camera: "",
      secondary_camera: "",
      ram: "",
      brand: "",
      color: {
        code: [],
        image: [],
      },
    });
    formik.setTouched({});
    formik.setErrors({});
  };

  const handleShowAddColor = () => {
    setIsShowModal(true);
  };

  const handleChangeComplete = (color) => {
    setColor(color.hex);
  };

  const handleChangeImage = (url, file) => {
    setFile(file);
    setImage(url);
    setLoading(false);
  };
  const uploadButton = (
    <div>
      {loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div className="ant-upload-text">Upload</div>
    </div>
  );

  const handleChangeBrand = (value) => {
    formik.setFieldValue("brand", value);
  };

  const handleAddColor = () => {
    // const newListImage = [...listImage];
    // const newListColor = [...listColor];
    // newListImage.push(image);
    // setListImage(newListImage);
    // newListColor.push(color);
    if (currentColor) {
      const newListColor = [...listColorShow];

      const index = listColor.findIndex((x) => x._id === formik.values.color);
      if (index !== -1) {
        const conflict = listColorShow.findIndex(
          (x) =>
            x.id === listColor[index]._id &&
            listColor[index]._id !== currentColor.id
        );
        if (conflict !== -1) {
          dispatch(actionSetMgsError("The color have  in the product"));
          return;
        }

        const indexCurrent = listColorShow.findIndex(
          (x) => x.id === currentColor.id
        );

        newListColor[indexCurrent] = {
          ...newListColor[indexCurrent],
          id: listColor[index]._id,
          name: listColor[index].name,
          code_color: listColor[index].code_color,
          image: image,
        };
      }

      setListColorShow(newListColor);

      toDataURL(image, (imageUrl) => {
        var tempFile = dataURLtoFile(imageUrl, `${new Date().getTime()}.jpg`);
        const newListFile = [...listFile];
        newListFile[listFile.length - 1] = tempFile;

        setListFile(newListFile);
      });

      setImage(null);
      formik.setFieldValue("color", null);
      setIsShowModal(false);
      setCurrentColor(null);
    } else {
      const newListColor = [...listColorShow];
      console.log("zo");
      const index = listColor.findIndex((x) => x._id === formik.values.color);
      if (index !== -1) {
        const conflict = listColorShow.findIndex(
          (x) => x.id === listColor[index]._id
        );
        if (conflict !== -1) {
          dispatch(actionSetMgsError("The color have  in the product"));
          return;
        }
        newListColor.push({
          id: listColor[index]._id,
          name: listColor[index].name,
          code_color: listColor[index].code_color,
          image: image,
        });
      }

      setListColorShow(newListColor);

      const temp3 = [...listFile];
      var tempFile = dataURLtoFile(image, `${new Date().getTime()}.jpg`);

      temp3.push(tempFile);
      setListFile(temp3);
      // const temp = [...formik.values.color.image];
      // temp.push(file);
      // const temp2 = [...formik.values.color.code];
      // temp2.push(color);
      // formik.setFieldValue("color", {
      //   image: temp,
      //   code: temp2,
      // });
      setImage(null);
      formik.setFieldValue("color", null);
      setIsShowModal(false);
    }
  };

  const handleShowEditColor = (current) => {
    setIsShowModal(true);

    setCurrentColor(current);
    setImage(current.image);
    formik.setFieldValue("color", current.id);
    setCurrentColor(current);
  };

  const handleRemoveColor = (current) => {
    const index = listColorShow.findIndex((x) => x.id === current.id);

    const indexFile = listFile.findIndex((x) => current.image.includes(x));

    if (indexFile !== -1) {
      const newListFile = [...listFile];
      newListFile.splice(indexFile, 1);
      setListFile(newListFile);
    }
    if (index !== -1) {
      const newListColor = [...listColorShow];
      newListColor.splice(index, 1);
      setListColorShow(newListColor);
    }
  };

  const renderListColor = () => {
    let element = (
      <>
        <div
          className="d-flex mb-5"
          style={{ justifyContent: "center", alignItems: "center" }}
        >
          <label className="pr-3">Image: </label>
          <img
            style={{
              width: 50,
              height: 50,
              objectFit: "center",
            }}
            className="img-color"
            src={EmptyImage}
          />
          <label className="pr-3">Image name</label>
          <div
            style={{
              width: 50,
              height: 50,
              backgroundColor: `#fff`,
              border: "1px solid",
            }}
            className={`img-color`}
          ></div>
        </div>
      </>
    );

    if (listColorShow.length > 0) {
      element = listColorShow.map((item, index) => {
        return (
          <>
            <div
              className="d-flex mb-5"
              style={{ justifyContent: "center", alignItems: "center" }}
            >
              <label className="pr-3">Image: </label>
              <img
                style={{
                  width: 50,
                  height: 50,
                  objectFit: "center",
                }}
                className="img-color"
                src={`${item.image}`}
              />
              <label className="pr-3">{item.name}</label>
              <div
                style={{
                  width: 50,
                  height: 50,
                  backgroundColor: `${item.code_color}`,
                  border: "1px solid",
                }}
                className={`img-color ${
                  index === listColorShow.length - 1 ? "" : "img-spre"
                }`}
              ></div>

              <EditOutlined
                style={{ fontSize: "2rem" }}
                className="icon-edit pr-4"
                onClick={() => handleShowEditColor(item)}
              />
              <DeleteOutlined
                style={{ fontSize: "2rem" }}
                className="icon-edit"
                onClick={() => handleRemoveColor(item)}
              />
            </div>
          </>
        );
      });
    }

    return element;
  };
  const renderItem = () => {
    let element = null;
    element = listBrand.map((brand) => {
      return <Option value={brand._id}>{brand.name}</Option>;
    });
    return element;
  };
  useEffect(() => {
    if (!location.state) {
      return;
    }

    const imageShow = [];

    const temp3 = [...listFile];

    for (let i = 0; i < location.state.length; i++) {
      imageShow.push({
        image: `${BASE_URL}${location.state[i].image}`,
        id: location.state[i].colorId._id,
        name: location.state[i].colorId.name,
        code_color: location.state[i].colorId.code_color,
        _id: location.state[i]._id,
      });
      temp3.push(location.state[i].image);
    }
    setListFile(temp3);
    setListColorShow(imageShow);

    formik.setValues({
      name: location.state[0].productId.name,
      price: location.state[0].productId.price,
      quality: location.state[0].productId.quality,
      cpu: location.state[0].productId.cpu,
      batteries: location.state[0].productId.batteries,
      operating_system: location.state[0].productId.operating_system,
      internal_storage: location.state[0].productId.internal_storage,
      primary_camera: location.state[0].productId.primary_camera,
      secondary_camera: location.state[0].productId.secondary_camera,
      ram: location.state[0].productId.ram,
      brand: location.state[0].productId.idBrand,
      color: "",
    });
  }, [location.state]);

  const handleUploadFail = () => {
    dispatch(actionSetMgsError("Upload fail"));
  };
  const renderColorItem = () => {
    let element = null;
    element = listColor.map((color) => {
      return <Option value={color._id}>{color.name}</Option>;
    });
    return element;
  };
  return (
    <>
      <div className="row">
        <div className="title-login">
          {location.state ? "EDIT PRODUCT" : "CREATE PRODUCT"}
        </div>
      </div>
      <form onSubmit={handleSubmit} class="login">
        <div className="row mb-4 ">
          <div className="col-md-6">
            <div className="form-group">
              <div className="input-group">
                <label className="title-form">Name: </label>
                <Input
                  className={`form-control ml-3 ${
                    errors.name && touched.name ? "validate-form-error" : ""
                  }`}
                  name="name"
                  value={values.name}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                <MessageValidationError
                  touchedOop={touched.name}
                  messageErr={errors.name}
                />
              </div>
            </div>
          </div>

          <div className="col-md-6">
            <div className="form-group ">
              <div className="input-group">
                <label className="title-form">CPU: </label>
                <Input
                  name="cpu"
                  className={`form-control ml-3 ${
                    errors.cpu && touched.cpu ? "validate-form-error" : ""
                  }`}
                  value={values.cpu}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  type="cpu"
                />
                <MessageValidationError
                  touchedOop={touched.password}
                  messageErr={errors.password}
                />
              </div>
            </div>
          </div>
        </div>

        <div className="row mb-4 ">
          <div className="col-md-6">
            <div className="form-group">
              <div className="input-group">
                <label className="title-form">Batteries: </label>
                <Input
                  className={`form-control ml-3 ${
                    errors.batteries && touched.batteries
                      ? "validate-form-error"
                      : ""
                  }`}
                  name="batteries"
                  value={values.batteries}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                <MessageValidationError
                  touchedOop={touched.batteries}
                  messageErr={errors.batteries}
                />
              </div>
            </div>
          </div>

          <div className="col-md-6">
            <div className="form-group ">
              <div className="input-group">
                <label className="title-form">Operating System: </label>
                <Input
                  name="operating_system"
                  className={`form-control ml-3 ${
                    errors.operating_system && touched.operating_system
                      ? "validate-form-error"
                      : ""
                  }`}
                  value={values.operating_system}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                <MessageValidationError
                  touchedOop={touched.operating_system}
                  messageErr={errors.operating_system}
                />
              </div>
            </div>
          </div>
        </div>

        <div className="row mb-4 ">
          <div className="col-md-6">
            <div className="form-group">
              <div className="input-group">
                <label className="title-form">Internal Storage: </label>
                <Input
                  className={`form-control ml-3 ${
                    errors.internal_storage && touched.internal_storage
                      ? "validate-form-error"
                      : ""
                  }`}
                  name="internal_storage"
                  value={values.internal_storage}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                <MessageValidationError
                  touchedOop={touched.internal_storage}
                  messageErr={errors.internal_storage}
                />
              </div>
            </div>
          </div>

          <div className="col-md-6">
            <div className="form-group ">
              <div className="input-group">
                <label className="title-form">Primary Camera: </label>
                <Input
                  name="primary_camera"
                  className={`form-control ml-3 ${
                    errors.primary_camera && touched.primary_camera
                      ? "validate-form-error"
                      : ""
                  }`}
                  value={values.primary_camera}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                <MessageValidationError
                  touchedOop={touched.primary_camera}
                  messageErr={errors.primary_camera}
                />
              </div>
            </div>
          </div>
        </div>

        <div className="row mb-4 ">
          <div className="col-md-6">
            <div className="form-group">
              <div className="input-group">
                <label className="title-form">Secondary Camera: </label>
                <Input
                  className={`form-control ml-3 ${
                    errors.secondary_camera && touched.secondary_camera
                      ? "validate-form-error"
                      : ""
                  }`}
                  name="secondary_camera"
                  value={values.secondary_camera}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                <MessageValidationError
                  touchedOop={touched.secondary_camera}
                  messageErr={errors.secondary_camera}
                />
              </div>
            </div>
          </div>

          <div className="col-md-6">
            <div className="form-group ">
              <div className="input-group">
                <label className="title-form">Ram: </label>
                <Input
                  name="ram"
                  className={`form-control ml-3 ${
                    errors.ram && touched.ram ? "validate-form-error" : ""
                  }`}
                  value={values.ram}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                <MessageValidationError
                  touchedOop={touched.ram}
                  messageErr={errors.ram}
                />
              </div>
            </div>
          </div>
        </div>

        <div className="row mb-4 ">
          <div className="col-md-6">
            <div className="form-group">
              <div className="input-group">
                <label className="title-form">Quality: </label>
                <Input
                  className={`form-control ml-3 ${
                    errors.quality && touched.quality
                      ? "validate-form-error"
                      : ""
                  }`}
                  name="quality"
                  value={values.quality}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                <MessageValidationError
                  touchedOop={touched.quality}
                  messageErr={errors.quality}
                />
              </div>
            </div>
          </div>

          <div className="col-md-6">
            <div className="form-group ">
              <div className="input-group">
                <label className="title-form">Price: </label>
                <Input
                  name="price"
                  className={`form-control ml-3 ${
                    errors.price && touched.price ? "validate-form-error" : ""
                  }`}
                  value={values.price}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                <MessageValidationError
                  touchedOop={touched.price}
                  messageErr={errors.price}
                />
              </div>
            </div>
          </div>
        </div>

        <div
          className="row"
          style={{ display: "flex", justifyContent: "space-between" }}
        >
          <div>List color of product</div>
          <PlusCircleOutlined
            style={{ fontSize: "2rem" }}
            className="icon-add"
            onClick={handleShowAddColor}
          />
        </div>

        <hr />

        <div className="row">
          <div
            className="col-md-12 d-flex"
            style={{
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "column",
            }}
          >
            {renderListColor()}

            <Modal
              onCancel={() => setIsShowModal(false)}
              footer={null}
              title="Add color"
              visible={isShowModal}
            >
              <div
                className="d-flex"
                style={{
                  justifyContent: "space-between",
                  alignItems: "center",
                  flexDirection: "column",
                }}
              >
                <div className="row mb-3">
                  <div className="col-md-12">
                    <label>Color name: </label>
                    <Select
                      style={{ width: 200 }}
                      onChange={handleChangeColor}
                      value={formik.values.color}
                    >
                      {renderColorItem()}
                    </Select>
                  </div>
                </div>
                <UploadFile
                  onUploadSuccess={handleChangeImage}
                  onUploadFail={handleUploadFail}
                />
              </div>

              <div className="row">
                <Button
                  onClick={handleAddColor}
                  type="primary"
                  style={{ marginTop: 20, width: "100%" }}
                >
                  ADD
                </Button>
              </div>
            </Modal>
          </div>
        </div>

        <hr />
        <div className="row mb-4">
          <div className="col-md-12">
            <div className="form-group">
              <div className="input-group d-flex  ">
                <label className="title-form">Brand: </label>
                <Select
                  style={{ width: "100%" }}
                  onChange={handleChangeBrand}
                  value={formik.values.brand}
                >
                  {renderItem()}
                </Select>
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-md-12 d-flex">
            <Button
              htmlType="submit"
              className="login-form-button flex-fill primary"
            >
              SAVE
            </Button>

            <Button
              className="login-form-button flex-fill"
              onClick={() => history.goBack()}
            >
              CANCEL
            </Button>
          </div>
        </div>
      </form>
    </>
  );
}

export default Product;
