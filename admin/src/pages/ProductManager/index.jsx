import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Table, Button, Pagination } from "antd";
import productApi from "../../api/productApi";
import { BASE_URL } from "../../constants";
import { Route, useHistory } from "react-router-dom";
import { DeleteOutlined, EditOutlined, PlusOutlined } from "@ant-design/icons";
import { useDispatch } from "react-redux";
import { actionSetMgsError } from "../../actions/message";
ProductManager.propTypes = {};

function ProductManager(props) {
  const [dataSource, setaDataSource] = useState([]);
  const [pagina, setPagina] = useState({
    page: 0,
    total: 10,
    amount: 10,
  });
  const dispatch = useDispatch();
  const history = useHistory();
  useEffect(() => {
    const getListProduct = async () => {
      const {
        data: { listData },
        pagination,
      } = await productApi.getList();

      setPagina(pagination);
      const data = [];
      for (let i = 0; i < listData.length; i++) {
        data.push({
          image: `${BASE_URL}${listData[i][0].image}`,
          name: `${listData[i][0].productId.name}`,
          price: `${listData[i][0].productId.price}`,
          quality: `${listData[i][0].productId.quality}`,
          action: listData[i],
        });
      }

      setaDataSource(data);
    };
    getListProduct();
  }, []);

  const handleEdit = (data) => {
    history.push({
      pathname: "/edit-product",
      state: data,
    });
  };

  const handleRemove = async (data) => {
    try {
      await productApi.delete(data[0].productId._id);
      const getListProduct = async () => {
        const {
          data: { listData },
          pagination,
        } = await productApi.getList();

        setPagina(pagination);
        const data = [];
        for (let i = 0; i < listData.length; i++) {
          data.push({
            image: `${BASE_URL}${listData[i][0].image}`,
            name: `${listData[i][0].productId.name}`,
            price: `${listData[i][0].productId.price}`,
            quality: `${listData[i][0].productId.quality}`,
            action: listData[i],
          });
        }

        setaDataSource(data);
      };
      getListProduct();
    } catch (e) {
      dispatch(actionSetMgsError(e));
    }
  };

  const columns = [
    {
      title: "Image",
      dataIndex: "image",
      key: "image",
      render: (image) => {
        return (
          <img
            style={{ width: 50, height: 50, objectFit: "cover" }}
            src={image}
          />
        );
      },
    },
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Price",
      dataIndex: "price",
      key: "price",
    },
    {
      title: "Quality",
      dataIndex: "quality",
      key: "quality",
    },
    {
      title: "",
      dataIndex: "action",
      key: "action",
      render: (data) => {
        return (
          <div className="d-flex">
            <Button
              className="mr-3 d-flex justify-content-center align-items-center"
              onClick={() => handleEdit(data)}
            >
              <EditOutlined />
              <span>Edit</span>
            </Button>
            <Button
              onClick={() => handleRemove(data)}
              className=" d-flex justify-content-center align-items-center"
            >
              <DeleteOutlined />
              <span> Remove</span>
            </Button>
          </div>
        );
      },
    },
  ];
  const handleChangePage = (value) => {
    const getListProduct = async () => {
      const {
        data: { listData },
        pagination,
      } = await productApi.getList({
        page: value,
      });

      setPagina(pagination);
      const data = [];
      for (let i = 0; i < listData.length; i++) {
        data.push({
          image: `${BASE_URL}${listData[i][0].image}`,
          name: `${listData[i][0].productId.name}`,
          price: `${listData[i][0].productId.price}`,
          quality: `${listData[i][0].productId.quality}`,
          action: listData[i],
        });
      }

      setaDataSource(data);
    };
    getListProduct();
  };
  return (
    <>
      <div style={{ justifyContent: "flex-end" }} className="row">
        <Button type="primary"  style={{ display: "flex", alignItems: "center" }} onClick={() => history.push("/add-product")}>
        <PlusOutlined/> 
        
        <span>  Add Product</span>
        </Button>
      </div>
      <Table
        dataSource={dataSource}
        columns={columns}
        pagination={false}
      ></Table>
      <Pagination
        current={+pagina.page}
        total={pagina ? Math.ceil(+pagina.total / +pagina.amount) * 10 : 0}
        onChange={handleChangePage}
      />
    </>
  );
}

export default ProductManager;
