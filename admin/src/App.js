import React, { useState, useEffect } from 'react';
import Login from './pages/Login';
import "./index.scss";
import 'antd/dist/antd.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';
import Message from "./components/Message";
import { ToastContainer } from 'react-toastify';
import { BrowserRouter as Router, Switch, Route, useHistory, Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { Layout, Menu } from 'antd';
import { read } from "jwt-client"
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  VideoCameraOutlined,
  UploadOutlined,
  ContainerOutlined,
  ShopOutlined,
  BookOutlined,
  CaretRightOutlined,
  PoweroffOutlined,
} from '@ant-design/icons';

import * as ActionType from "./constants/ActionType"

import ProductManager from "./pages/ProductManager";
import Product from "./pages/ProductManager/Product";
import OrderManager from "./pages/OrderManager";
import UserManager from "./pages/UserManager";
import User from "./pages/UserManager/User";
import BrandManager from './pages/BrandManager';
import Brand from "./pages/BrandManager/Brand";
import SliderManager from "./pages/SliderManager";
import Slider from "./pages/SliderManager/Slider";
import { actionSetMgsError } from './actions/message';

function App() {
  const [collapsed, setCollapsed] = useState(false)
  const { Header, Sider, Content } = Layout;
  const { token, user } = useSelector(state => state.auth);
  const [selected, setSelected] = useState('1')
  const history = useHistory()
  const [role, setRole] = useState('')

  const dispatch = useDispatch();
  const toggle = () => {
    setCollapsed(!collapsed)
  }

  useEffect(() => {
    if (token) {
      const data = read(token);
      if (data.claim.role === "USER") {
        dispatch(actionSetMgsError("YOU NOT A ADMIN"));

      } else {
        setRole(data.claim.role)
      }
    }
  }, [token])
  const handleGoToOrder = () => {
    setSelected('3')
  }

  const handleGoToUser = () => {
    setSelected('1')
  }

  const handleGoToProduct = () => {
    setSelected('2')
  }
  const handleGoToBrand = () => {
    setSelected('4')
  }
  const handleLogout = () => {
    dispatch({
      type: ActionType.LOG_OUT
    })
  }

  const handleGoToSlider = () => {
    setSelected('5')
  }
  return (
    <>
      <Router>
        <Message>
          {token && role ? <Layout>
            <Sider Sider trigger={null} collapsible collapsed={collapsed} >
              <div className="logo" />
              <Menu theme="dark" mode="inline" defaultSelectedKeys={[selected]}>

                <Menu.Item key="1" icon={<UserOutlined />} onClick={handleGoToUser}>
                  <Link to="/">
                    User Manager
                    </Link>
                </Menu.Item>
                <Menu.Item key="4" icon={<BookOutlined />} onClick={handleGoToBrand}>
                  <Link to="/brand">
                    Brand Manager
                  </Link>
                </Menu.Item>
                <Menu.Item key="2" icon={<ShopOutlined />} onClick={handleGoToProduct}>
                  <Link to="/product">
                    Product Manager
                  </Link>
                </Menu.Item>
                <Menu.Item key="3" icon={<ContainerOutlined />} onClick={handleGoToOrder}>
                  <Link to="/order">
                    Order Manager
                  </Link>
                </Menu.Item>
                <Menu.Item key="5" icon={<CaretRightOutlined />} onClick={handleGoToSlider}>
                  <Link to="/slider">
                    Slider Manager
                  </Link>
                </Menu.Item>
              </Menu>
            </Sider >
            <Layout className="site-layout">
              <Header className="site-layout-background" style={{ padding: 0 }}>
                <div className="row ">
                  <div className="col-md-12 d-flex justify-content-between">
                    {collapsed ? <MenuUnfoldOutlined onClick={toggle} className="trigger" /> : <MenuFoldOutlined onClick={toggle} className="trigger" />}
                    {token && <div className="justify-content-end d-flex align-items-center header">
                      <label className="pr-3"> {user.email}</label>
                      <button className="btn btn-danger btn-header" style={{ display: "flex", alignItems: "center" }} onClick={handleLogout}>
                        <PoweroffOutlined />
                        <span className='ml-3'>Log out</span>
                      </button>
                    </div>}
                  </div>
                </div>
              </Header>
              <Content
                className="site-layout-background"
                style={{
                  margin: '24px 16px',
                  padding: 24,
                  minHeight: 280,
                }}
              >
                <Switch>
                  <Route component={UserManager} exact path="/" />
                  <Route component={Product} exact path="/add-product" />
                  <Route component={Product} exact path="/edit-product" />
                  <Route component={User} exact path="/add-user" />
                  <Route component={User} exact path="/edit-user" />
                  <Route component={Brand} exact path="/add-brand" />
                  <Route component={Brand} exact path="/edit-brand" />
                  <Route component={Slider} exact path="/add-slider" />
                  <Route component={Slider} exact path="/edit-slider" />
                  <Route component={ProductManager} exact path="/product" />
                  <Route component={OrderManager} exact path="/order" />
                  <Route component={BrandManager} exact path="/brand" />
                  <Route component={SliderManager} exact path="/slider" />
                </Switch>
              </Content>
            </Layout>
          </Layout > : <Login />}
          <ToastContainer />
        </Message>
      </Router>

    </>
  );
}

export default App;
