import React from "react";
import { toast } from "react-toastify";
import { useSelector, useDispatch } from "react-redux";

import MessageType from "../../constants/MessageType";
import { actionClearMgs } from "../../actions/message";

function Message({ children }) {
  const { message, type } = useSelector((state) => state.message);
  const dispatch = useDispatch();
  if (message) {
    type === MessageType.SUCCESS
      ? toast.success(message)
      : toast.error(message);

    dispatch(actionClearMgs());
  }

  return <>{children}</>;
}

export default Message;
