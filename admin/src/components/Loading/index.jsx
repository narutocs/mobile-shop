import React from "react";
import { Spin } from "antd";
import { useSelector } from "react-redux";

function Loading({ children }) {
  const isLoading = useSelector((state) => state.isLoading);
  return (
    <Spin tip="Waiting..." spinning={isLoading}>
      {children}
    </Spin>
  );
}

export default Loading;
