import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import axios from "axios";
import { Progress } from "reactstrap";

import "./style.scss";

const UploadFile = ({
  width = 200,
  height = 200,
  defaultUrl,
  onUploadSuccess = (url) => {},
  onUploadFail = (message) => {},
}) => {
  const [url, setUrl] = useState("");
  const [temp] = useState([]);
  const [isShowProgress, setIsShowProgress] = useState(false);
  const [valueProgress, setValueProgress] = useState(1);

  // show progress bar
  useEffect(() => {
    if (valueProgress + 0.1 <= 100) {
      setValueProgress(valueProgress + 5);
    } else {
      setTimeout(() => {
        setIsShowProgress(false);
      }, 500);
    }
  }, [valueProgress]);
  console.log("efault", defaultUrl);
  useEffect(() => {
    if (defaultUrl) {
      setUrl(defaultUrl);
    }
  }, [defaultUrl]);
  const handleOnChange = async (e) => {
    const { target: { files = [] } = {} } = e;
    const fileUpload = files[0];
    if (fileUpload.type.indexOf("image") === -1) {
      onUploadFail("The file is not a image");
      return;
    }
    let reader = new FileReader();
    reader.readAsDataURL(fileUpload);
    reader.onload = (event) => {
      if (event.target) {
        if (onUploadSuccess) {
          setUrl(event.target.result);
          onUploadSuccess(event.target.result, fileUpload);
        }
      } else {
        if (onUploadFail) {
          onUploadFail("Upload file fail");
        }
      }
    };
  };

  const renderUploadAvatar = () => {
    return (
      <div style={{ width, height }} className="wrapper-upload wrapper">
        <label
          className={`box-upload ${url ? "upload" : ""}`}
          htmlFor="avatar"
          style={{ width, height }}
        >
          {url ? (
            <div className="content-image">
              <img src={url} alt="" className="image" />
              {/* <img src={IconDelete} alt="" className="icon-delete" onClick={deleteImage} /> */}
            </div>
          ) : (
            <div className="box-content">
              <span>+ Upload</span>
            </div>
          )}
          <input
            value={temp}
            type="file"
            id="avatar"
            hidden
            onChange={handleOnChange}
            accept="image/*"
          />
        </label>
      </div>
    );
  };

  return (
    <>
      {isShowProgress && <Progress animated value={valueProgress} />}

      {renderUploadAvatar()}
    </>
  );
};

UploadFile.propTypes = {
  onUploadSuccess: PropTypes.func,
  onUploadFail: PropTypes.func,
  width: PropTypes.number,
  height: PropTypes.number,
};

export default UploadFile;
