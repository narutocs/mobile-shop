import axiosClient from "./axiosClient";

class OrderApi {
  getList = (params) => {
    const url = "/order";
    return axiosClient.get(url, {
      params
    });
  }
  getById = (id) => {
    const url = `/order/${id}`;
    return axiosClient.get(url);
  }
  update = (id, params) => {
    const url = `/order/${id}`;
    return axiosClient.put(url, params)
  }
  delete = (id) => {
    const url = `/order/${id}`;
    return axiosClient.delete(url)
  }

  create = (params) => {
    const url = `/order`;
    return axiosClient.post(url, params)
  }


}

const orderApi = new OrderApi();

export default orderApi;