import axiosClient from "./axiosClient";

class ProductApi {
  getList = (params) => {
    const url = "/product";
    return axiosClient.get(url, {
      params
    });
  }
  getById = (id) => {
    const url = `/product/${id}`;
    return axiosClient.get(url);
  }
  update = (id, params) => {
    const url = `/product/${id}`;
    return axiosClient.put(url, params)
  }
  delete = (id) => {
    const url = `/product/${id}`;
    return axiosClient.delete(url)
  }

  create = (params) => {
    const url = `/product`;
    return axiosClient.post(url, params)
  }

  createProductColor = (params) => {

    const formData = new FormData();
    const keys = Object.keys(params);
    const values = Object.values(params);

    for (let i = 0; i < keys.length; i++) {
      formData.append(keys[i], values[i])
    }
    const url = "/product-color";
    return axiosClient.post(url, formData);
  }

  updateProductColor = (params) => {
    const formData = new FormData();

    for (let i = 0; i < params.colorId.length; i++) {
      if (typeof params.colorId[i].image === 'object') {
        formData.append('image', params.colorId[i].image)

      }

    }
    formData.append('idColor', JSON.stringify(params.colorId))
    formData.append('productId', params.productId)

    const url = `/product-color`;
    return axiosClient.put(url, formData);
  }

  createColor = (params) => {
    const url = "/color";
    return axiosClient.post(url, params);
  }
}

const productApi = new ProductApi();

export default productApi;