import axiosClient from "./axiosClient";

class OrderApi {
  getList = (params) => {
    const url = "/color";
    return axiosClient.get(url, {
      params
    });
  }
}

const orderApi = new OrderApi();

export default orderApi;