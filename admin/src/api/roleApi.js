import axiosClient from "./axiosClient";

class RoleApi {
  getList = (params) => {
    const url = "/role";
    return axiosClient.get(url, {
      params
    });
  }
  getById = (id) => {
    const url = `/role/${id}`;
    return axiosClient.get(url);
  }
  update = (id, params) => {
    const url = `/role/${id}`;
    return axiosClient.put(url, params)
  }
  delete = (id) => {
    const url = `/role/${id}`;
    return axiosClient.delete(url)
  }

  create = (params) => {
    const url = `/role`;
    return axiosClient.post(url, params)
  }


}

const roleApi = new RoleApi();

export default roleApi;