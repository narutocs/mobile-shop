import axiosClient from "./axiosClient";

class SliderApi {
  create = (params) => {
    const url = "/slider";
    const formData = new FormData();
    const keys = Object.keys(params);
    const values = Object.values(params);

    for (let i = 0; i < keys.length; i++) {
      formData.append(keys[i], values[i])
    }
    return axiosClient.post(url, formData);
  }
  getList = () => {
    const url = "/slider";
    return axiosClient.get(url);
  }
  getById = (id) => {
    const url = `/slider/${id}`;
    return axiosClient.get(url);
  }
  update = (id, params) => {
    const url = `/slider/${id}`;
    const formData = new FormData();
    const keys = Object.keys(params);
    const values = Object.values(params);

    for (let i = 0; i < keys.length; i++) {
      formData.append(keys[i], values[i])
    }
    return axiosClient.put(url, formData)
  }
  delete = (id) => {
    const url = `/slider/${id}`;
    return axiosClient.delete(url)
  }
}

const sliderApi = new SliderApi();

export default sliderApi;