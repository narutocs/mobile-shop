import axiosClient from "./axiosClient";

class UserApi {
  getList = (params) => {
    const url = "/user";
    return axiosClient.get(url, {
      params
    });
  }
  getById = (id) => {
    const url = `/user/admin/${id}`;
    return axiosClient.get(url);
  }
  update = (id, params) => {
    const url = `/user/admin/${id}`;
    return axiosClient.put(url, params)
  }
  delete = (id) => {
    const url = `/user/admin/${id}`;
    return axiosClient.delete(url)
  }

  create = (params) => {
    const url = `/user/admin/`;
    return axiosClient.post(url, params)
  }


}

const userApi = new UserApi();

export default userApi;