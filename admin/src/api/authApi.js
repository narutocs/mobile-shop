import axiosClient from "./axiosClient";

class AuthApi {
  register = params => {
    const url = "/auth/register";
    return axiosClient.post(url, params);
  }
  login = params => {
    const url = "/auth/login";
    return axiosClient.post(url, params);
  }
  active = params => {
    const url = '/auth/active';
    return axiosClient.put(url, params)
  }
}

const authApi = new AuthApi();

export default authApi;