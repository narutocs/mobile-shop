import axiosClient from "./axiosClient";

class BrandApi {
  create = (params) => {
    const url = "/brand";
    const formData = new FormData();
    const keys = Object.keys(params);
    const values = Object.values(params);

    for (let i = 0; i < keys.length; i++) {
      formData.append(keys[i], values[i])
    }
    return axiosClient.post(url, formData);
  }
  getList = () => {
    const url = "/brand";
    return axiosClient.get(url);
  }
  getById = (id) => {
    const url = `/brand/${id}`;
    return axiosClient.get(url);
  }
  update = (id, params) => {
    const url = `/brand/${id}`;
    const formData = new FormData();
    const keys = Object.keys(params);
    const values = Object.values(params);

    for (let i = 0; i < keys.length; i++) {
      formData.append(keys[i], values[i])
    }
    return axiosClient.put(url, formData)
  }
  delete = (id) => {
    const url = `/brand/${id}`;
    return axiosClient.delete(url)
  }
}

const brandApi = new BrandApi();

export default brandApi;